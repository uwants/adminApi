<?php

namespace App\Exceptions;

use App\Contracts\Response\ErrorResponseInterface;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Log;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param Throwable $e
     * @throws Throwable
     */
    public function report(Throwable $e)
    {
        Log::error('错误', [$e->getMessage(),$e->getTrace()]);
        $error = app(ErrorResponseInterface::class);
        if ($e instanceof MessageException) {
            $error->setMessage($e->getMessage());
            $error->execute();
        } elseif (!$e instanceof HttpResponseException && $e instanceof Throwable) {
            Log::error(MessageException::SYSTEM_ERROR_MESSAGE, $e->getTrace());
            $error->setMessage($e->getMessage());
            $error->execute();
        }
        parent::report($e);
    }
}
