<?php

namespace App\Exceptions;

use Exception;

class MessageException extends Exception
{
    const SYSTEM_ERROR_MESSAGE = '系统异常，请稍后重试';
}
