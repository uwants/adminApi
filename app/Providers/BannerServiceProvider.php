<?php

namespace App\Providers;

use App\Contracts\Banner\CreateBannerInterface;
use App\Contracts\Banner\DeleteBannerInterface;
use App\Contracts\Banner\GetBannerInterface;
use App\Contracts\Banner\ListBannerInterface;
use App\Contracts\Banner\SwitchBannerInterface;
use App\Contracts\Banner\UpdateBannerInterface;
use App\Logic\Banner\CreateBannerLogic;
use App\Logic\Banner\DeleteBannerLogic;
use App\Logic\Banner\GetBannerLogic;
use App\Logic\Banner\ListBannerLogic;
use App\Logic\Banner\SwitchBannerLogic;
use App\Logic\Banner\UpdateBannerLogic;
use Illuminate\Support\ServiceProvider;

class BannerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UpdateBannerInterface::class, UpdateBannerLogic::class);
        $this->app->bind(SwitchBannerInterface::class, SwitchBannerLogic::class);
        $this->app->bind(ListBannerInterface::class, ListBannerLogic::class);
        $this->app->bind(GetBannerInterface::class, GetBannerLogic::class);
        $this->app->bind(CreateBannerInterface::class, CreateBannerLogic::class);
        $this->app->bind(DeleteBannerInterface::class, DeleteBannerLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
