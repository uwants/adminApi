<?php


namespace App\Providers;

use App\Contracts\Statistics\DataUsersInterface;
use App\Logic\Statistics\DataUsersLogic;
use App\Contracts\Statistics\EveryDataInterface;
use App\Logic\Statistics\EveryDataLogic;
use App\Contracts\Statistics\ActivityDataInterface;
use App\Logic\Statistics\ActivityDataLogic;
use App\Contracts\Statistics\ActivityOpInterface;
use App\Logic\Statistics\ActivityOpLogic;
use Illuminate\Support\ServiceProvider;

class StatisticsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataUsersInterface::class, DataUsersLogic::class);
        $this->app->bind(EveryDataInterface::class, EveryDataLogic::class);
        $this->app->bind(ActivityDataInterface::class, ActivityDataLogic::class);
        $this->app->bind(ActivityOpInterface::class, ActivityOpLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
