<?php

namespace App\Providers;

use App\Contracts\Operate\ActivityInterface;
use App\Logic\Operate\ActivityLogic;
use App\Contracts\Operate\CreateActivityInterface;
use App\Logic\Operate\CreateActivityLogic;
use Illuminate\Support\ServiceProvider;


class OperateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ActivityInterface::class, ActivityLogic::class);
        $this->app->bind(CreateActivityInterface::class, CreateActivityLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
