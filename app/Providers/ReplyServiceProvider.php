<?php

namespace App\Providers;

use App\Contracts\Reply\GetReplyInterface;
use App\Contracts\Reply\ListReplyInterface;
use App\Logic\Reply\GetReplyLogic;
use App\Logic\Reply\ListReplyLogic;
use Illuminate\Support\ServiceProvider;

class ReplyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GetReplyInterface::class,GetReplyLogic::class);
        $this->app->bind(ListReplyInterface::class,ListReplyLogic::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
