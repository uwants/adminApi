<?php

namespace App\Providers;

use App\Contracts\Response\ErrorResponseInterface;
use App\Contracts\Response\SuccessResponseInterface;
use App\Logic\Response\ErrorResponseLogic;
use App\Logic\Response\SuccessResponseLogic;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SuccessResponseInterface::class, SuccessResponseLogic::class);
        $this->app->bind(ErrorResponseInterface::class, ErrorResponseLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
