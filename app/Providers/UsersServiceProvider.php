<?php

namespace App\Providers;

use App\Contracts\Users\ListUsersInterface;
use App\Logic\Users\ListUsersLogic;
use App\Contracts\Users\ListUserOrderInterface;
use App\Logic\Users\ListUserOrderLogic;
use App\Contracts\Users\GetUsersInterface;
use App\Logic\Users\GetUsersLogic;
use App\Contracts\Users\ListUserBoxInterface;
use App\Logic\Users\ListUserBoxLogic;
use App\Contracts\Users\ListUserOpInterface;
use App\Logic\Users\ListUserOpLogic;

use Illuminate\Support\ServiceProvider;

class UsersServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ListUsersInterface::class, ListUsersLogic::class);
        $this->app->bind(GetUsersInterface::class, GetUsersLogic::class);
        $this->app->bind(ListUserOrderInterface::class, ListUserOrderLogic::class);
        $this->app->bind(ListUserBoxInterface::class, ListUserBoxLogic::class);
        $this->app->bind(ListUserOpInterface::class, ListUserOpLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
