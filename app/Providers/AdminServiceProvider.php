<?php

namespace App\Providers;

use App\Contracts\AdminUser\GetAdminUserInterface;
use App\Logic\AdminUser\GetAdminUserLogic;
use App\Contracts\AdminUser\CreateAdminUserInterface;
use App\Logic\AdminUser\CreateAdminUserLogic;
use App\Contracts\AdminUser\GetMessageInterface;
use App\Logic\AdminUser\GetMessageLogic;
use App\Contracts\AdminUser\UpdateAdminInterface;
use App\Logic\AdminUser\UpdateAdminLogic;
use App\Contracts\AdminUser\ListAdminInterface;
use App\Logic\AdminUser\ListAdminLogic;
use App\Contracts\AdminUser\DeleteAdminInterface;
use App\Logic\AdminUser\DeleteAdminLogic;
use App\Contracts\AdminUser\CreateMenuInterface;
use App\Contracts\AdminUser\DeleteMenuInterface;
use App\Contracts\AdminUser\ListMenuInterface;
use App\Logic\AdminUser\CreateMenuLogic;
use App\Logic\AdminUser\DeleteMenuLogic;
use App\Logic\AdminUser\ListMenuLogic;
use App\Contracts\AdminUser\ListRoleInterface;
use App\Contracts\AdminUser\CreateRoleInterface;
use App\Logic\AdminUser\ListRoleLogic;
use App\Logic\AdminUser\CreateRoleLogic;

use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GetAdminUserInterface::class, GetAdminUserLogic::class);
        $this->app->bind(CreateAdminUserInterface::class, CreateAdminUserLogic::class);
        $this->app->bind(GetMessageInterface::class, GetMessageLogic::class);
        $this->app->bind(UpdateAdminInterface::class, UpdateAdminLogic::class);
        $this->app->bind(ListAdminInterface::class, ListAdminLogic::class);
        $this->app->bind(DeleteAdminInterface::class, DeleteAdminLogic::class);
        $this->app->bind(CreateMenuInterface::class, CreateMenuLogic::class);
        $this->app->bind(DeleteMenuInterface::class, DeleteMenuLogic::class);
        $this->app->bind(ListMenuInterface::class, ListMenuLogic::class);
        $this->app->bind(ListRoleInterface::class, ListRoleLogic::class);
        $this->app->bind(CreateRoleInterface::class, CreateRoleLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
