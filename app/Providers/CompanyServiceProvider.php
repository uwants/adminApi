<?php

namespace App\Providers;

use App\Contracts\Company\ListCompanyApplyInterface;
use App\Logic\Company\ListCompanyApplyLogic;
use App\Contracts\Company\ListCompanyInterface;
use App\Logic\Company\ListCompanyLogic;
use App\Contracts\Company\UpdateCompanyApplyInterface;
use App\Logic\Company\UpdateCompanyApplyLogic;
use App\Contracts\Company\UpdateCompanyInterface;
use App\Logic\Company\UpdateCompanyLogic;
use App\Contracts\Company\GetCompanyInterface;
use App\Logic\Company\GetCompanyLogic;
use App\Contracts\Company\ListComOpInterface;
use App\Logic\Company\ListComOpLogic;
use App\Contracts\Company\ListCompanyBoxInterface;
use App\Logic\Company\ListCompanyBoxLogic;
use App\Contracts\Company\ListMemberInterface;
use App\Logic\Company\ListMemberLogic;
use App\Contracts\Company\ListHotCompanyInterface;
use App\Logic\Company\ListHotCompanyLogic;
use Illuminate\Support\ServiceProvider;



class CompanyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ListCompanyApplyInterface::class, ListCompanyApplyLogic::class);
        $this->app->bind(UpdateCompanyApplyInterface::class, UpdateCompanyApplyLogic::class);
        $this->app->bind(ListCompanyInterface::class, ListCompanyLogic::class);
        $this->app->bind(UpdateCompanyInterface::class, UpdateCompanyLogic::class);
        $this->app->bind(GetCompanyInterface::class, GetCompanyLogic::class);
        $this->app->bind(ListComOpInterface::class, ListComOpLogic::class);
        $this->app->bind(ListCompanyBoxInterface::class, ListCompanyBoxLogic::class);
        $this->app->bind(ListMemberInterface::class, ListMemberLogic::class);
        $this->app->bind(ListHotCompanyInterface::class, ListHotCompanyLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
