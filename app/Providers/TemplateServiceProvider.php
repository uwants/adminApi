<?php

namespace App\Providers;

use App\Contracts\WechatTemplate\ListTemplateInterface;
use App\Logic\WechatTemplate\ListTemplateLogic;
use App\Contracts\WechatTemplate\SendTemplateInterface;
use App\Logic\WechatTemplate\SendTemplateLogic;
use App\Contracts\WechatTemplate\PullTemplateInterface;
use App\Logic\WechatTemplate\PullTemplateLogic;
use App\Contracts\CallBack\CallbackInterface;
use App\Logic\CallBack\CallbackLogic;
use App\Contracts\WechatTemplate\GetMenuInterface;
use App\Logic\Applets\GetMenuLogic;
use App\Contracts\WechatTemplate\UpdateMenuInterface;
use App\Logic\Applets\UpdateMenuLogic;
use App\Contracts\WechatTemplate\GetTemplateInterface;
use App\Logic\WechatTemplate\GetTemplateLogic;

use Illuminate\Support\ServiceProvider;

class   TemplateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ListTemplateInterface::class, ListTemplateLogic::class);
        $this->app->bind(SendTemplateInterface::class, SendTemplateLogic::class);
        $this->app->bind(PullTemplateInterface::class, PullTemplateLogic::class);
        $this->app->bind(CallbackInterface::class, CallbackLogic::class);
        $this->app->bind(GetMenuInterface::class, GetMenuLogic::class);
        $this->app->bind(GetTemplateInterface::class, GetTemplateLogic::class);
        $this->app->bind(UpdateMenuInterface::class, UpdateMenuLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
