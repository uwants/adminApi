<?php

namespace App\Providers;

use App\Contracts\Opinion\ListOpinionInterface;
use App\Logic\Opinion\ListOpinionLogic;
use App\Contracts\Opinion\GetOpinionInterface;
use App\Logic\Opinion\GetOpinionLogic;
use App\Contracts\Boxes\ListBoxInterface;
use App\Logic\Boxes\ListBoxLogic;
use App\Contracts\Boxes\GetBoxInterface;
use App\Logic\Boxes\GetBoxLogic;
use App\Contracts\Boxes\ListBoxOpInterface;
use App\Logic\Boxes\ListBoxOpLogic;

use Illuminate\Support\ServiceProvider;

class OpinionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ListOpinionInterface::class, ListOpinionLogic::class);
        $this->app->bind(GetOpinionInterface::class, GetOpinionLogic::class);
        $this->app->bind(ListBoxInterface::class, ListBoxLogic::class);
        $this->app->bind(GetBoxInterface::class, GetBoxLogic::class);
        $this->app->bind(ListBoxOpInterface::class, ListBoxOpLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
