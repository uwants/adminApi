<?php

namespace App\Providers;

use App\Contracts\Index\ListIndexInterface;
use App\Logic\Index\ListIndexLogic;
use App\Contracts\Keyword\GetKeywordInterface;
use App\Contracts\Keyword\CreateKeywordInterface;
use App\Contracts\Keyword\DeleteKeywordInterface;
use App\Contracts\Keyword\DelAllKeywordInterface;
use App\Logic\Keyword\GetKeywordLogic;
use App\Logic\Keyword\DeleteKeywordLogic;
use App\Logic\Keyword\DelAllKeywordLogic;
use App\Logic\Keyword\CreateKeywordLogic;
use Illuminate\Support\ServiceProvider;

class IndexServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ListIndexInterface::class, ListIndexLogic::class);
        $this->app->bind(GetKeywordInterface::class, GetKeywordLogic::class);
        $this->app->bind(CreateKeywordInterface::class, CreateKeywordLogic::class);
        $this->app->bind(DeleteKeywordInterface::class, DeleteKeywordLogic::class);
        $this->app->bind(DelAllKeywordInterface::class, DelAllKeywordLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
