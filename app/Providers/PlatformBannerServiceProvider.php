<?php

namespace App\Providers;

use App\Contracts\PlatformBanner\CreatePlatformBannerInterface;
use App\Contracts\PlatformBanner\DeletePlatformBannerInterface;
use App\Contracts\PlatformBanner\GetPlatformBannerInterface;
use App\Contracts\PlatformBanner\ListPlatformBannerInterface;
use App\Contracts\PlatformBanner\SwitchPlatformBannerInterface;
use App\Contracts\PlatformBanner\UpdatePlatformBannerInterface;
use App\Logic\PlatformBanner\CreatePlatformBannerLogic;
use App\Logic\PlatformBanner\DeletePlatformBannerLogic;
use App\Logic\PlatformBanner\GetPlatformBannerLogic;
use App\Logic\PlatformBanner\ListPlatformBannerLogic;
use App\Logic\PlatformBanner\SwitchPlatformBannerLogic;
use App\Logic\PlatformBanner\UpdatePlatformBannerLogic;
use Illuminate\Support\ServiceProvider;

class PlatformBannerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CreatePlatformBannerInterface::class, CreatePlatformBannerLogic::class);
        $this->app->bind(DeletePlatformBannerInterface::class, DeletePlatformBannerLogic::class);
        $this->app->bind(GetPlatformBannerInterface::class, GetPlatformBannerLogic::class);
        $this->app->bind(ListPlatformBannerInterface::class, ListPlatformBannerLogic::class);
        $this->app->bind(SwitchPlatformBannerInterface::class, SwitchPlatformBannerLogic::class);
        $this->app->bind(UpdatePlatformBannerInterface::class, UpdatePlatformBannerLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
