<?php

namespace App\Providers;

use App\Contracts\Article\ListArticleInterface;
use App\Contracts\Faq\CreateFaqInterface;
use App\Contracts\Faq\DeleteFaqInterface;
use App\Contracts\Faq\GetFaqInterface;
use App\Contracts\Faq\ListFaqInterface;
use App\Contracts\Faq\SwitchFaqInterface;
use App\Contracts\Faq\UpdateFaqInterface;
use App\Logic\Article\ListArticleLogic;
use App\Contracts\Article\ListArticleAllInterface;
use App\Logic\Article\ListArticleAllLogic;
use App\Contracts\Article\GetArticleInterface;
use App\Logic\Article\GetArticleLogic;
use App\Contracts\Article\CreateArticleInterface;
use App\Logic\Article\CreateArticleLogic;
use App\Contracts\Article\DeleteArticleInterface;
use App\Logic\Article\DeleteArticleLogic;
use App\Contracts\Article\ListEditionInterface;
use App\Logic\Article\ListEditionLogic;
use App\Contracts\Article\GetEditionInterface;
use App\Logic\Article\GetEditionLogic;
use App\Contracts\Article\CreateEditionInterface;
use App\Logic\Article\CreateEditionLogic;
use App\Contracts\Article\SwitchEditionInterface;
use App\Logic\Article\SwitchEditionLogic;
use App\Contracts\Article\DeleteEditionInterface;
use App\Logic\Article\DeleteEditionLogic;
use App\Logic\Faq\CreateFaqLogic;
use App\Logic\Faq\DeleteFaqLogic;
use App\Logic\Faq\GetFaqLogic;
use App\Logic\Faq\ListFaqLogic;
use App\Logic\Faq\SwitchFaqLogic;
use App\Logic\Faq\UpdateFaqLogic;
use Illuminate\Support\ServiceProvider;

class ArticleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ListArticleInterface::class, ListArticleLogic::class);
        $this->app->bind(ListArticleAllInterface::class, ListArticleAllLogic::class);
        $this->app->bind(GetArticleInterface::class, GetArticleLogic::class);
        $this->app->bind(DeleteArticleInterface::class, DeleteArticleLogic::class);
        $this->app->bind(CreateArticleInterface::class, CreateArticleLogic::class);
        $this->app->bind(ListEditionInterface::class, ListEditionLogic::class);
        $this->app->bind(GetEditionInterface::class, GetEditionLogic::class);
        $this->app->bind(CreateEditionInterface::class, CreateEditionLogic::class);
        $this->app->bind(SwitchEditionInterface::class, SwitchEditionLogic::class);
        $this->app->bind(DeleteEditionInterface::class, DeleteEditionLogic::class);
        //faq
        $this->app->bind(CreateFaqInterface::class, CreateFaqLogic::class);
        $this->app->bind(ListFaqInterface::class, ListFaqLogic::class);
        $this->app->bind(GetFaqInterface::class, GetFaqLogic::class);
        $this->app->bind(UpdateFaqInterface::class, UpdateFaqLogic::class);
        $this->app->bind(DeleteFaqInterface::class, DeleteFaqLogic::class);
        $this->app->bind(SwitchFaqInterface::class, SwitchFaqLogic::class);


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
