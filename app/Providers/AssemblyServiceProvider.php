<?php

namespace App\Providers;

use App\Contracts\Assembly\DragInterface;
use App\Contracts\Assembly\OrganizationInterface;
use App\Contracts\Assembly\TreeInterface;
use App\Logic\Assembly\DragLogic;
use App\Logic\Assembly\OrganizationLogic;
use App\Logic\Assembly\TreeLogic;
use Illuminate\Support\ServiceProvider;

class AssemblyServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TreeInterface::class, TreeLogic::class);
        $this->app->bind(DragInterface::class, DragLogic::class);
        $this->app->bind(OrganizationInterface::class, OrganizationLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
