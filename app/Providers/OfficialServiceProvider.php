<?php

namespace App\Providers;

use App\Contracts\Website\Index\GetMessageInterface;
use App\Logic\Website\Index\GetMessageLogic;
use App\Contracts\Website\Index\ListContentInterface;
use App\Logic\Website\Index\ListContentLogic;
use App\Contracts\Website\Index\GetContentInterface;
use App\Logic\Website\Index\GetContentLogic;
use App\Contracts\Website\Index\ListSortInterface;
use App\Logic\Website\Index\ListSortLogic;
use App\Contracts\Website\Index\LoginCodeInterface;
use App\Logic\Website\Index\LoginCodeLogic;
use App\Contracts\Website\Home\GetHomeInterface;
use App\Logic\Website\Home\GetHomeLogic;


use Illuminate\Support\ServiceProvider;

class OfficialServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GetMessageInterface::class, GetMessageLogic::class);
        $this->app->bind(ListContentInterface::class, ListContentLogic::class);
        $this->app->bind(GetContentInterface::class, GetContentLogic::class);
        $this->app->bind(ListSortInterface::class, ListSortLogic::class);
        $this->app->bind(GetHomeInterface::class, GetHomeLogic::class);
        $this->app->bind(LoginCodeInterface::class, LoginCodeLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
