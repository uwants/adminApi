<?php

namespace App\Providers;

use App\Contracts\InternalUser\CreateInternalUserInterface;
use App\Contracts\InternalUser\DeleteInternalUserInterface;
use App\Contracts\InternalUser\ListInternalUserInterface;
use App\Logic\InternalUser\CreateInternalUserLogic;
use App\Logic\InternalUser\DeleteInternalUserLogic;
use App\Logic\InternalUser\ListInternalUserLogic;
use Illuminate\Support\ServiceProvider;

class InternalUserServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ListInternalUserInterface::class,ListInternalUserLogic::class);
        $this->app->bind(CreateInternalUserInterface::class,CreateInternalUserLogic::class);
        $this->app->bind(DeleteInternalUserInterface::class,DeleteInternalUserLogic::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
