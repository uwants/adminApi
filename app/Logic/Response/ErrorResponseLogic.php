<?php

namespace App\Logic\Response;

use App\Contracts\Response\ErrorResponseInterface;
use Illuminate\Http\Exceptions\HttpResponseException;

class ErrorResponseLogic implements ErrorResponseInterface
{
    private int $code = 1;

    private string $message = 'error';

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function execute(): HttpResponseException
    {
        throw new HttpResponseException(response()->json([
            'code' => $this->getCode() ?: 1,
            'message' => $this->getMessage(),
            'data' => []
        ]));
    }
}
