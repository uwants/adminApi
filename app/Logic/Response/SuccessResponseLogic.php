<?php

namespace App\Logic\Response;

use App\Contracts\Response\SuccessResponseInterface;
use Illuminate\Http\Exceptions\HttpResponseException;

class SuccessResponseLogic implements SuccessResponseInterface
{
    private string $message = 'success';

    private array $data = [];

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return HttpResponseException
     */
    public function execute(): HttpResponseException
    {
        throw new HttpResponseException(response()->json([
            'code' => 0,
            'message' => $this->getMessage(),
            'data' => $this->getData()
        ]));
    }
}
