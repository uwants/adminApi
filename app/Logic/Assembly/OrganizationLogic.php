<?php


namespace App\Logic\Assembly;


use App\Contracts\Assembly\OrganizationInterface;

class OrganizationLogic implements OrganizationInterface
{
    protected array $data = [];

    public function execute(): void
    {
        //array( 'children' => array(),'id'=>1001,'label'=>'three1'),
        $list = array(
            'children' => array(
                array(
                    'children' => array(
                        array( 'children' => array(),'id'=>1001,'label'=>'three1'),
                        array( 'children' => array(),'id'=>1002,'label'=>'three2'),
                    ),
                    'id' => 101,
                    'label'=>'two1'
                ),
                array(
                    'children' => array(
                        array( 'children' => array(),'id'=>1003,'label'=>'three3'),
                        array( 'children' => array(),'id'=>1004,'label'=>'three4'),
                    ),
                    'id' => 102,
                    'label'=>'two2'
                )
            ),
            'id' => 10,
            'label'=>'one'
        );
        $this->setData($list);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
