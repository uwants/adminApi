<?php

namespace App\Logic\Assembly;


use App\Contracts\Assembly\TreeInterface;

class TreeLogic implements TreeInterface
{
    protected array $data = [];

    public function execute(): void
    {

    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
