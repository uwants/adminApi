<?php

namespace App\Logic\Assembly;

use App\Contracts\Assembly\DragInterface;

class DragLogic implements DragInterface
{
    protected array $data = [];

    public function execute(): void
    {
        $list = [
            ['name' => '标题一', 'id' => 10],
            ['name' => '标题2', 'id' => 20],
            ['name' => '标题3', 'id' => 30],
            ['name' => '标题4', 'id' => 40],
        ];
        $list1 = ['name' => '标题一', 'id' => 10];
        $this->setData(compact('list', 'list1'));
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
