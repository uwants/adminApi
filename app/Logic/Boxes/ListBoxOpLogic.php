<?php

namespace App\Logic\Boxes;

use App\Contracts\Boxes\ListBoxOpInterface;
use App\Models\Comments;
use App\Models\Boxes;
use Illuminate\Support\Facades\DB;

class ListBoxOpLogic implements ListBoxOpInterface
{
    protected int $boxId = 0;
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $keywordState = '';
    protected string $exportData = '';
    protected string $sortColumn = '';
    protected string $sortVal = '';
    protected array $time = [];
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '用户信息', 'key' => 'userId', 'minWidth' => 250, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'sortable' => 'custom'],
            ['title' => '意见内容', 'key' => 'contentMsg', 'minWidth' => 150, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '是否已回复', 'key' => 'replayType', 'minWidth' => 110, 'align' => 'center', 'slot' => 'replay'],
            ['title' => '是否匿名', 'key' => 'anonymousType', 'minWidth' => 100, 'align' => 'center', 'slot' => 'anonymousType'],
            ['title' => '是否采纳', 'key' => 'adoptType', 'minWidth' => 100, 'align' => 'center', 'slot' => 'adopt'],
            ['title' => '是否已读', 'key' => 'readType', 'minWidth' => 100, 'align' => 'center', 'slot' => 'read'],
            ['title' => '是否删除', 'key' => 'isDelete', 'minWidth' => 100, 'align' => 'center', 'slot' => 'delete'],
            ['title' => '提出时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit'],
        ]);
        $boxMsg = Boxes::query()
            ->where('id', $this->getBoxId())
            ->first();
        $companyId = $boxMsg->company_id;
        $model = Comments::withTrashed()
            ->with('user')
            ->with(['reply' => function ($query) use ($companyId) {
                $query->with(['companyUser' => function ($query) use ($companyId) {
                    $query->where('company_id', $companyId);
                }]);
            }])
            ->where('box_id', $this->getBoxId());
        if ($this->getKeyword()) {
            switch ($this->getKeywordState()) {
                case'mobile':
                    $model->whereIn('user_id', function ($query) {
                        $query->select('id')->from('users')->where('mobile', 'like', '%' . $this->getKeyword() . '%');
                    });
                    break;
                case'nickName':
                    $model->whereIn('user_id', function ($query) {
                        $query->select('id')->from('users')->where('name', 'like', '%' . $this->getKeyword() . '%');
                    });
                    break;
            }
        }
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:s', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $model->orderBy($this->getSortColumn(), $this->getSortVal());
        } else {
            $model->orderBy('id', 'desc');
        }
        $box = $model->paginate($this->getPageSize());
        $item = $box->items();
        $this->setTotal($box->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['userId'] = $val->user_id;
            $items[$key]['content'] = json_decode($val->content);
            $items[$key]['contentMsg'] = $items[$key]['content']->content;
            $items[$key]['anonymous'] = $val->anonymous;
            $items[$key]['anonymousType'] = $val->anonymous ? '是' : '否';
            $items[$key]['adopt'] = $val->adopt;
            $items[$key]['adoptType'] = $val->adopt ? '是' : '否';
            $items[$key]['read'] = $val->read;
            $items[$key]['readType'] = $val->read ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? 1 : 0;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['userMsg'] = $val->user;
           if (!empty($val->reply)) {
                foreach ($val->reply as $ke => $va) {
                    $items[$key]['reply'][$ke]['replyName'] = empty($va->companyUser[0]) ? @$va->user->name : $va->companyUser[0]->name;
                    $items[$key]['reply'][$ke]['avatar'] = empty($va->companyUser[0]) ? @$va->user->avatar : $va->companyUser[0]->avatar;
                    $items[$key]['reply'][$ke]['replyTime'] = date($va->created_at);
                    $items[$key]['reply'][$ke]['readType'] = $va->read == 1 ? '已读' : '未读';
                    $content = json_decode($va->content);
                    $items[$key]['reply'][$ke]['replyContent'] = $content->content;
                }
            } else {
                $items[$key]['reply'] = [];
            }
            $items[$key]['replayType'] = empty($items[$key]['reply']) ? '0' : '1';
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return int
     */
    public function getBoxId(): int
    {
        return $this->boxId;
    }

    /**
     * @param int $boxId
     */
    public function setBoxId(int $boxId): void
    {
        $this->boxId = $boxId;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getKeywordState(): string
    {
        return $this->keywordState;
    }

    /**
     * @param string $keywordState
     */
    public function setKeywordState(string $keywordState): void
    {
        $this->keywordState = $keywordState;
    }

    /**
     * @return string
     */
    public function getExportData(): string
    {
        return $this->exportData;
    }

    /**
     * @param string $exportData
     */
    public function setExportData(string $exportData): void
    {
        $this->exportData = $exportData;
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
