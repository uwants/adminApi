<?php

namespace App\Logic\Boxes;

use App\Contracts\Boxes\GetBoxInterface;
use App\Models\Boxes;
use Illuminate\Support\Facades\DB;

class GetBoxLogic implements GetBoxInterface
{
    protected int $id = 0;
    protected array $data = [];

    public function execute(): void
    {
        $model = Boxes::withTrashed()
            ->select('boxes.*',
                DB::raw("(select count(id) from comments where comments.box_id=boxes.id) as number"),
                DB::raw("(select count(id) from comments where comments.box_id=boxes.id and comments.adopt=1 and comments.deleted_at is null ) as adoptNumber"),
                DB::raw("(select count(id) from replies where replies.comment_id in( select id from comments where comments.box_id=boxes.id) and replies.deleted_at is null ) as replyNumber"),
                DB::raw("(select count(id) from comments where comments.box_id=boxes.id and comments.deleted_at is not null) as delNumber"),
                DB::raw("(select count(id) from box_visits where box_visits.box_id=boxes.id and box_visits.type='share') as share"),
                DB::raw("(select count(id) from box_visits where box_visits.box_id=boxes.id and box_visits.type='qr_code') as code")
            )
            ->with('company')
            ->with('user')
            ->where('id', $this->getId())
            ->first();
        $list = [];
        $list['name'] = $model->name;
        $list['id'] = $model->id;
        $content = json_decode($model->content);
        $list['content'] = empty($content->content) ? '' : $content->content;
        $list['anonymousType'] = $model->is_allow_anonymous ? true : false;
        $list['onlyOnce'] = $model->is_only_once ? true : false;
        $list['isDelete'] = $model->deleted_at ? true : false;
        $list['deleteTime'] = empty($model->deleted_at) ? '' : date($model->deleted_at);
        $list['createTime'] = date($model->created_at);
        $list['boxStatus'] = $model->company_id > 0 ? '企业意见箱' : '个人意见箱';
        $list['number'] = $model->number;
        $list['status'] = $model->status == 'STATUS_ENABLE';
        $company = empty($model->company->name) ? '' : $model->company->name;
        $userPeople = empty($model->user->name) ? '' : $model->user->name;
        if ($company && empty($userPeople)) {
            $list['opinionPeople'] = $company;
        } elseif (empty($company) && $userPeople) {
            $list['opinionPeople'] = $userPeople;
        } elseif (!empty($company) && !empty($userPeople)) {
            $list['opinionPeople'] = $company . '/' . $userPeople;
        } else {
            $list['opinionPeople'] = '';
        }
        if ($model->company_id > 0) {
            $list['avatar'] = showImage(empty($model->company->logo) ? '' : $model->company->logo);
        } else {
            $list['avatar'] = showImage(empty($model->user->avatar) ? '' : $model->user->avatar);
        }
        $list['numOne'] = array(
            ['label' => '收到意见数量', 'num' => $model->number],
            ['label' => '正常意见总数', 'num' => $model->number - $model->delNumber],
            ['label' => '已删除意见总数', 'num' => $model->delNumber],
            ['label' => '采纳意见总数', 'num' => $model->adoptNumber],
            ['label' => '回复意见总数', 'num' => $model->replyNumber],
            ['label' => '访问次数', 'num' => $model->share + $model->code],
            ['label' => '扫码访问', 'num' => $model->share],
            ['label' => '微信分享访问', 'num' => $model->code]
        );
        $this->setData($list);
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }


}
