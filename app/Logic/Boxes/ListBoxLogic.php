<?php

namespace App\Logic\Boxes;

use App\Contracts\Boxes\ListBoxInterface;
use Illuminate\Support\Facades\DB;
use App\Models\Boxes;

class ListBoxLogic implements ListBoxInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $status = '';
    protected string $boxStatus = '';
    protected string $boxType = '';
    protected string $exportData = '';
    protected string $sortColumn = '';
    protected string $sortVal = '';
    protected array $time = [];
    protected array $data = [];
    protected array $columns = [];
    protected bool $filter = true;

    public function execute(): void
    {
        if ($this->getExportData() == 1) {
            ini_set('memory_limit', '1000M');
            set_time_limit(0);
            $this->setColumns([
                ['title' => '意见箱名字', 'key' => 'name',],
                ['title' => '创建者', 'key' => 'opinionPeople',],
                ['title' => '创建者手机', 'key' => 'opinionPeopleMobile',],
                ['title' => '意见箱类型', 'key' => 'boxStatus'],
                ['title' => '参与人数', 'key' => 'people'],
                ['title' => '收到意见', 'key' => 'number'],
            ]);
        } else {
            $this->setColumns([
                ['title' => '信息', 'key' => 'id', 'minWidth' => 250, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'fixed' => 'left', 'sortable' => 'custom'],
                ['title' => '意见箱类型', 'key' => 'boxStatus', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '意见内容', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '参与人数', 'key' => 'people', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '收到意见', 'key' => 'number', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '允许匿名', 'key' => 'anonymousType', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '仅限一次', 'key' => 'onlyOnce', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '是否删除', 'key' => 'isDelete', 'minWidth' => 100, 'align' => 'center', 'slot' => 'delete'],
                ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
            ]);
        }

        $model = Boxes::withTrashed()
            ->select('name', 'id', 'user_id', 'content', 'status', 'company_id', 'is_only_once', 'is_allow_anonymous', 'deleted_at', 'created_at',
                DB::raw("(select count(id) from comments where comments.box_id=boxes.id and deleted_at is null) as number"),
                DB::raw("(select count(*) from (select DISTINCT(user_id) from comments where comments.box_id=boxes.id and deleted_at is null) as table_source) as people")
            )
            ->with('company')
            ->with('user');
        //内部人员意见箱过滤、同时意见总数不过滤内部人员的意见数。
        if ($this->isFilter()) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        //关键字查找
        if ($this->getKeyword()) {
            switch ($this->getStatus()) {
                case'name':
                    $model->where('name', 'like', '%' . $this->getKeyword() . '%');
                    break;
                case'user':
                    $model->whereIn('user_id', function ($query) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $this->getKeyword() . '%');
                    })
                        ->orWhereIn('company_id', function ($query) {
                            $query->select('id')
                                ->from('companies')
                                ->where('name', 'like', '%' . $this->getKeyword() . '%');
                        });
                    break;
            }
        }
        switch ($this->getBoxStatus()) {
            case'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        switch ($this->getBoxType()) {
            case'userBox':
                $model->where('company_id', 0);
                break;
            case'companyBox':
                $model->where('company_id', '>', 0);
                break;
        }
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:S', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:S', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $model->orderBy($this->getSortColumn(), $this->getSortVal());
        } else {
            $model->orderBy('id', 'desc');
        }
        if ($this->getExportData() == 1) {
            $item = $model->get();
            $items = [];
            $columns = $this->getColumns();
            foreach ($item as $key => $val) {
                $items[$key]['name'] = $val->name;
                $items[$key]['opinionPeople'] = empty($val->user->name) ? '' : $val->user->name;
                $items[$key]['opinionPeopleMobile'] = empty($val->user->mobile) ? '' : $val->user->mobile;
                $items[$key]['boxStatus'] = $val->company_id > 0 ? '企业意见箱' : '个人意见箱';
                $items[$key]['people'] = $val->people;
                $items[$key]['number'] = $val->number;
            }
            $this->setData(compact('items', 'columns'));
            return;
        } else {
            $box = $model->paginate($this->getPageSize());
            $item = $box->items();
            $this->setTotal($box->total());
        }
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $content = json_decode($val->content);
            $items[$key]['content'] = empty($content->content) ? '' : $content->content;
            $items[$key]['anonymousType'] = $val->is_allow_anonymous ? '是' : '否';
            $items[$key]['onlyOnce'] = $val->is_only_once ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? 1 : 0;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['boxStatus'] = $val->company_id > 0 ? '企业意见箱' : '个人意见箱';
            $items[$key]['number'] = $val->number;
            $items[$key]['people'] = $val->people;
            $company = empty($val->company->name) ? '' : $val->company->name;
            $userPeople = empty($val->user->name) ? '' : $val->user->name;
            if ($company && empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company;
            } elseif (empty($company) && $userPeople) {
                $items[$key]['opinionPeople'] = $userPeople;
            } elseif (!empty($company) && !empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company . '/' . $userPeople;
            } else {
                $items[$key]['opinionPeople'] = '';
            }
            if ($val->company_id > 0) {
                $items[$key]['avatar'] = showImage(empty($val->company->logo) ? '' : $val->company->logo);
            } else {
                $items[$key]['avatar'] = showImage(empty($val->user->avatar) ? '' : $val->user->avatar);
            }

        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    public function explode(): void
    {
        $this->setColumns([
            ['title' => '意见箱ID', 'key' => 'id',],
            ['title' => '意见箱名字', 'key' => 'name',],
            ['title' => '创建者', 'key' => 'opinionPeople',],
            ['title' => '意见内容', 'key' => 'content'],
            ['title' => '意见箱类型', 'key' => 'boxStatus'],
            ['title' => '允许匿名', 'key' => 'anonymousType'],
            ['title' => '仅限一次', 'key' => 'onlyOnce'],
            ['title' => '是否删除', 'key' => 'isDelete'],
            ['title' => '收到意见', 'key' => 'number'],
            ['title' => '创建时间', 'key' => 'createTime'],
        ]);
        $model = Boxes::withTrashed()
            ->select('*', DB::raw("(select count(id) from comments where comments.box_id=boxes.id) as number"))
            ->with('company')
            ->with('user');
        //关键字查找
        if ($this->getKeyword()) {
            switch ($this->getStatus()) {
                case'name':
                    $model->where('name', 'like', '%' . $this->getKeyword() . '%');
                    break;
                case'user':
                    $model->whereIn('user_id', function ($query) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $this->getKeyword() . '%');
                    })
                        ->orWhereIn('company_id', function ($query) {
                            $query->select('id')
                                ->from('companies')
                                ->where('name', 'like', '%' . $this->getKeyword() . '%');
                        });
                    break;
            }
        }
        switch ($this->getBoxStatus()) {
            case'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        switch ($this->getBoxType()) {
            case'userBox':
                $model->where('company_id', 0);
                break;
            case'companyBox':
                $model->where('company_id', '>', 0);
                break;
        }
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:S', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:S', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        $model->orderBy('id', 'desc');
        $item = $model->get();
        $columns = $this->getColumns();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $content = json_decode($val->content);
            $items[$key]['content'] = empty($content->content) ? '' : str_replace([',', '\n', '\r\n', '\r'], '', $content->content);
            $items[$key]['anonymousType'] = $val->is_allow_anonymous ? '是' : '否';
            $items[$key]['onlyOnce'] = $val->is_only_once ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? '是' : '否';
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['boxStatus'] = $val->company_id > 0 ? '企业意见箱' : '个人意见箱';
            $items[$key]['number'] = $val->number;
            $company = empty($val->company->name) ? '' : $val->company->name;
            $userPeople = empty($val->user->name) ? '' : $val->user->name;
            if ($company && empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company;
            } elseif (empty($company) && $userPeople) {
                $items[$key]['opinionPeople'] = $userPeople;
            } elseif (!empty($company) && !empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company . '/' . $userPeople;
            } else {
                $items[$key]['opinionPeople'] = '';
            }
            if ($val->company_id > 0) {
                $items[$key]['avatar'] = showImage(empty($val->company->logo) ? '' : $val->company->logo);
            } else {
                $items[$key]['avatar'] = showImage(empty($val->user->avatar) ? '' : $val->user->avatar);
            }

        }
        $this->setData(compact('items', 'columns'));
    }

    /**
     * @return string
     */
    public function getExportData(): string
    {
        return $this->exportData;
    }

    /**
     * @param string $exportData
     */
    public function setExportData(string $exportData): void
    {
        $this->exportData = $exportData;
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getBoxStatus(): string
    {
        return $this->boxStatus;
    }

    /**
     * @param string $boxStatus
     */
    public function setBoxStatus(string $boxStatus): void
    {
        $this->boxStatus = $boxStatus;
    }

    /**
     * @return string
     */
    public function getBoxType(): string
    {
        return $this->boxType;
    }

    /**
     * @param string $boxType
     */
    public function setBoxType(string $boxType): void
    {
        $this->boxType = $boxType;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->filter;
    }

    /**
     * @param bool $filter
     */
    public function setFilter(bool $filter): void
    {
        $this->filter = $filter;
    }

}
