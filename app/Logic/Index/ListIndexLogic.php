<?php

namespace App\Logic\Index;

use App\Contracts\Index\ListIndexInterface;
use App\Models\Companies;
use App\Models\User;
use App\Models\Comments;
use App\Models\Boxes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class ListIndexLogic implements ListIndexInterface
{
    protected array $data = [];

    public function execute(): void
    {
        //个人用户量
        $userNum = user::query()
            ->whereNotIn('id', function ($query) {
                $query->select('user_id')->from('internal_users')
                ->where('deleted_at',null);
            })->count();
        //企业用户量
        $companyNum = Companies::query()
            ->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at',null);
            })
            ->where('certified_status', 'STATUS_CERTIFIED_PASS')
            ->count();
        //总访问量
        $countVisit = DB::table('box_visits')
            ->whereNotIn('box_id', function ($query) {
                $query->select('id')->from('boxes')
                    ->whereIn('user_id', function ($queryBox) {
                        $queryBox->select('user_id')->from('internal_users')
                            ->where('deleted_at',null);
                    });
            })
            ->count('id');
        //提意见总量
        $commentNum = Comments::query()
            ->whereNotIn('box_id', function ($query) {
                $query->select('id')->from('boxes')
                    ->whereIn('user_id', function ($queryBox) {
                        $queryBox->select('user_id')->from('internal_users')
                        ->where('deleted_at',null);
                    });
            })
            ->count();
        //发征集总量
        $solicitationNum = Boxes::query()
            ->whereNotIn('user_id', function ($query) {
                $query->select('id')->from('internal_users');
            })
            ->count();
        $header = [
            ['title' => '个人用户量', 'icon' => 'md-person-add', 'count' => $userNum, 'color' => '#2d8cf0'],
            ['title' => '企业用户量', 'icon' => 'md-home', 'count' => $companyNum, 'color' => '#19be6b'],
            ['title' => '意见访问量', 'icon' => 'ios-eye', 'count' => $countVisit, 'color' => '#ff9900'],
            ['title' => '提意见总量', 'icon' => 'ios-chatbubbles', 'count' => $commentNum, 'color' => '#ed3f14'],
            ['title' => '意见箱总量', 'icon' => 'ios-cube', 'count' => $solicitationNum, 'color' => '#E46CBB'],
        ];
        //今日数据统计
        $todayNumber = $this->getTodayNum();
        //上周数据统计
        //新用户增加量
        $userWeek = $this->getUserNum();
        //新认证企业数量
        $companyWeek = $this->getCompanyNum();
        //企业用户状态占比
        $return_data = $this->getCompanyStatic();
        $companyStatic = $return_data['data'];
        $companyAll = '企业总数量：' . $return_data['all'];
        $sumTime = date('Y-m-d H:i:s', time());
        $this->setData(compact('sumTime', 'header', 'userWeek', 'companyWeek', 'companyStatic', 'companyAll', 'todayNumber'));
    }

    public function getTodayNum()
    {
        $time = time();
        $todayStart = date('Y-m-d 00:00:00', $time);
        $todayEnd = date('Y-m-d H:i:s', $time);
        //今日新增用户
        $user = User::query()
            ->select(
                DB::raw("(select count(id) from users where created_at between '$todayStart' and '$todayEnd' and deleted_at is null) as total "),
                'name', 'avatar', 'id')
            ->whereBetween('created_at', [$todayStart, $todayEnd])
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get()->toArray();
        $userAll = User::query()->count('id');//所有用户
        $total = empty($user[0]['total']) ? 0 : $user[0]['total'];
        $todayUser = ['total' => $total, 'totalAll' => $userAll, 'message' => $user];
        //今日意见箱访问量
        $box = DB::table('box_visits')
            ->select(
                DB::raw("(select count(id) from box_visits where  deleted_at is null) as boxAll"),
                DB::raw("(select count(id) from box_visits where created_at between '$todayStart' and '$todayEnd' and deleted_at is null) as boxNum"),
                DB::raw("(select count(id) from box_visits where created_at between '"
                    . date('Y-m-d 00:00:00', strtotime("-1 day")) .
                    "' and '" .
                    date('Y-m-d H:i:s', $time - 24 * 3600) . "' and deleted_at is null) as boxYd"),
                DB::raw("(select count(id) from box_visits where created_at between '"
                    . date('Y-m-d 00:00:00', strtotime("-7 day")) .
                    "' and '" .
                    date('Y-m-d H:i:s', $time - 24 * 3600) . "' and deleted_at is null ) as boxWd"),
            )
            ->first();
        $boxYd = $box->boxYd > 0 ? ($box->boxNum - $box->boxYd) / $box->boxYd * 100 : ($box->boxNum > 0 ? $box->boxNum * 100 : 0);
        $boxWd = $box->boxWd > 0 ? ($box->boxNum - $box->boxWd) / $box->boxWd * 100 : ($box->boxNum > 0 ? $box->boxNum * 100 : 0);
        //同比增长率=（本年的指标值-去年同期的值）÷去年同期的值*100%
        $todayVisit = ['total' => $box->boxNum, 'totalAll' => $box->boxAll, 'boxYd' => $boxYd, 'boxWd' => $boxWd, 'box' => $box];
        //今日意见同比
        $comments = Comments::query()
            ->select(
                DB::raw("(select count(id) from comments where  deleted_at is null) as comAll"),
                DB::raw("(select count(id) from comments where created_at between '$todayStart' and '$todayEnd' and deleted_at is null) as comNum"),
                DB::raw("(select count(id) from comments where created_at between '"
                    . date('Y-m-d 00:00:00', strtotime("-1 day")) .
                    "' and '" .
                    date('Y-m-d H:i:s', $time - 24 * 3600) . "' and deleted_at is null) as comYd"),
                DB::raw("(select count(id) from comments where created_at between '"
                    . date('Y-m-d 00:00:00', strtotime("-7 day")) .
                    "' and '" .
                    date('Y-m-d H:i:s', $time - 24 * 3600) . "' and deleted_at is null ) as comWd"),
            )
            ->first();
        $comYd = $comments->comYd > 0 ? ($comments->comNum - $comments->comYd) / $comments->comYd * 100 : ($comments->comNum > 0 ? $comments->comNum * 100 : 0);
        $comWd = $comments->comWd > 0 ? ($comments->comNum - $comments->comWd) / $comments->comWd * 100 : ($comments->comNum > 0 ? $comments->comNum * 100 : 0);

        $todayCom = ['total' => $comments->comNum, 'totalAll' => $comments->comAll, 'comYd' => $comYd, 'comWd' => $comWd, 'com' => $comments];
        //收到意见数量
        $todayOp = Comments::query()
            ->with(['box'=>function($query){
                return call_user_func([$query, 'withTrashed']);
            }])
            ->select(DB::raw("count(id) as num"), 'box_id')
            ->whereBetween('created_at', [$todayStart, $todayEnd])
            ->groupBy('box_id')
            ->orderBy('num', 'desc')
            ->limit(10)
            ->get()->toArray();
        return ['todayUser' => $todayUser, 'visit' => $todayVisit,'com'=>$todayCom, 'op' => $todayOp];
    }

    /**
     * @return array
     * 上周新注册用户数
     */
    public function getUserNum()
    {
        $arrayTime = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'];
        $showMes = [];
        $user = Cache::get('number_list_userWeek');
        if ($user) {
            $showMes = $user;
        } else {
            for ($i = 1; $i <= 7; $i++) {
                $start = date("Y-m-d 00:00:00", strtotime("last week") + 24 * 3600 * ($i - 1));
                $end = date("Y-m-d 23:59:59", strtotime("last week") + 24 * 3600 * ($i - 1));
                $user = user::query()
                    ->whereNotIn('id', function ($query) {
                        $query->select('user_id')->from('internal_users');
                    })
                    ->whereBetween('created_at', [$start, $end])->count();
                $showMes[$arrayTime[$i - 1]] = $user;
            }
            Cache::put('number_list_userWeek', $showMes, 3600 * 2);
        }
        return $showMes;
    }

    /**
     * @return array
     * 上周新增企业数
     */
    public function getCompanyNum()
    {
        $arrayTime = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期日'];
        $showMes = [];
        $company = Cache::get('number_list_companyWeek');
        if ($company) {
            $showMes = $company;
        } else {
            for ($i = 1; $i <= 7; $i++) {
                $start = date("Y-m-d 00:00:00", strtotime("last week") + 24 * 3600 * ($i - 1));
                $end = date("Y-m-d 23:59:59", strtotime("last week") + 24 * 3600 * ($i - 1));
                $user = Companies::query()
                    ->whereNotIn('user_id', function ($query) {
                        $query->select('user_id')->from('internal_users');
                    })
                    ->where('user_id', '>', 0)
                    ->whereBetween('created_at', [$start, $end])->count();
                $showMes[$arrayTime[$i - 1]] = $user;
            }
            Cache::put('number_list_companyWeek', $showMes, 3600 * 2);
        }
        return $showMes;
    }

    /**
     * @return array
     * 企业用户状态占比
     */
    public function getCompanyStatic()
    {
        $company = Companies::query()->count();
        //认证数量
        $ren_num = Companies::query()
            ->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                ->where('deleted_at',null);
            })
            ->where('certified_status', 'STATUS_CERTIFIED_PASS')
            ->count();
        //未认证
        $no_num = $company - $ren_num;
        $mes = [
            //['value'=>$company,'name'=>'企业总数'],
            ['value' => $ren_num, 'name' => '已认证', 'title' => 'ddd'],
            ['value' => $no_num, 'name' => '未认证'],
        ];
        return ['all' => $company, 'data' => $mes];
    }

    /**
     * 最近30天意见数据统计
     */
    public function thirtyOp(): void
    {
        $timeDate = [];
        $solicitation_user = [];//个人意见箱
        $solicitation_com = [];//企业意见箱
        $one = Cache::get('list_solicitation_user');
        $two = Cache::get('list_solicitation_com');
        $three = Cache::get('list_solicitation_time');
        if ($one && $two) {
            $solicitation_user = $one;
            $solicitation_com = $two;
            $timeDate = $three;
        } else {
            for ($i = 30; $i >= 0; $i--) {
                $start = date("Y-m-d 00:00:00", strtotime("-$i day"));
                $end = date("Y-m-d 23:59:59", strtotime("-$i day"));
                $timeDate[] = date('m-d', strtotime("-$i day"));
                $solicitation_user[] = Comments::query()
                    ->whereIn('box_id', function ($query) {
                        $query->select('id')
                            ->from('boxes')
                            ->where('user_id', '>', 0)
                            ->where('company_id', 0);
                    })
                    ->whereNotIn('user_id', function ($query) {
                        $query->select('user_id')->from('internal_users');
                    })
                    ->whereBetween('created_at', [$start, $end])
                    ->count();
                $solicitation_com[] = Comments::query()
                    ->whereIn('box_id', function ($query) {
                        $query->select('id')
                            ->from('boxes')
                            ->where('company_id', '>', 0);
                    })
                    ->whereNotIn('user_id', function ($query) {
                        $query->select('user_id')->from('internal_users');
                    })
                    ->whereBetween('created_at', [$start, $end])
                    ->count();
            }
            Cache::put('list_solicitation_user', $solicitation_user, 3600 * 2);
            Cache::put('list_solicitation_com', $solicitation_com, 3600 * 2);
            Cache::put('list_solicitation_time', $timeDate, 3600 * 2);
        }
        $this->setData(compact('timeDate', 'solicitation_user', 'solicitation_com'));
    }

    /**
     * 最近30天意见箱数量
     */
    public function thirtyBox(): void
    {
        $arrayTime = [];
        $user = [];
        $company = [];
        $one = Cache::get('number_list_solicitation_user');
        $two = Cache::get('number_list_solicitation_company');
        $three = Cache::get('number_list_solicitation_time');
        if ($one && $two) {
            $user = $one;
            $company = $two;
            $arrayTime = $three;
        } else {
            for ($i = 30; $i >= 0; $i--) {
                $start = date("Y-m-d 00:00:00", strtotime("-$i day"));
                $end = date("Y-m-d 23:59:59", strtotime("-$i day"));
                $userBox = Boxes::query()
                    ->whereNotIn('user_id', function ($query) {
                        $query->select('user_id')->from('internal_users');
                    })
                    ->where('user_id', '>', 0)
                    ->where('company_id', 0)
                    ->whereBetween('created_at', [$start, $end])
                    ->count();
                $companyBox = Boxes::query()
                    ->whereNotIn('user_id', function ($query) {
                        $query->select('user_id')->from('internal_users');
                    })
                    ->where('company_id', '>', 0)
                    ->whereBetween('created_at', [$start, $end])
                    ->count();
                $arrayTime[] = date('m-d', strtotime("-$i day"));
                $user[] = $userBox;
                $company[] = $companyBox;
            }
            Cache::put('number_list_solicitation_user', $user, 3600 * 2);
            Cache::put('number_list_solicitation_company', $company, 3600 * 2);
            Cache::put('number_list_solicitation_time', $arrayTime, 3600 * 2);
        }
        $this->setData(compact('user', 'company', 'arrayTime'));
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
