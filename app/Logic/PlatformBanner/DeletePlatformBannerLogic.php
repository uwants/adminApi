<?php


namespace App\Logic\PlatformBanner;


use App\Contracts\PlatformBanner\DeletePlatformBannerInterface;
use App\Models\PlatformBanner;

class DeletePlatformBannerLogic implements DeletePlatformBannerInterface
{

    public function execute(): void
    {
        $model = PlatformBanner::query()
            ->findOrFail($this->getId());
        $model->delete();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    private int $id = 0;

}