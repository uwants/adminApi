<?php


namespace App\Logic\PlatformBanner;


use App\Contracts\PlatformBanner\CreatePlatformBannerInterface;
use App\Models\PlatformBanner;

class CreatePlatformBannerLogic implements CreatePlatformBannerInterface
{
    public function execute(): void
    {
        $model = new PlatformBanner();
        $model->fill([
            'image' => $this->getImage(),
            'url' => $this->getUrl(),
//            'platform_code' => $this->getPlatformCode(),
            'platform_code' => 'MP-DINGTALK',
            'sort' => $this->getSort(),
            'suspend' => $this->isSuspend(),
        ]);
        $model->save();
        $this->setId($model->getKey());
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }


    /**
     * @return bool
     */
    public function isSuspend(): bool
    {
        return $this->suspend;
    }

    /**
     * @param bool $suspend
     */
    public function setSuspend(bool $suspend): void
    {
        $this->suspend = $suspend;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPlatformCode(): string
    {
        return $this->platformCode;
    }

    /**
     * @param string $platformCode
     */
    public function setPlatformCode(string $platformCode): void
    {
        $this->platformCode = $platformCode;
    }

    private string $url = '';

    private string $image = '';

    private bool $suspend = false;

    private int $sort = 0;

    private int $id = 0;

    private string $platformCode = '';
}