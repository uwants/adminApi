<?php


namespace App\Logic\PlatformBanner;


use App\Contracts\PlatformBanner\ListPlatformBannerInterface;
use App\Models\PlatformBanner;
use Illuminate\Database\Eloquent\Builder;

class ListPlatformBannerLogic implements ListPlatformBannerInterface
{
    public function execute(): void
    {
        $platformCode = $this->getPlatformCode();
        $model = PlatformBanner::query()
            ->orderBy('sort', 'desc')
            ->orderBy('created_at', 'desc')
            ->when(!empty($platformCode), function ($query) use ($platformCode) {
                $query->where('platform_code', $platformCode);
            })
            ->paginate($this->getPageSize(), ['*'], 'page', $this->getPage());
        $list = $model->items();
        $items = [];
        foreach ($list as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['image'] = $val->image;
            $items[$key]['url'] = $val->url;
            $items[$key]['sort'] = $val->sort;
            $items[$key]['platformCode'] = $val->platform_code;
            if ($val->suspend == 0) {
                $items[$key]['status'] = true;
            } else {
                $items[$key]['status'] = false;
            }
        }
        $this->setColumns([
                ['title' => 'ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '图片', 'key' => 'image', 'minWidth' => 200, 'align' => 'center', 'slot' => 'image'],
                ['title' => '链接', 'key' => 'url', 'minWidth' => 120, 'align' => 'center'],
                ['title' => '排序', 'key' => 'sort', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '类型', 'key' => 'platformCode', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '状态', 'key' => 'status', 'minWidth' => 80, 'align' => 'center', 'slot' => 'switch'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 180, 'align' => 'center', 'slot' => 'edit'],
            ]
        );
        $total = $model->total();
        $page = $this->getPage();
        $pageSize = $this->getPageSize();
        $columns = $this->getColumns();
        $this->setData(compact('items', 'total', 'page', 'pageSize', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return string
     */
    public function getPlatformCode(): string
    {
        return $this->platformCode;
    }

    /**
     * @param string $platformCode
     */
    public function setPlatformCode(string $platformCode): void
    {
        $this->platformCode = $platformCode;
    }


    private int $page = 1;

    private int $pageSize = 10;

    private array $data = [];

    private array $columns = [];

    private string $platformCode = '';
}