<?php

namespace App\Logic\Operate;

use App\Contracts\Operate\ActivityInterface;
use App\Models\Activities;

class ActivityLogic implements ActivityInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '活动id', 'key' => 'id', 'minWidth' => 80, 'align' => 'center'],
            ['title' => '活动名称', 'key' => 'name', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '活动开始时间', 'key' => 'startTime', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '活动结束时间', 'key' => 'endTime', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '状态', 'key' => 'status', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 120, 'align' => 'center', 'slot' => 'edit'],
        ]);
        $activity = Activities::query();
        if ($this->getKeyword()) {
            $activity->where('name', 'like', '%' . $this->getKeyword() . '%');
        }
        $user = $activity->paginate($this->getPageSize());
        $item = $user->items();
        $this->setTotal($user->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['name'] = $val->name;
            $items[$key]['startTime'] = date($val->start);
            $items[$key]['endTime'] = date($val->end);
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['suspend'] = $val->suspend == 1;
            $items[$key]['status'] = $val->suspend == 1 ? '暂停' : '进行中';
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
