<?php

namespace App\Logic\Operate;

use App\Contracts\Operate\CreateActivityInterface;
use App\Models\Activities;

class CreateActivityLogic implements CreateActivityInterface
{
    protected array $activity = [];
    protected array $data = [];

    public function execute(): void
    {
        if (!$this->getActivity()) {
            return;
        }
        if ($this->activity['id'] > 0) {
            $message = Activities::query()
                ->where('id', $this->activity['id'])
                ->update([
                    'name' => $this->activity['name'],
                    'suspend' => $this->activity['suspend'],
                    'start' => date('Y-m-d',strtotime($this->activity['time'][0])),
                    'end' => date('Y-m-d',strtotime($this->activity['time'][1])),
                ]);
        } else {
            $message = Activities::query()
                ->insert([
                    'name' => $this->activity['name'],
                    'start' => date('Y-m-d',strtotime($this->activity['time'][0])),
                    'end' => date('Y-m-d',strtotime($this->activity['time'][1])),
                    'suspend' => $this->activity['suspend'],
                    'created_at' => date('Y-m-d H:i:s',time()),
                    'updated_at' => date('Y-m-d H:i:s',time()),
                ]);
        }
        $this->setData(compact('message'));
    }

    /**
     * @return array
     */
    public function getActivity(): array
    {
        return $this->activity;
    }

    /**
     * @param array $activity
     */
    public function setActivity(array $activity): void
    {
        $this->activity = $activity;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
