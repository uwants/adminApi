<?php

namespace App\Logic\Article;

use App\Contracts\Article\ListArticleAllInterface;
use App\Models\Articles;

class ListArticleAllLogic implements ListArticleAllInterface
{
    protected array $data = [];

    public function execute(): void
    {
        $model = Articles::query()
            ->select('id', 'title')
            ->get()
            ->toArray();
        $this->setData($model);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
