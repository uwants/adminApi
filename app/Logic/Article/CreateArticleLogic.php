<?php

namespace App\Logic\Article;

use App\Contracts\Article\CreateArticleInterface;
use App\Models\Articles;

class CreateArticleLogic implements CreateArticleInterface
{
    protected int $id = 0;
    protected string $title = '';
    protected string $content = '';
    protected string $cat = '';
    protected string $isOpen = '';

    public function execute(): void
    {
        if ($this->getId() > 0) {
            Articles::query()
                ->where('id', $this->getId())
                ->update([
                    'title' => $this->getTitle(),
                    'type' => $this->getCat(),
                    'suspend' => $this->getIsOpen() ? 1 : 0,
                    'content' => $this->getContent()
                ]);
            $this->setId($this->getId());
        } else {
            $model = Articles::query()
                ->firstOrCreate([
                    'title' => $this->getTitle(),
                    'type' => $this->getCat(),
                    'suspend' => $this->getIsOpen() ? 1 : 0,
                    'content' => $this->getContent(),
                ]);
            $this->setId($model->getOriginal('id'));
        }
    }

    /**
     * @return string
     */
    public function getCat(): string
    {
        return $this->cat;
    }

    /**
     * @param string $cat
     */
    public function setCat(string $cat): void
    {
        $this->cat = $cat;
    }

    /**
     * @return string
     */
    public function getIsOpen(): string
    {
        return $this->isOpen;
    }

    /**
     * @param string $isOpen
     */
    public function setIsOpen(string $isOpen): void
    {
        $this->isOpen = $isOpen;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }


}
