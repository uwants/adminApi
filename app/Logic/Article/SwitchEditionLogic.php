<?php

namespace App\Logic\Article;

use App\Contracts\Article\SwitchEditionInterface;
use App\Models\Articles;

class SwitchEditionLogic implements SwitchEditionInterface
{
    protected int $id = 0;
    protected string $status = '';
    protected array $data = [];

    public function execute(): void
    {
        $time = date('Y-m-d H:i:s', time());
        Articles::query()
            ->where('id', $this->getId())
            ->update([
                'suspend' => $this->getStatus() == true ? 0 : 1,
            ]);
        $this->setData(compact('time'));
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }


}
