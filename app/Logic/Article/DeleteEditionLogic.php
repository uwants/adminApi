<?php

namespace App\Logic\Article;

use App\Contracts\Article\DeleteEditionInterface;
use App\Models\Articles;

class DeleteEditionLogic implements DeleteEditionInterface
{
    protected int $id = 0;

    public function execute(): void
    {
        Articles::query()
            ->where('id', '=', $this->getId())
            ->delete();
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}
