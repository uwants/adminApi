<?php

namespace App\Logic\Article;

use App\Contracts\Article\DeleteArticleInterface;
use App\Models\Articles;

class DeleteArticleLogic implements DeleteArticleInterface
{
    protected int $id = 0;

    public function execute(): void
    {
        Articles::query()
            ->where('id', '=', $this->getId())
            ->delete();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


}
