<?php

namespace App\Logic\Article;

use App\Contracts\Article\GetEditionInterface;
use App\Models\Articles;

class GetEditionLogic implements GetEditionInterface
{
    protected int $id = 0;
    protected array $data = [];

    public function execute(): void
    {
        if ($this->getId() <= 0 || empty($this->getId())) {
            $this->setData(['id' => 0, 'title' => '', 'content' => '','status'=>true,'sort'=>0,'createTime'=>date('Y-m-d H:i:s',time())]);
        } else {
            $model = Articles::query()
                ->findOrFail($this->getId());
            $data = $model->toArray();
            $data['createTime'] = date($model->created_at);
            if ($model->suspend == 0) {
                $data['status'] = true;
            } else {
                $data['status'] = false;
            }
            $this->setData($data);
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
