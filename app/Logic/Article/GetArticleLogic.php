<?php

namespace App\Logic\Article;

use App\Contracts\Article\GetArticleInterface;
use App\Models\Articles;

class GetArticleLogic implements GetArticleInterface
{
    protected int $id = 0;
    protected array $data = [];


    public function execute(): void
    {
        if ($this->getId() <= 0 || empty($this->getId())) {
            $this->setData(['id' => 0, 'title' => '', 'content' => '', 'cat' => 'ABOUT_US', 'isOpen' => true]);
        } else {
            $model = Articles::query()
                ->select('id', 'title', 'content', 'created_at', 'type as cat', 'suspend')
                ->findOrFail($this->getId());
            $data = $model->toArray();
            $returnList['content'] = $data['content'];
            $returnList['id'] = $data['id'];
            $returnList['cat'] = $data['cat'];
            $returnList['title'] = $data['title'];
            $returnList['isOpen'] = $data['content'] ? true : false;
            $returnList['createTime'] = date($data['created_at']);
            $this->setData($returnList);
        }
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }


}
