<?php


namespace App\Logic\Article;

use App\Contracts\Article\CreateEditionInterface;
use App\Models\Articles;

class CreateEditionLogic implements CreateEditionInterface
{
    protected int $id = 0;
    protected string $title = '';
    protected string $content = '';
    protected string $status = '';
    protected string $createTime = '';
    protected int $sort = 0;

    public function execute(): void
    {
        if ($this->getId() > 0) {
            Articles::query()
                ->where('id', $this->getId())
                ->update([
                    'title' => $this->getTitle(),
                    'content' => $this->getContent(),
                    'sort' => $this->getSort(),
                    'suspend' => $this->getStatus() == true ? 0 : 1,
                    'created_at' => date('Y-m-d H:i:s', strtotime($this->getCreateTime()))
                ]);
            $this->setId($this->getId());
        } else {
            $model = Articles::query()
                ->firstOrCreate([
                    'type' => 'VERSION',
                    'title' => $this->getTitle(),
                    'content' => $this->getContent(),
                    'sort' => $this->getSort(),
                    'suspend' => $this->getStatus() == true ? 0 : 1,
                    'created_at' => empty($this->getCreateTime()) ? date('Y-m-d H:i:s', time()) : date('Y-m-d H:i:s', strtotime($this->getCreateTime()))
                ]);
            $this->setId($model->getOriginal('id'));
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCreateTime(): string
    {
        return $this->createTime;
    }

    /**
     * @param string $createTime
     */
    public function setCreateTime(string $createTime): void
    {
        $this->createTime = $createTime;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

}
