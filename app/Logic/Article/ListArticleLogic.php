<?php

namespace App\Logic\Article;

use App\Contracts\Article\ListArticleInterface;
use App\Models\Articles;

class ListArticleLogic implements ListArticleInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '文章ID', 'key' => 'id', 'minWidth' => 50, 'align' => 'center'],
            ['title' => '文章标题', 'key' => 'title', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 220, 'align' => 'center', 'slot' => 'edit'],
        ]);
        $model = Articles::query()
            ->select('id', 'title', 'content', 'created_at')
            ->where('type','!=','VERSION');
        //订单关键字查找
        if ($this->getKeyword()) {
            $model->where('title', 'like', "%" . $this->getKeyword() . "%");
        }
        $model->orderBy('id', 'desc');
        $article = $model->paginate($this->getPageSize());
        $item = $article->items();
        $this->setTotal($article->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['title'] = $val->title;
            $items[$key]['content'] = $val->content;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }


}
