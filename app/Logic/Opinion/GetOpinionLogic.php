<?php

namespace App\Logic\Opinion;

use App\Contracts\Opinion\GetOpinionInterface;
use App\Models\Comments;

class GetOpinionLogic implements GetOpinionInterface
{
    protected int $id = 0;
    protected array $data = [];

    public function execute(): void
    {
        $model = Comments::withTrashed()
            ->with(['box' => function ($query) {
                $query->with('company')
                    ->with('user');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user')
            ->with(['reply' => function ($query) {
                $query->with('user');
            }])
            ->where('id', $this->getId())
            ->first();
        $list = [];
        $content = json_decode($model->content);
        $list['content'] = empty($content->content) ? '' : $content->content;
        $list['images'] = empty($content->images) ? [] : $content->images;
        $list['title'] = empty($content->title) ? '' : $content->title;
        $list['video'] = empty($content->videos) ? '' : $content->videos[0];
        $company = empty($model->box->company->name) ? '' : $model->box->company->name;
        $userPeople = empty($model->box->user->name) ? '' : $model->box->user->name;
        if ($company && empty($userPeople)) {
            $list['opinionPeople'] = $company;
        } elseif (empty($company) && $userPeople) {
            $list['opinionPeople'] = $userPeople;
        } elseif (!empty($company) && !empty($userPeople)) {
            $list['opinionPeople'] = $company . '/' . $userPeople;
        } else {
            $list['opinionPeople'] = '';
        }
        $list['opinionStatus'] = @$model->box->company_id > 0 ? '企业意见箱' : '个人意见箱';
        $list['createTime'] = date($model->created_at);
        $list['mobile'] = empty($model->user->mobile) ? '' : $model->user->mobile;
        $list['nickName'] = empty($model->user->name) ? '' : $model->user->name;
        $list['boxName'] = empty($model->box->name) ? '' : $model->box->name;
        $list['adopt'] = $model->adopt == 1;
        $list['anonymous'] = $model->anonymous == 1;
        $list['delete'] = !empty($model->deleted_at);
        if (!empty($model->reply)) {
            foreach ($model->reply as $key => $val) {
                $list['reply'][$key]['replyName'] = empty($val->user->name) ? '' : $val->user->name;
                $list['reply'][$key]['replyTime'] = date($val->created_at);
                $list['reply'][$key]['readType'] = $val->read == 1 ? '已读' : '未读';
                $content = json_decode($val->content);
                $list['reply'][$key]['replyContent'] = $content->content;
            }
        } else {
            $list['reply'] = [];
        }

        $this->setData($list);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }


}
