<?php

namespace App\Logic\Opinion;

use App\Contracts\Opinion\ListOpinionInterface;
use App\Models\Comments;

class ListOpinionLogic implements ListOpinionInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $status = '';
    protected string $opinionStatus = '';
    protected array $time = [];
    protected array $data = [];
    protected array $columns = [];
    protected bool  $filter = true;

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '用户信息', 'key' => 'id', 'width' => 250, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'fixed' => 'left'],
            ['title' => '意见箱', 'key' => 'boxName', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '意见类型', 'key' => 'opinionStatus', 'minWidth' => 120, 'align' => 'center', 'slot' => 'people'],
            ['title' => '意见内容', 'key' => 'content', 'minWidth' => 120, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '是否匿名', 'key' => 'anonymousType', 'width' => 85, 'align' => 'center'],
            ['title' => '是否删除', 'key' => 'isDelete', 'width' => 85, 'align' => 'center', 'slot' => 'delete'],
            ['title' => '提出时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
        ]);
        $model = Comments::withTrashed()
            ->with(['box' => function ($query) {
                $query->with('company')
                    ->with('user');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user');
        //        过滤代码
        if ($this->isFilter()) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        //关键字查找
        if ($this->getKeyword()) {
            switch ($this->getStatus()) {
                case'nickName':
                    $model->whereIn('user_id', function ($query) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $this->getKeyword() . '%');
                    });
                    break;
                case'boxName':
                    $model->whereIn('box_id', function ($query) {
                        $query->select('id')
                            ->from('boxes')
                            ->where('name', 'like', '%' . $this->getKeyword() . '%');
                    });
                    break;
                case'userId':
                    $model->where('user_id', $this->getKeyword());
                    break;
            }
        }
        switch ($this->getOpinionStatus()) {
            case'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:s', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        $model->orderBy('id', 'desc');
        $apply = $model->paginate($this->getPageSize());
        $item = $apply->items();
        $this->setTotal($apply->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['nickName'] = empty($val->user->name) ? '' : $val->user->name;
            $items[$key]['mobile'] = empty($val->user->mobile) ? '' : $val->user->mobile;
            $items[$key]['avatar'] = showImage(empty($val->user->avatar) ? '' : $val->user->avatar);
            $items[$key]['userId'] = empty($val->user->id) ? '' : $val->user->id;
            $items[$key]['boxName'] = empty($val->box->name) ? '' : $val->box->name;
            $content = json_decode($val->content);
            $items[$key]['content'] = empty($content->content) ? '' : $content->content;
            $items[$key]['anonymousType'] = $val->anonymous ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? 1 : 0;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['opinionStatus'] = @$val->box->company_id > 0 ? '企业意见箱' : '个人意见箱';
            $company = empty($val->box->company->name) ? '' : $val->box->company->name;
            $userPeople = empty($val->box->user->name) ? '' : $val->box->user->name;
            if ($company && empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company;
            } elseif (empty($company) && $userPeople) {
                $items[$key]['opinionPeople'] = $userPeople;
            } elseif (!empty($company) && !empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company . '/' . $userPeople;
            } else {
                $items[$key]['opinionPeople'] = '';
            }
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    public function explode(): void
    {
        $this->setColumns([
            ['title' => '用户ID', 'key' => 'userId'],
            ['title' => '用户昵称', 'key' => 'nickName'],
            ['title' => '意见箱', 'key' => 'boxName', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '意见类型', 'key' => 'opinionStatus'],
            ['title' => '意见所属', 'key' => 'opinionPeople'],
            ['title' => '意见内容', 'key' => 'content'],
            ['title' => '是否匿名', 'key' => 'anonymousType'],
            ['title' => '是否删除', 'key' => 'isDelete'],
            ['title' => '提出时间', 'key' => 'createTime'],
        ]);
        $model = Comments::withTrashed()
            ->with(['box' => function ($query) {
                $query->with('company')
                    ->with('user');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('user');
        //关键字查找
        if ($this->getKeyword()) {
            switch ($this->getStatus()) {
                case'nickName':
                    $model->whereIn('user_id', function ($query) {
                        $query->select('id')
                            ->from('users')
                            ->where('name', 'like', '%' . $this->getKeyword() . '%');
                    });
                    break;
                case'boxName':
                    $model->whereIn('box_id', function ($query) {
                        $query->select('id')
                            ->from('boxes')
                            ->where('name', 'like', '%' . $this->getKeyword() . '%');
                    });
                    break;
                case'userId':
                    $model->where('user_id', $this->getKeyword());
                    break;
            }
        }
        switch ($this->getOpinionStatus()) {
            case'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:S', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:S', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        $model->orderBy('id', 'desc');
        $item = $model->get();
        $columns = $this->getColumns();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['nickName'] = empty($val->user->name) ? '' : $val->user->name;
            $items[$key]['avatar'] = showImage(empty($val->user->avatar) ? '' : $val->user->avatar);
            $items[$key]['userId'] = empty($val->user->id) ? '' : $val->user->id;
            $items[$key]['boxName'] = empty($val->box->name) ? '' : $val->box->name;
            $content = json_decode($val->content);
            $items[$key]['content'] = empty($content->content) ? '' : str_replace([',', '\n', '\r\n'], '', $content->content);
            $items[$key]['anonymousType'] = $val->anonymous ? '是' : '否';
            $items[$key]['isDelete'] = $val->deleted_at ? '是' : '否';
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['opinionStatus'] = @$val->box->company_id > 0 ? '企业意见箱' : '个人意见箱';
            $company = empty($val->box->company->name) ? '' : $val->box->company->name;
            $userPeople = empty($val->box->user->name) ? '' : $val->box->user->name;
            if ($company && empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company;
            } elseif (empty($company) && $userPeople) {
                $items[$key]['opinionPeople'] = $userPeople;
            } elseif (!empty($company) && !empty($userPeople)) {
                $items[$key]['opinionPeople'] = $company . '/' . $userPeople;
            } else {
                $items[$key]['opinionPeople'] = '';
            }
        }
        $this->setData(compact('items', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getOpinionStatus(): string
    {
        return $this->opinionStatus;
    }

    /**
     * @param string $opinionStatus
     */
    public function setOpinionStatus(string $opinionStatus): void
    {
        $this->opinionStatus = $opinionStatus;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->filter;
    }

    /**
     * @param bool $filter
     */
    public function setFilter(bool $filter): void
    {
        $this->filter = $filter;
    }
}
