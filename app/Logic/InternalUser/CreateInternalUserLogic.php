<?php


namespace App\Logic\InternalUser;

use App\Contracts\InternalUser\CreateInternalUserInterface;
use App\Models\InternalUser;
use App\Models\User;
class CreateInternalUserLogic implements CreateInternalUserInterface
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }
    private int $userId;
    private int $id;
    public function execute(): void
    {
        User::query()-> findOrfail($this->getUserId());


        $model = InternalUser::withTrashed()-> firstOrCreate([
            'user_id'=>$this->getUserId()
        ]);
        if ($model->trashed()){
            $model->restore();
        }
        $this->setId($model->id);
    }
}
