<?php


namespace App\Logic\InternalUser;


use App\Contracts\InternalUser\ListInternalUserInterface;
use App\Models\InternalUser;
use App\Models\User;

class ListInternalUserLogic implements ListInternalUserInterface
{
    private int $page = 0;
    private int $pageSize = 0;
    private array $data =[];
    private int $total=0;
    private array $columns=[];
    public function execute(): void
    {
        $model = User::query()->whereIn('id',function ($query){
            $query->whereNull('deleted_at')->select('user_id')->from('internal_users');
        })->orderBy('id','asc');
        $user = $model->paginate($this->getPageSize());
        $item = $user->items();
        $items=[];
        foreach ($item as $key => $val){
            $items[$key]['id'] = $val->id;
            $items[$key]['mobile'] = $val->mobile;
            $items[$key]['nickName'] = $val->name;
            $items[$key]['createTime'] = date($val->created_at);
        }

        $this->setColumns([
                ['title'=>'用户ID','key'=>'id','minWidth' => 100, 'align' => 'center'],
                ['title'=>'昵称','key'=>'nickName','minWidth' => 100, 'align' => 'center'],
                ['title'=>'手机','key'=>'mobile','minWidth' => 120, 'align' => 'center'],
                ['title'=>'创建时间','key'=>'createTime','minWidth' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 100, 'align' => 'center', 'slot' => 'edit']
            ]
        );
        $this->setTotal($user->total());
        $total=$this->getTotal();
        $page=$this->getPage();
        $pageSize = $this->getPageSize();
        $columns= $this->getColumns();
        $this->setData(compact('total', 'pageSize', 'columns','page', 'items'));
    }
    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }
    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }
}
