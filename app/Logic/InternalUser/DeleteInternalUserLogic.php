<?php


namespace App\Logic\InternalUser;
use App\Contracts\InternalUser\DeleteInternalUserInterface;
use App\Models\InternalUser;

class DeleteInternalUserLogic implements DeleteInternalUserInterface
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }
    private int $userId=0;
    private int $id=0;
    public function execute(): void
    {
        InternalUser::query()->where('user_id','=',$this->getId())->delete();
    }
}
