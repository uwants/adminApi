<?php

namespace App\Logic\Applets;

use App\Contracts\WechatTemplate\GetMenuInterface;

class GetMenuLogic implements GetMenuInterface
{
    protected array $data = [];

    public function execute(): void
    {
        $access_token = serviceAccessToken();
        $url = "https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=$access_token";
        $code_json_list = curl_get($url);
        $code_arr_tpList = json_decode($code_json_list, true);
        $rs = [];
        $select = ['menu1', 'menu2', 'menu3'];
        $viewList = $this->getSome();
        foreach ($select as $key => $val) {
            if (!empty($code_arr_tpList['selfmenu_info']['button'][$key])) {
                $rs[$val] = $this->createShow($code_arr_tpList['selfmenu_info']['button'][$key], $key);
            } else {
                $tp = array('messageShow' => false, 'message' => array(
                    'name' => '+',
                    'urlType' => 'url',
                    'url' => '',
                    'applets' => '',
                    'onlyOne' => true,
                    'msgType' => false,
                    'main' => $key + 1,
                    'mainSon' => 0
                ));
                $rs[$val]['main'] = $tp;
                $rs[$val]['son'] = [];
            }
        }
        $this->setData(compact('rs', 'viewList'));
    }


    public function getSome()
    {
        $access_token = serviceAccessToken();
        $postOne = array('type' => 'news', 'offset' => 0, 'count' => 10);
        $req_str = json_encode($postOne, JSON_UNESCAPED_UNICODE);
        $url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=$access_token";
        $code_json_list = curl_post($url, $req_str);
        $code_arr_tpList = json_decode($code_json_list, true);
        $list = [];
        foreach ($code_arr_tpList['item'] as $key => $val) {
            $list[$key]['value'] = $val['media_id'];
            $list[$key]['name'] = $val['content']['news_item'][0]['title'];
        }
        return $list;
    }

    public function createShow($data, $kee)
    {
        $rs = [];
        $urlType = ['view' => 'url', 'miniprogram' => 'applets', 'view_limited' => 'view'];
        if (!empty($data['sub_button'])) {
            foreach ($data['sub_button']['list'] as $key => $val) {
                $tp = array(
                    'name' => $val['name'],
                    'urlType' => $urlType[$val['type']],
                    'url' => @$val['url'],
                    'applets' => empty($val['pagepath']) ? '' : $val['pagepath'],
                    'view' => empty($val['media_id']) ? '' : $val['media_id'],
                    'onlyOne' => true,
                    'msgType' => true,
                    'main' => $kee + 1,
                    'mainSon' => $key
                );
                $rs['son'][$key] = $tp;
            }
            $tp = array('messageShow' => false, 'message' => array(
                'name' => $data['name'],
                'urlType' => 'url',
                'url' => '',
                'applets' => '',
                'view' => '',
                'onlyOne' => false,
                'msgType' => false,
                'main' => $kee + 1,
                'mainSon' => 0
            ));
            $rs['main'] = $tp;
        } else {
            $tp = array('messageShow' => false, 'message' => array(
                'name' => $data['name'],
                'urlType' => $data['type'] == 'view' ? 'url' : 'applets',
                'url' => $data['url'],
                'applets' => empty($data['pagepath']) ? '' : $data['pagepath'],
                'view' => empty($val['media_id']) ? '' : $val['media_id'],
                'onlyOne' => true,
                'msgType' => false,
                'main' => $kee + 1,
                'mainSon' => 0
            ));
            $rs['main'] = $tp;
            $rs['son'] = [];
        }
        return $rs;
    }

    /**
     * @return array
     */
    public
    function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public
    function setData(array $data): void
    {
        $this->data = $data;
    }

}
