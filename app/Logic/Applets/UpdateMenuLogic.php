<?php

namespace App\Logic\Applets;

use App\Contracts\WechatTemplate\UpdateMenuInterface;

class UpdateMenuLogic implements UpdateMenuInterface
{
    protected array $data = [];
    protected array $menuMessage = [];

    public function execute(): void
    {
        $newRs = $this->createMenu($this->getMenuMesSage());
        $postOne = array('button' => $newRs);
        $access_token = serviceAccessToken();
        $url="https://api.weixin.qq.com/cgi-bin/menu/create?access_token=$access_token";
        $req_str = json_encode($postOne,JSON_UNESCAPED_UNICODE);
        $res_str = curl_post($url,$req_str);
        $res = json_decode($res_str,true);
        $this->setData(compact('res','url'));
    }

    public function createMenu($menu)
    {
        $rs = array();
        $i = 0;
        foreach ($menu as $key => $val) {
            if ($val['main']['message']['name'] != '+' && empty($val['son'])) {
                //小程序
                if ($val['main']['message']['urlType'] == 'applets') {
                    $rs[$i] = array(
                        "type" => 'miniprogram',
                        "name" => $val['main']['message']['name'],
                        "url" => $val['main']['message']['applets'],
                        "appid" => env('WX_APP_ID'),
                        "pagepath" => $val['main']['message']['applets'],
                    );
                } elseif ($val['main']['message']['urlType'] == 'url') {
                    $rs[$i] = array(
                        "type" => 'view',
                        "name" => $val['main']['message']['name'],
                        "url" => empty($val['main']['message']['url']) ? '' : $val['main']['message']['url'],
                    );
                } else {
                    $rs[$i] = array(
                        "type" => 'view_limited',
                        "name" => $val['main']['message']['name'],
                        "url" => empty($val['main']['message']['view']) ? '' : $val['main']['message']['view'],
                    );
                }
                $i++;
            } elseif (!empty($val['son'])) {
                $son = array();
                foreach ($val['son'] as $ke => $va) {
                    //小程
                    if ($va['urlType'] == 'applets') {
                        $son[$ke] = array(
                            "type" => 'miniprogram',
                            "name" => $va['name'],
                            "url" => empty($va['applets']) ? '' : $va['applets'],
                            "appid" => env('WX_APP_ID'),
                            "pagepath" => empty($va['applets']) ? '' : $va['applets'],
                        );
                    } elseif ($va['urlType'] == 'url') {//网页
                        $son[$ke] = array(
                            "type" => 'view',
                            "name" => $va['name'],
                            "url" => empty($va['url']) ? '' : $va['url'],
                        );
                    } else {
                        $son[$ke] = array(
                            "type" => 'view_limited',
                            "name" => $va['name'],
                            "media_id" => empty($va['view']) ? '' : $va['view'],
                        );
                    }

                }//foreach(2)
                $rs[$i] = array(
                    "name" => $val['main']['message']['name'],
                    "sub_button" => $son,
                );
                $i++;
            }
        }

        return $rs;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getMenuMessage(): array
    {
        return $this->menuMessage;
    }

    /**
     * @param array $menuMessage
     */
    public function setMenuMessage(array $menuMessage): void
    {
        $this->menuMessage = $menuMessage;
    }


}
