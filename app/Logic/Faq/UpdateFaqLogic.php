<?php


namespace App\Logic\Faq;


use App\Contracts\Faq\UpdateFaqInterface;
use App\Models\Articles;

class UpdateFaqLogic implements UpdateFaqInterface
{
    public function execute(): void
    {
        $model = Articles::query()
            ->where('id', $this->getId())
            ->first();
        $model->fill([
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'sort' => $this->getSort(),
            'suspend' => $this->getSuspend(),
        ]);
        $model->save();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getSuspend(): int
    {
        return $this->suspend;
    }

    /**
     * @param int $suspend
     */
    public function setSuspend(int $suspend): void
    {
        $this->suspend = $suspend;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    private int $id = 0;

    private string $title = '';

    private string $content = '';

    private int $suspend = 0;

    private int $sort = 0;

}
