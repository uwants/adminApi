<?php


namespace App\Logic\Faq;


use App\Contracts\Faq\DeleteFaqInterface;
use App\Models\Articles;

class DeleteFaqLogic implements DeleteFaqInterface
{
    public function execute(): void
    {
        $model = Articles::query()
            ->findOrFail($this->getId());
        $model->delete();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    private int $id = 0;

}
