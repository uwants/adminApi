<?php


namespace App\Logic\Faq;


use App\Contracts\Faq\GetFaqInterface;
use App\Models\Articles;

class GetFaqLogic implements GetFaqInterface
{

    public function execute(): void
    {
        $model = Articles::query()
            ->firstOrFail($this->id);
        $this->setData($model->toArray());
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    private int $id = 0;

    private array $data = [];

}
