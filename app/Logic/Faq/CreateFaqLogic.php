<?php


namespace App\Logic\Faq;


use App\Contracts\Faq\CreateFaqInterface;
use App\Models\Articles;

class CreateFaqLogic implements CreateFaqInterface
{
    public function execute(): void
    {
        $model = new Articles;
        $model->fill([
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'type' => 'FAQ',
            'sort' => $this->sort,
            'suspend' => $this->isSuspend(),
        ]);
        $model->save();
        $this->setId($model->getKey());
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return bool
     */
    public function isSuspend(): bool
    {
        return $this->suspend;
    }

    /**
     * @param bool $suspend
     */
    public function setSuspend(bool $suspend): void
    {
        $this->suspend = $suspend;
    }

    private int $id = 0;

    private string $title = '';

    private string $content = '';

    private int $sort = 0;

    private bool $suspend = false;

}
