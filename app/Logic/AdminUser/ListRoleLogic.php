<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\ListRoleInterface;
use App\Models\AdminRoles;
use App\Models\AdminUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

class ListRoleLogic implements ListRoleInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $state = '';
    protected array $data = [];
    protected array $columns = [];
    protected int $roleId = 0;
    protected array $roleList = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '角色ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '角色名称', 'key' => 'name', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '备注', 'key' => 'remark', 'minWidth' => 100, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '创建者', 'key' => 'createUser', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '添加时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '状态', 'key' => 'stateType', 'width' => 100, 'align' => 'center'],
            ['title' => '编辑', 'key' => 'edit', 'minWidth' => 250, 'align' => 'center', 'slot' => 'edit',],
        ]);

        $model = AdminRoles::withTrashed()
            ->with('adminUser');
        if (Auth::user()->role_id != 0) {
            $model->where('created_admin_id', Auth::id());
        }
        if ($this->getKeyword()) {
            $model->where('name', 'like', "%" . $this->getKeyword() . "%");
        }
        switch ($this->getState()) {
            case'delete':
                $model->where('deleted_at', 'is not null');
                break;
            case'stay':
                $model->where('deleted_at', null);
                break;
        }
        $model->orderBy('id', 'desc');
        $admin = $model->paginate($this->getPageSize());
        $item = $admin->items();
        $this->setTotal($admin->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['remark'] = $val->remark;
            $items[$key]['createUser'] = @$val->adminUser->admin_name;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['stateType'] = empty($val->deleted_at) ? '正常' : '已删除';
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    public function roleMenu()
    {
        if (Auth::user()->role_id == 0) {
            $master = DB::table('admin_menus')
                ->where(['is_ban' => 0])
                ->pluck('id')
                ->toArray();
        } else {
            $master = DB::table('admin_role_permission')
                ->where(['role_id' => Auth::user()->role_id])
                ->pluck('menu_id')
                ->toArray();
        }
        $role = DB::table('admin_role_permission')
            ->where(['role_id' => $this->getRoleId()])
            ->pluck('menu_id')
            ->toArray();
        $arr = static::getAllPower(0, $master, $role);
        $this->setData(compact('arr',));
    }

    public function getAllPower($pid, $master, $role_list)
    {
        $list = DB::table('admin_menus')
            ->where(['parent_id' => $pid, 'is_ban' => 0])
            ->get();
        $arr = array();
        //以超级管理员权限作为后台主要权限
        if (sizeof($list) != 0) {
            foreach ($list as $k => $datum) {
                if (in_array($datum->id, $master)) {
                    $give['title'] = $datum->title;
                    $give['expand'] = true;
                    $give['value'] = $datum->id;
                    $give['pId'] = $datum->parent_id;
                    $give['level'] = $datum->level;
                    if (in_array($datum->id, $role_list)) {
                        //$give['selected']=true;
                        $give['checked'] = true;
                    } else {
                        $give['checked'] = false;
                    }
                    $give['children'] = static::getAllPower($datum->id, $master, $role_list);
                    $arr[] = $give;
                }
            }
        }
        return $arr;
    }

    /**
     * @throws Exception
     */
    public function setRolePower()
    {
        $insert_arr = array();
        $time_set = date('Y-m-d H:i:s', time());
        foreach ($this->getRoleList() as $key => $val) {
            $insert_arr[$key]['role_id'] = $this->getRoleId();
            $insert_arr[$key]['menu_id'] = $val['menuId'];
            $insert_arr[$key]['level'] = $val['level'];
            $insert_arr[$key]['parent_id'] = $val['pId'];
            $insert_arr[$key]['created_at'] = $time_set;
            $insert_arr[$key]['updated_at'] = $time_set;
        }
        DB::beginTransaction();
        DB::table('admin_role_permission')
            ->where(['role_id' => $this->getRoleId()])
            ->delete();
        $insertMes = DB::table('admin_role_permission')->insert($insert_arr);
        if ($insertMes) {
            DB::commit();
            $this->setData(compact('insertMes',));
        } else {
            throw new Exception('操作失败，数据修改失败！');
        }
    }

    /**
     * @throws Exception
     */
    public function delete()
    {
        $adminUser = AdminUser::query()->where('role_id', $this->getRoleId())->first();
        if (!empty($adminUser->id)) {
            throw new Exception('操作失败，该角色存在管理员，请移除后再删除！');
        }
        $roleMsg = AdminRoles::query()
            ->where('id', $this->getRoleId())
            ->first();
        if (Auth::id() != $roleMsg->created_admin_id && Auth::user()->role_id != 0) {
            throw new Exception('操作失败，参数错误！你没有修改该角色的权限');
        }
        DB::beginTransaction();
        $deleteOne = DB::table('admin_role_permission')
            ->where(['role_id' => $this->getRoleId()])
            ->delete();
        $deleteTwo = AdminRoles::query()
            ->where('id', $this->getRoleId())
            ->forceDelete();
        if ($deleteOne || $deleteTwo) {
            DB::commit();
            $this->setData(compact('deleteOne', 'deleteTwo'));
        } else {
            throw new Exception('操作失败，数据删除失败！');
        }
    }

    /**
     * @return array
     */
    public function getRoleList(): array
    {
        return $this->roleList;
    }

    /**
     * @param array $roleList
     */
    public function setRoleList(array $roleList): void
    {
        $this->roleList = $roleList;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): void
    {
        $this->roleId = $roleId;
    }


    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
