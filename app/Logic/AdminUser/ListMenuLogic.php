<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\ListMenuInterface;
use App\Models\AdminMenus;

class ListMenuLogic implements ListMenuInterface
{
    protected array $data = [];

    public function execute(): void
    {
        $menuList = $this->allMenu(0);
        $powerList = $this->allMenuSelect(0);
        $this->setData(compact('menuList', 'powerList'));
    }

    public function allMenu($pid = 0)
    {
        $query = AdminMenus::query()
            ->where('parent_id', $pid)
            ->orderBy('sort', 'desc')
            ->get();
        $arr = array();
        if (sizeof($query) != 0) {
            foreach ($query as $k => $datum) {
                $give['title'] = $datum->title;
                $give['expand'] = true;
                $give['children'] = static::allMenu($datum->id);
                $arr[] = $give;
            }
        }
        return $arr;
    }

    public function allMenuSelect($pid = 0)
    {
        $query = AdminMenus::query()
            ->where('parent_id', $pid)
            ->orderBy('sort', 'desc')
            ->get();
        static $messageShow = array();
        $stringList = [1 => '', '2' => '--', '3' => '----'];
        if (sizeof($query) != 0) {
            foreach ($query as $k => $datum) {
                $datum['is_ban'] = !empty($datum['is_ban']);
                $datum['stringList'] = $stringList[$datum->level];
                $messageShow[] = $datum;
                static::allMenuSelect($datum->id);
            }
        }
        return $messageShow;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
