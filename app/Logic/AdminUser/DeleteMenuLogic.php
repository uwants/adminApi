<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\DeleteMenuInterface;
use App\Models\AdminMenus;

class DeleteMenuLogic implements DeleteMenuInterface
{
    protected int $id = 0;

    public function execute(): void
    {
        AdminMenus::query()
            ->where('id', $this->getId())
            ->forceDelete();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


}
