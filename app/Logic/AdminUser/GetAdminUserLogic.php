<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\GetAdminUserInterface;
use App\Models\AdminUser;
use Exception;
use Illuminate\Support\Facades\Cache;

class GetAdminUserLogic implements GetAdminUserInterface
{
    protected string $adminName = '';
    protected string $passWork = '';
    protected string $accessToken = '';
    private array $data = [];

    /**
     * @throws Exception
     */
    public function execute(): void
    {
       /* var_dump($this->getPassword());
        var_dump(md5(md5($this->getPassword())));*/
        $model = AdminUser::query()
            ->select('nick_name as nickName', 'admin_name as adminName', 'avatar', 'id','role_id as roleId')
            ->where('admin_name', '=', $this->getAdminName())
            ->where('password', '=', md5(md5($this->getPassword())))
            ->first();
        if (empty($model->id)) {
            throw new Exception('用户名字或者密码错误！');
        } else {
            $model->nickName = empty($model->nickName) ? $model->adminName : $model->nickName;
            $token = md5(uniqid() . '|' . $model->id);
            Cache::put('admin_user_token_from_id_' . $token, $model->id, 24 * 3600);
            $this->setAccessToken($token);
            $model->accessToken = $this->getAccessToken();
            $model->message = '登录成功！';
            $this->setData($model->toArray());
        }
    }

    public function setAdminName(string $adminName): void
    {
        $this->adminName = $adminName;
    }

    public function getAdminName(): string
    {
        return $this->adminName;
    }

    public function setPassword(string $passWord): void
    {
        $this->passWork = $passWord;
    }

    public function getPassword(): string
    {
        return $this->passWork;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

}
