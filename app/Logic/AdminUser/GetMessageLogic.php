<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\GetMessageInterface;
use App\Models\AdminUser;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class GetMessageLogic implements GetMessageInterface
{
    protected int $id = 0;
    protected int $roleId = 0;
    private array $data = [];

    public function execute(): void
    {
        $admin = AdminUser::query()
            ->where('id', $this->getId())
            ->first();
        $this->setData($admin->toArray());
    }

    public function getMenuList()
    {
        $query = Cache::get('menu_list_' . $this->getRoleId());
        if (empty($query)) {
            //记录角色权限
            if ($this->getRoleId() == 0) {
                $weight = DB::table('admin_menus')->where(['is_ban' => 0])->pluck('id')->toArray();
            } else {
                $weight = DB::table('admin_role_permission')
                    ->where(['role_id' => $this->getRoleId()])
                    ->pluck('menu_id')->toArray();
            }
            $query = static::getAllMenu('0', $weight);
            Cache::put('menu_list_' . $this->getRoleId(), $query, '3600');
        }
        $this->setData($query);
    }

    public function getAllMenu($pid, $role_list, $type = 1)
    {
        $query = DB::table('admin_menus')
            ->where(['parent_id' => $pid, 'is_ban' => 0])
            ->orderBy('sort', 'desc')
            ->get();
        $arr = array();
        if (sizeof($query) != 0) {
            foreach ($query as $k => $datum) {
                $give['name'] = $datum->name;
                $give['title'] = $datum->title;
                $give['icon'] = empty($datum->icon) ? '' : $datum->icon;
                $give['meta'] = [
                    'icon' => empty($datum->icon) ? '' : $datum->icon,
                    'title' => $datum->title
                ];
                if ($type == 1) {
                    if ($datum->level < 2) {//过滤菜单等级
                        $give['children'] = static::getAllMenu($datum->id, $role_list);
                    }
                } else {
                    $give['children'] = static::getAllMenu($datum->id, $role_list);
                }
                if (in_array($datum->id, $role_list)) {
                    $arr[] = $give;
                }
            }
        }
        return $arr;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): void
    {
        $this->roleId = $roleId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
