<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\CreateRoleInterface;
use App\Models\AdminRoles;
use Illuminate\Support\Facades\Auth;
use Exception;

class CreateRoleLogic implements CreateRoleInterface
{
    protected array $role = [];
    protected array $data = [];

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        if (!$this->getRole()) {
            throw new Exception('操作失败，参数错误！');
        }
        if ($this->role['id'] > 0) {
            $roleMsg = AdminRoles::query()
                ->where('id', $this->role['id'])
                ->first();
            if ($this->role['id'] > 0 && Auth::id() != $roleMsg->created_admin_id && Auth::user()->role_id != 0) {
                throw new Exception('操作失败，参数错误！你没有修改该角色的权限');
            }
            $model = AdminRoles::query()
                ->where('id', $this->role['id'])
                ->update([
                    'name' => $this->role['name'],
                    'remark' => $this->role['remark'],
                ]);
        } else {
            $model = AdminRoles::query()
                ->insert([
                    'name' => $this->role['name'],
                    'remark' => $this->role['remark'],
                    'created_admin_id' => Auth::id(),
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'updated_at' => date('Y-m-d H:i:s', time())
                ]);
        }
        if (!$model) {
            throw new Exception('操作失败，数据修改失败！');
        }
        $this->setData(compact('model'));
    }


    /**
     * @return array
     */
    public function getRole(): array
    {
        return $this->role;
    }

    /**
     * @param array $role
     */
    public function setRole(array $role): void
    {
        $this->role = $role;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
