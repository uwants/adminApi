<?php


namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\CreateAdminUserInterface;
use App\Models\AdminUser;
use Illuminate\Support\Facades\Auth;

class CreateAdminUserLogic implements CreateAdminUserInterface
{
    protected string $adminName = '';
    protected int $roleId = 0;
    private string $password = '';
    private int $id = 0;

    public function execute(): void
    {
        $oldMessage = AdminUser::query()
            ->where('admin_name', $this->getAdminName())
            ->first();
        if (!empty($oldMessage->id)) {
            throw new Exception('账号名字已经被占用，操作失败！');
        }
        if ($this->getId() > 0) {
            AdminUser::query()
                ->where('id', $this->getId())
                ->update([
                    'admin_name' => $this->getAdminName(),
                    'role_id' => $this->getRoleId(),
                ]);
            $id = $this->getId();
        } else {
            $model = AdminUser::query()->insertGetId([
                'admin_name' => $this->getAdminName(),
                'password' => md5(md5($this->getPassword())),
                'avatar' => '',
                'nick_name' => $this->getAdminName(),
                'role_id' => $this->getRoleId(),
                'created_id' => Auth::id(),
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time())
            ]);
            $id = $model;
        }
        $this->setId($id);
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): void
    {
        $this->roleId = $roleId;
    }

    public function setAdminName(string $adminName): void
    {
        $this->adminName = $adminName;
    }

    public function getAdminName(): string
    {
        return $this->adminName;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }


}
