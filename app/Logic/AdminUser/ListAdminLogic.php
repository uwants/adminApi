<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\ListAdminInterface;
use App\Models\AdminRoles;
use App\Models\AdminUser;
use Illuminate\Support\Facades\Auth;

class ListAdminLogic implements ListAdminInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '管理员ID', 'key' => 'id', 'minWidth' => 50, 'align' => 'center'],
            ['title' => '名字', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '角色', 'key' => 'roleName', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 150, 'align' => 'center', 'slot' => 'edit'],
        ]);
        $model = AdminUser::query()
            ->with('role');
        if (Auth::user()->role_id != 0) {
            $model->where('created_id', Auth::id());
        }
        //订单关键字查找
        if ($this->getKeyword()) {
            $model->where('admin_name', 'like', "%" . $this->getKeyword() . "%");
        }
        $model->orderBy('id', 'desc');
        $admin = $model->paginate($this->getPageSize());
        $item = $admin->items();
        $this->setTotal($admin->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->admin_name;
            $items[$key]['roleName'] = empty($val->role->name) ? '超神账号' : $val->role->name;
            $items[$key]['roleId'] = $val->role_id;
            $items[$key]['avatar'] = $val->avatar;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $roleList = $this->roleList();
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns', 'roleList'));
    }

    public function roleList()
    {
        $model = AdminRoles::query();
        if (Auth::user()->role_id != 0) {
            $model->where('created_admin_id', Auth::id());
        }
        $item = $model->get();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['roleId'] = $val->id;
            $items[$key]['name'] = $val->name;
        }
        return $items;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }


}
