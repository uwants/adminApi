<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\CreateMenuInterface;
use App\Models\AdminMenus;


class CreateMenuLogic implements CreateMenuInterface
{
    protected array $menu = [];
    protected array $data = [];

    public function execute(): void
    {
        if (!$this->getMenu()) {
            return;
        }
        if ($this->menu['id'] > 0) {
            $message = AdminMenus::query()
                ->where('id', $this->menu['id'])
                ->update([
                    'name' => $this->menu['name'],
                    'path' => $this->menu['path'],
                    'title' => $this->menu['title'],
                    'icon' => empty($this->menu['icon']) ? '' : $this->menu['icon'],
                    'power_name' => $this->menu['power_name'],
                    'parent_id' => $this->menu['parent_id'],
                    'level' => $this->menu['level'],
                    'is_ban' => $this->menu['is_ban'],
                    'sort' => $this->menu['sort'],
                ]);
        } else {
            $message = AdminMenus::query()
                ->insert([
                    'name' => $this->menu['name'],
                    'path' => $this->menu['path'],
                    'title' => $this->menu['title'],
                    'icon' => empty($this->menu['icon']) ? '' : $this->menu['icon'],
                    'power_name' => $this->menu['power_name'],
                    'parent_id' => $this->menu['parent_id'],
                    'level' => $this->menu['level'],
                    'is_ban' => $this->menu['is_ban'],
                    'sort' => $this->menu['sort'],
                ]);
        }
        $this->setData(compact('message'));
    }

    /**
     * @return array
     */
    public function getMenu(): array
    {
        return $this->menu;
    }

    /**
     * @param array $menu
     */
    public function setMenu(array $menu): void
    {
        $this->menu = $menu;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
