<?php

namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\DeleteAdminInterface;
use App\Models\AdminUser;

class DeleteAdminLogic implements DeleteAdminInterface
{
    protected int $id = 0;

    public function execute(): void
    {
        AdminUser::query()
            ->where('id', '=', $this->getId())
            ->delete();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

}
