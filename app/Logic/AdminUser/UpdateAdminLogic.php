<?php


namespace App\Logic\AdminUser;

use App\Contracts\AdminUser\UpdateAdminInterface;
use App\Models\AdminUser;
use Exception;
use Illuminate\Support\Arr;

class UpdateAdminLogic implements UpdateAdminInterface
{
    protected int $id = 0;
    protected array $data = [];

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        $message = $this->getData();
        $admin = AdminUser::query()
            ->where('id', $message['id'])
            ->first();
        $oldPass = md5(md5($message['oldPass']));
        $newPass = md5(md5($message['newPass']));
        if ($oldPass != $admin->password) {
            throw new Exception('原始密码不正确！');
        }
        if (!empty($message['newName'])) {
            $oldMessage = AdminUser::query()
                ->where('admin_name', $message['newName'])
                ->where('id', '!=', $message['id'])
                ->first();
            if (!empty($oldMessage->id)) {
                throw new Exception('账号名字已经存在，修改失败！');
            }
        }
        $model = AdminUser::query()
            ->where('id', $message['id'])
            ->findOrFail($message['id']);
        $model->fill(Arr::where([
            'admin_name' => $message['newName'],
            'avatar'=>$message['headerUrl'],
            'password' => $newPass,
        ], function ($item) {
            return !is_null($item);
        }));
        $model->save();
        $this->setId($message['id']);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }


}
