<?php


namespace App\Logic\Reply;
use App\Models\Reply;

use App\Contracts\Reply\ListReplyInterface;
use Illuminate\Support\Arr;

class ListReplyLogic implements ListReplyInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $status = '';
    protected string $opinionStatus = '';
    protected array $time = [];
    protected array $data = [];
    protected array $columns = [];
    protected bool  $filter = true;
    protected string $sortType='';
    public function execute(): void
    {
        $this->setColumns([
            ['title' => '回复人员信息', 'key' => 'id', 'width' => 250, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'fixed' => 'left'],
            ['title' => '意见反馈', 'key' => 'content', 'minWidth' => 120, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '意见内容', 'key' => 'opinionContent', 'minWidth' => 120, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '采纳', 'key' => 'adopt', 'width' => 80, 'align' => 'center'],
            ['title' => '回复时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
        ]);
        $model=Reply::withTrashed()->with(['user','comment']);
        //        过滤代码
        if ($this->isFilter()) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users');
            });
        }
        //关键字查询
        if($this->getKeyword()){
            switch ($this->getStatus()){
                case 'nickeName':
                    $model->whereIn('user_id',function ($query){
                        $query->select('id')
                            ->from('users')
                            ->where('name','like','%'.$this->getKeyword().'%');
                    });
                    break;
                case 'commentId':
                    $model->where('comment_id',$this->getKeyword());
                    break;
                case 'replyContent':
                    $model ->where('content','like','%'.$this->getKeyword().'%');
                    break;
                case 'userId':
                    $model->where('user_id', $this->getKeyword());
                    break;
                case 'commentContent':
                    $model->whereIn('comment_id', function ($query){
                        $query->select('id')->from('comments')
                            ->where('content','like','%'.$this->getKeyword().'%');
                    });
                    break;
            }
        }
        //筛选删除
        switch ($this->getOpinionStatus()) {
            case 'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case 'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        //时间筛选
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:s', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        //排序
        switch ($this->getSortType()){
            case 'timeDESC':
                $model->orderBy('created_at', 'desc');
                break;
            case 'timeASC':
                $model->orderBy('created_at','ASC');
                break;
            case 'commentIdASC':
                $model->orderBy('comment_id','ASC');
                break;
            case 'commentIdDESC':
                $model->orderBy('comment_id','desc');
                break;
        }
        $apply = $model->paginate($this->getPageSize());
        $items = $apply->items();
        $this->setTotal($apply->total());
        $total=$this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $list = [];
        foreach ($items as $item){
            $content=\Qiniu\json_decode(Arr::get($item,'content'));
            $opinionContent=\Qiniu\json_decode(Arr::get($item,'comment.content'));
            $list[] =[
                'id'=>$item->id,
                'commentId'=> empty($item->comment->id)?'':$item->comment->id,
                'userId' => empty($item->user->id) ? '' : $item->user->id,
                'mobile'=>empty($item->user->mobile)?'':$item->user->mobile,
                'nickName' => empty($item->user->name) ? '' : $item->user->name,
                'avatar' => showImage(empty($item->user->avatar) ? '' : $item->user->avatar),
                'content' => empty( $content->content)?'':$content->content,
                'opinionContent' => empty( $opinionContent->content)?'':$opinionContent->content,
                'adopt' =>empty($item->comment->adopt)?'否':'是',
                'createTime' => date($item->created_at),
            ];
        }
        $this->setData(compact('total', 'pageSize', 'page', 'list', 'columns'));
    }

    public function explode(): void
    {
        $this->setColumns([
            ['title' => '回复人员ID', 'key' => 'userId'],
            ['title' => '用户昵称', 'key' => 'nickName'],
            ['title' => '手机号码', 'key' => 'mobile'],
            ['title' => '意见反馈', 'key' => 'content'],
            ['title' => '意见内容', 'key' => 'opinionContent'],
            ['title' => '采纳', 'key' => 'adopt', 'align' => 'center'],
            ['title' => '回复时间', 'key' => 'createTime',  'align' => 'center'],
        ]);
        $model=Reply::withTrashed()->with(['user','comment']);
        //        过滤代码
        if ($this->isFilter()) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users');
            });
        }
        //关键字查询
        if($this->getKeyword()){
            switch ($this->getStatus()){
                case 'nickeName':
                    $model->whereIn('user_id',function ($query){
                        $query->select('id')
                            ->from('users')
                            ->where('name','like','%'.$this->getKeyword().'%');
                    });
                    break;
                case 'replyContent':
                    $model ->where('content','like','%'.$this->getKeyword().'%');
                    break;
                case 'userId':
                    $model->where('user_id', $this->getKeyword());
                    break;
            }
        }
        //筛选删除
        switch ($this->getOpinionStatus()) {
            case 'delete':
                $model->where('deleted_at', '!=', null);
                break;
            case 'noDelete':
                $model->where('deleted_at', '=', null);
                break;
        }
        //时间筛选
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:S', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:S', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        //排序
        $model->orderBy('created_at', 'desc');
        $items = $model->get();
        $columns = $this->getColumns();
        $list = [];
        foreach ($items as $item){
            $content=\Qiniu\json_decode(Arr::get($item,'content'));
            $opinionContent=\Qiniu\json_decode(Arr::get($item,'comment.content'));
            $list[] = [
                'id'=>$item->id,
                'userId' => empty($item->user->id) ? '' : $item->user->id,
                'mobile'=>empty($item->user->mobile)?'':$item->user->mobile,
                'nickName' => empty($item->user->name) ? '' : $item->user->name,
                'avatar' => showImage(empty($item->user->avatar) ? '' : $item->user->avatar),
                'content' => empty( $content->content)?'':$content->content,
                'opinionContent' => empty( $opinionContent->content)?'':$opinionContent->content,
                'adopt' =>empty($item->comment->adopt)?'否':'是',
                'createTime' => date($item->created_at),
            ];
        }
        $this->setData(compact( 'list', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getOpinionStatus(): string
    {
        return $this->opinionStatus;
    }

    /**
     * @param string $opinionStatus
     */
    public function setOpinionStatus(string $opinionStatus): void
    {
        $this->opinionStatus = $opinionStatus;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->filter;
    }

    /**
     * @param bool $filter
     */
    public function setFilter(bool $filter): void
    {
        $this->filter = $filter;
    }

    /**
     * @return string
     */
    public function getSortType(): string
    {
        return $this->sortType;
    }

    /**
     * @param string $sortType
     */
    public function setSortType(string $sortType): void
    {
        $this->sortType = $sortType;
    }
}
