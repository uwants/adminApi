<?php


namespace App\Logic\Reply;


use App\Contracts\Reply\GetReplyInterface;
use App\Models\Reply;

class GetReplyLogic implements GetReplyInterface
{
    protected int $id = 0;
    protected array $data = [];

    public function execute(): void
    {
        $model = Reply::query()
            ->select('id', 'comment_id', 'content', 'user_id', 'read', 'created_at')
            ->findOrFail($this->getId())
            ->with('comment');
        $this->setData($model->toArray());
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
