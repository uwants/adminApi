<?php

namespace App\Logic\Statistics;

use App\Contracts\Statistics\ActivityDataInterface;
use App\Models\Activities;
use App\Models\ActivityBoxes;
use App\Models\User;
use App\Models\Comments;
use Illuminate\Support\Facades\DB;

class ActivityDataLogic implements ActivityDataInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected int $number = 0;
    protected int $activityId = 0;
    protected string $exportData = '';
    protected string $sortColumn = '';
    protected string $sortVal = '';
    protected array $data = [];
    protected array $dataBox = [];
    protected array $dataUser = [];
    protected array $columns = [];
    protected array $time = [];
    protected string $timeState = '';
    protected string $activityType = '';
    protected string $numberSelect = '';
    protected bool $filter = true;
    protected string $newUser = '';

    public function execute(): void
    {

        //活动列表
        $activityList = Activities::query()->select('id as value', 'name as label')->get();
        if ($this->getActivityId() == 0) {
            $this->setActivityId($activityList[0]->value);
        }
        $start = '';
        $end = '';
        switch ($this->getTimeState()) {
            case'allDay':
                $start = '';
                $end = '';
                break;
            case'yesterday':
                $start = date('Y-m-d 00:00:00', strtotime("-1 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastSevenDays':
                $start = date('Y-m-d 00:00:00', strtotime("-7 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastThirtyDays':
                $start = date('Y-m-d 00:00:00', strtotime("-30 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
        }
        //头部
        $header = [];
        DB::enableQueryLog();
        if ($this->isFilter()) {
            $box = ActivityBoxes::query()
                ->select('*',
                    DB::raw("(select count(id) from users where created_at>activity_boxes.created_at and id in (select user_id from comments where comments.box_id=activity_boxes.box_id)
                    and id not in(select user_id from internal_users where deleted_at is null)) as newUserNum"),//新用户
                    DB::raw("(select count(id) from comments where comments.box_id=activity_boxes.box_id
                    and comments.user_id not in(select user_id from internal_users where deleted_at is null) ) as commentNum"),//意见数
                    DB::raw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id
                    and comments.user_id not in(select user_id from internal_users where deleted_at is null)) as inPeopleNum")//参与人数
                )
                ->where('activity_id', $this->getActivityId());
            $box->whereNotIn('box_id', function ($query) {
                $query->select('id')->from('boxes')->whereIn('user_id', function ($query) {
                    $query->select('user_id')->from('internal_users')
                        ->where('deleted_at', null);
                });
            });
        } else {
            $box = ActivityBoxes::query()
                ->select('*',
                    DB::raw("(select count(id) from users where created_at>activity_boxes.created_at and id in (select user_id from comments where comments.box_id=activity_boxes.box_id)) as newUserNum"),//新用户
                    DB::raw("(select count(id) from comments where comments.box_id=activity_boxes.box_id) as commentNum"),//意见数
                    DB::raw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id) as inPeopleNum")//参与人数
                )
                ->where('activity_id', $this->getActivityId());
        }
        if ($start && $end) {
            $box->whereBetween('created_at', [$start, $end]);
        } elseif (!empty($this->time[0]) && empty($this->getTimeState())) {
            $start = date('Y-m-d 00:00:00', strtotime($this->time[0]));
            $end = date('Y-m-d 23:59:59', strtotime($this->time[1]));
            $box->whereBetween('created_at', [$start, $end]);
        }
        $contrast = ['big' => '>', 'etc' => '=', 'small' => '<'];
        switch ($this->getActivityType()) {
            case'inPeople'://参与人数
                if ($this->getNumberSelect() != 'all') {
                    if ($this->isFilter()) {
                        $box->whereRaw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id
                    and  comments.user_id not in (select user_id from internal_users where deleted_at is null))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    } else {
                        $box->whereRaw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id)"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    }
                }
                break;
            case'newPeople'://新用户
                if ($this->getNumberSelect() != 'all') {
                    if ($this->isFilter()) {
                        $box->whereRaw("(select count(id) from users where created_at>activity_boxes.created_at
                    and id in (select user_id from comments where comments.box_id=activity_boxes.box_id)
                    and id not in (select user_id from internal_users where deleted_at is null))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    } else {
                        $box->whereRaw("(select count(id) from users where created_at>activity_boxes.created_at and id in (select user_id from comments where comments.box_id=activity_boxes.box_id))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    }
                }
                break;
            case'opinion'://提意见数
                if ($this->getNumberSelect() != 'all') {
                    if ($this->isFilter()) {
                        $box->whereRaw("(select count(id) from comments where comments.box_id=activity_boxes.box_id
                    and comments.user_id not in (select user_id from internal_users where deleted_at is null))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    } else {
                        $box->whereRaw("(select count(id) from comments where comments.box_id=activity_boxes.box_id)"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    }
                }
                break;
        }
        $item = $box->get();
        DB::getQueryLog();
        //var_dump($one);
        $boxList = [];
        foreach ($item as $key => $val) {
            $boxList[] = $val->box_id;
        }

        //意见箱数量
        $header[0]['number'] = count($item);
        $header[0]['name'] = '意见箱数量';
        $header[0]['than'] = '';
        //总参与人数
        if ($this->isFilter()) {
            $inPeople = Comments::query()->distinct('user_id')
                ->whereIn('box_id', $boxList)
                ->whereNotIn('user_id', function ($query) {
                    $query->select('user_id')->from('internal_users')
                        ->where('deleted_at' . null);
                })
                ->count();
        } else {
            $inPeople = Comments::query()->distinct('user_id')
                ->whereIn('box_id', $boxList)
                ->count();
        }

        $header[1]['number'] = $inPeople;
        $header[1]['name'] = '总参与人数';
        $header[1]['than'] = '';
        //新用户数
        $msgTime = ActivityBoxes::query()
            ->where('activity_id', $this->getActivityId())
            ->orderBy('created_at', 'asc')
            ->first();
        $time = $msgTime->created_at;
        if ($this->isFilter()) {
            $newPeople = Comments::query()->distinct('user_id')
                ->whereIn('user_id', function ($query) use ($time) {
                    $query->select('id')->from('users')->where('created_at', '>=', $time);
                })
                ->whereNotIn('user_id', function ($query) {
                    $query->select('user_id')->from('internal_users')
                        ->where('deleted_at' . null);
                })
                ->whereIn('box_id', $boxList)->count();
        } else {
            $newPeople = Comments::query()->distinct('user_id')
                ->whereIn('user_id', function ($query) use ($time) {
                    $query->select('id')->from('users')->where('created_at', '>=', $time);
                })
                ->whereIn('box_id', $boxList)->count();
        }
        $header[2]['number'] = $newPeople;
        $header[2]['name'] = '新用户数';
        if (!empty($inPeople)) {
            $than = ($newPeople / $inPeople) * 100;
            $than = (float)number_format($than, 2, '.', '');
            $header[2]['than'] = '占比 ' . $than . '%';
        } else {
            $header[2]['than'] = '占比 0.00%';
        }

        //提意见数
        if ($this->isFilter()) {
            $commentsNum = Comments::query()->whereIn('box_id', $boxList)
                ->whereNotIn('user_id', function ($query) {
                    $query->select('user_id')->from('internal_users')
                        ->where('deleted_at' . null);
                })
                ->count();
        } else {
            $commentsNum = Comments::query()->whereIn('box_id', $boxList)->count();
        }
        $header[3]['number'] = $commentsNum;
        $header[3]['name'] = '提意见数';
        $header[3]['than'] = '';

        //关注公众号
        if ($this->isFilter()) {
            $followPeople = Comments::query()->distinct('user_id')
                ->whereIn('user_id', function ($query) {
                    $query->select('user_id')->from('wechat_users')
                        ->whereIn('unionid', function ($query) {
                            $query->select('unionid')->from('wechat_user_subscribes')
                                ->where('deleted_at', null);
                        });
                })
                ->whereNotIn('user_id', function ($query) {
                    $query->select('user_id')->from('internal_users')
                        ->where('deleted_at' . null);
                })
                ->whereIn('box_id', $boxList)->count();
        } else {
            $followPeople = Comments::query()->distinct('user_id')
                ->whereIn('user_id', function ($query) {
                    $query->select('user_id')->from('wechat_users')
                        ->whereIn('unionid', function ($query) {
                            $query->select('unionid')->from('wechat_user_subscribes')
                                ->where('deleted_at', null);
                        });
                })
                ->whereIn('box_id', $boxList)->count();
        }
        $header[4]['number'] = $followPeople;
        $header[4]['name'] = '关注公众号';
        if (!empty($inPeople)) {
            $than = ($followPeople / $inPeople) * 100;
            $than = (float)number_format($than, 2, '.', '');
            $header[4]['than'] = '占比 ' . $than . '%';
        } else {
            $header[4]['than'] = '占比 0.00%';
        }
        $time = [$start, $end];
        $activityId = $this->getActivityId();
        $this->setData(compact('activityList', 'time', 'header', 'activityId'));
    }

    public function executeBox(): void
    {
        if ($this->getExportData() == 1) {
            $columnsBox = [
                ['title' => '意见箱名字', 'key' => 'name', 'minWidth' => 250, 'align' => 'center'],
                ['title' => '创建者昵称', 'key' => 'userName', 'minWidth' => 150, 'align' => 'center'],
                ['title' => '创建者手机', 'key' => 'mobile', 'minWidth' => 150, 'align' => 'center'],
                ['title' => '总参与人数', 'key' => 'inPeopleNum', 'minWidth' => 110, 'align' => 'center'],
                ['title' => '新用户人数', 'key' => 'newUserNum', 'minWidth' => 110, 'align' => 'center'],
                ['title' => '提意见数量', 'key' => 'commentNum', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 120, 'align' => 'center'],
            ];
        } else {
            $columnsBox = [
                ['title' => '创建者信息', 'key' => 'createUser', 'minWidth' => 150, 'slot' => 'header', 'align' => 'left', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '意见箱名字', 'key' => 'name', 'minWidth' => 150, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '总参与人数', 'key' => 'inPeopleNum', 'minWidth' => 120, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '新用户人数', 'key' => 'newUserNum', 'minWidth' => 120, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '提意见数量', 'key' => 'commentNum', 'minWidth' => 120, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'createTime', 'width' => 120, 'align' => 'center', 'slot' => 'edit'],
            ];
        }
        DB::enableQueryLog();
        if ($this->isFilter()) {
            $box = ActivityBoxes::query()
                ->select('*',
                    DB::raw("(select count(id) from users where created_at>activity_boxes.created_at
                    and id in (select user_id from comments where comments.box_id=activity_boxes.box_id)
                    and id not in (select user_id from internal_users where deleted_at is null)) as newUserNum"),//新用户
                    DB::raw("(select count(id) from comments where comments.box_id=activity_boxes.box_id
                    and comments.user_id not in (select user_id from internal_users where deleted_at is null)) as commentNum"),//意见数
                    DB::raw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id
                    and  comments.user_id not in (select user_id from internal_users where deleted_at is null)) as inPeopleNum")//参与人数
                )
                ->where('activity_id', $this->getActivityId())
                ->with(['box' => function ($query) {
                    $query->with('user');
                    return call_user_func([$query, 'withTrashed']);
                }]);
            $box->whereNotIn('box_id', function ($query) {
                $query->select('id')->from('boxes')->whereIn('user_id', function ($query) {
                    $query->select('user_id')->from('internal_users')
                        ->where('deleted_at', null);
                });
            });
        } else {
            $box = ActivityBoxes::query()
                ->select('*',
                    DB::raw("(select count(id) from users where created_at>activity_boxes.created_at and id in (select user_id from comments where comments.box_id=activity_boxes.box_id)) as newUserNum"),//新用户
                    DB::raw("(select count(id) from comments where comments.box_id=activity_boxes.box_id) as commentNum"),//意见数
                    DB::raw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id) as inPeopleNum")//参与人数
                )
                ->where('activity_id', $this->getActivityId())
                ->with(['box' => function ($query) {
                    $query->with('user');
                    return call_user_func([$query, 'withTrashed']);
                }]);
        }
        $start = '';
        $end = '';
        switch ($this->getTimeState()) {
            case'allDay':
                $start = '';
                $end = '';
                break;
            case'yesterday':
                $start = date('Y-m-d 00:00:00', strtotime("-1 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastSevenDays':
                $start = date('Y-m-d 00:00:00', strtotime("-7 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastThirtyDays':
                $start = date('Y-m-d 00:00:00', strtotime("-30 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
        }
        if ($start && $end) {
            $box->whereBetween('created_at', [$start, $end]);
        } elseif (!empty($this->time[0]) && empty($this->getTimeState())) {
            $start = date('Y-m-d 00:00:00', strtotime($this->time[0]));
            $end = date('Y-m-d 23:59:59', strtotime($this->time[1]));
            $box->whereBetween('created_at', [$start, $end]);
        }
        $contrast = ['big' => '>', 'etc' => '=', 'small' => '<'];
        switch ($this->getActivityType()) {
            case'inPeople'://参与人数
                if ($this->getNumberSelect() != 'all') {
                    if ($this->isFilter()) {
                        $box->whereRaw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id
                    and  comments.user_id not in (select user_id from internal_users where deleted_at is null))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    } else {
                        $box->whereRaw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id)"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    }
                }
                break;
            case'newPeople'://新用户
                if ($this->getNumberSelect() != 'all') {
                    if ($this->isFilter()) {
                        $box->whereRaw("(select count(id) from users where created_at>activity_boxes.created_at
                    and id in (select user_id from comments where comments.box_id=activity_boxes.box_id)
                    and id not in (select user_id from internal_users where deleted_at is null))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    } else {
                        $box->whereRaw("(select count(id) from users where created_at>activity_boxes.created_at and id in (select user_id from comments where comments.box_id=activity_boxes.box_id))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    }
                }
                break;
            case'opinion'://提意见数
                if ($this->getNumberSelect() != 'all') {
                    if ($this->isFilter()) {
                        $box->whereRaw("(select count(id) from comments where comments.box_id=activity_boxes.box_id
                    and comments.user_id not in (select user_id from internal_users where deleted_at is null))"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    } else {
                        $box->whereRaw("(select count(id) from comments where comments.box_id=activity_boxes.box_id)"
                            . $contrast[$this->getNumberSelect()] . $this->getNumber());
                    }
                }
                break;
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $box->orderBy($this->getSortColumn(), $this->getSortVal());
        } else {
            $box->orderBy('id', 'desc');
        }
        if ($this->getExportData() == 1) {
            $item = $box->get();
        } else {
            $boxList = $box->paginate($this->getPageSize());
            $item = $boxList->items();
            $this->setTotal($boxList->total());
        }
        DB::getQueryLog();

        $totalBox = $this->getTotal();
        $pageSizeBox = $this->getPageSize();
        $pageBox = $this->getPage();
        $itemsBox = [];

        foreach ($item as $key => $val) {
            $itemsBox[$key]['id'] = $val->id;
            $itemsBox[$key]['boxId'] = $val->box_id;
            $itemsBox[$key]['name'] = empty($val->box->name) ? '' : $val->box->name;
            if (!empty($val->box->deleted_at)) {
                $itemsBox[$key]['name'] = $itemsBox[$key]['name'] . ' (已删除)';
            }
            $itemsBox[$key]['userId'] = empty($val->box->user->id) ? '' : $val->box->user->id;
            $itemsBox[$key]['userName'] = empty($val->box->user->name) ? '' : $val->box->user->name;
            $itemsBox[$key]['mobile'] = empty($val->box->user->mobile) ? '' : $val->box->user->mobile;
            $itemsBox[$key]['userAvatar'] = showImage(@$val->box->user->avatar, 'avatar');
            $itemsBox[$key]['createTime'] = date($val->created_at);
            $itemsBox[$key]['inPeopleNum'] = empty($val->inPeopleNum) ? 0 : $val->inPeopleNum;//参与人数
            $itemsBox[$key]['commentNum'] = $val->commentNum;//意见数
            $itemsBox[$key]['newUserNum'] = $val->newUserNum;//新用户
        }
        $this->setDataBox(compact('totalBox', 'pageSizeBox', 'pageBox', 'itemsBox', 'columnsBox'));
    }

    public function executeUser(): void
    {
        if ($this->getExportData() == 1) {
            $columnsUser = [
                ['title' => '用户昵称', 'key' => 'userName', 'minWidth' => 150, 'align' => 'center'],
                ['title' => '用户手机', 'key' => 'mobile', 'minWidth' => 150, 'align' => 'center'],
                ['title' => '提意见数量', 'key' => 'commentNum', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '注册时间', 'key' => 'createTime', 'minWidth' => 120, 'align' => 'center'],
            ];
        } else {
            $columnsUser = [
                ['title' => '用户信息', 'key' => 'name', 'minWidth' => 200, 'align' => 'left', 'ellipsis' => true, 'tooltip' => true, 'slot' => 'header'],
                ['title' => '提意见数量', 'key' => 'commentNum', 'minWidth' => 120, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 120, 'align' => 'center'],
                ['title' => '注册时间', 'key' => 'createTime', 'minWidth' => 120, 'align' => 'center'],
            ];
        }
        $user = User::query()
            ->select('*',
                DB::raw("(select count(id) from comments where comments.box_id in(select box_id from activity_boxes where activity_id=" .
                    $this->getActivityId() . " and comments.user_id=users.id)) as commentNum")
            )
            ->with(['userUnionid' => function ($query) {
                $query->with('wapOpenId');
            }])
            ->whereIn('id', function ($query) {
                $query->select('user_id')->from('comments')->whereIn('box_id', function ($query) {
                    $query->select('box_id')->from('activity_boxes')
                        ->where('activity_id', $this->getActivityId())
                        ->where('deleted_at', null);
                });
            });

        if ($this->isFilter()) {
            $user->whereNotIn('id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }
        $start = '';
        $end = '';
        switch ($this->getTimeState()) {
            case'allDay':
                $start = '';
                $end = '';
                break;
            case'yesterday':
                $start = date('Y-m-d 00:00:00', strtotime("-1 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastSevenDays':
                $start = date('Y-m-d 00:00:00', strtotime("-7 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastThirtyDays':
                $start = date('Y-m-d 00:00:00', strtotime("-30 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
        }
        if ($start && $end) {
            $user->whereIn('id', function ($query) use ($start, $end) {
                $query->select('user_id')->from('comments')->whereIn('box_id', function ($query) use ($start, $end) {
                    $query->select('box_id')->from('activity_boxes')
                        ->where('activity_id', $this->getActivityId())
                        ->whereBetween('created_at', [$start, $end])
                        ->where('deleted_at', null);
                });
            });
        } elseif (!empty($this->time[0]) && empty($this->getTimeState())) {
            $start = date('Y-m-d 00:00:00', strtotime($this->time[0]));
            $end = date('Y-m-d 23:59:59', strtotime($this->time[1]));
            $user->whereIn('id', function ($query) use ($start, $end) {
                $query->select('user_id')->from('comments')->whereIn('box_id', function ($query) use ($start, $end) {
                    $query->select('box_id')->from('activity_boxes')
                        ->where('activity_id', $this->getActivityId())
                        ->whereBetween('created_at', [$start, $end])
                        ->where('deleted_at', null);
                });
            });
        }
        $contrast = ['big' => '>', 'etc' => '=', 'small' => '<'];
        switch ($this->getActivityType()) {
            case'inPeople'://参与人数
                if ($this->getNumberSelect() != 'all') {
                    //满足条件的意见箱
                    $boxList = ActivityBoxes::query()
                        ->select('*',
                            DB::raw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id) as inPeopleNum")//参与人数
                        )
                        ->where('activity_id', $this->getActivityId())
                        ->whereRaw("(select count(distinct(user_id)) from comments where comments.box_id=activity_boxes.box_id)" . $contrast[$this->getNumberSelect()] . $this->getNumber())
                        ->pluck('box_id');
                    $user->whereIn('id', function ($query) use ($boxList) {
                        $query->select('user_id')->from('comments')->whereIn('box_id', $boxList);
                    });
                }
                break;
            case'newPeople'://新用户
                if ($this->getNumberSelect() != 'all') {
                    //满足条件的意见箱
                    $boxList = ActivityBoxes::query()
                        ->select('*',
                            DB::raw("(select count(id) from users where created_at>activity_boxes.created_at and id in (select user_id from comments where comments.box_id=activity_boxes.box_id)) as newUser")//参与人数
                        )
                        ->where('activity_id', $this->getActivityId())
                        ->whereRaw("(select count(id) from users where created_at>activity_boxes.created_at and id in (select user_id from comments where comments.box_id=activity_boxes.box_id))" . $contrast[$this->getNumberSelect()] . $this->getNumber())
                        ->pluck('box_id');
                    $user->whereIn('id', function ($query) use ($boxList) {
                        $query->select('user_id')->from('comments')->whereIn('box_id', $boxList);
                    });
                }
                break;
            case'opinion'://提意见数
                if ($this->getNumberSelect() != 'all') {
                    //满足条件的意见箱
                    $boxList = ActivityBoxes::query()
                        ->select('*',
                            DB::raw("(select count(id) from comments where comments.box_id=activity_boxes.box_id) as commentNum")
                        )
                        ->where('activity_id', $this->getActivityId())
                        ->whereRaw("(select count(id) from comments where comments.box_id=activity_boxes.box_id)" . $contrast[$this->getNumberSelect()] . $this->getNumber())
                        ->pluck('box_id');
                    $user->whereIn('id', function ($query) use ($boxList) {
                        $query->select('user_id')->from('comments')->whereIn('box_id', $boxList);
                    });
                }
                break;
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $user->orderBy($this->getSortColumn(), $this->getSortVal());
        } else {
            $user->orderBy('id', 'desc');
        }
        //新用户
        if ($this->getNewUser()) {
            $msgTime = ActivityBoxes::query()
                ->where('activity_id', $this->getActivityId())
                ->orderBy('created_at', 'asc')
                ->first();
            $user->where('created_at', '>=', $msgTime->created_at);
        }
        if ($this->getExportData() == 1) {
            $item = $user->get();
        } else {
            $userList = $user->paginate($this->getPageSize());
            $item = $userList->items();
            $this->setTotal($userList->total());
        }
        $totalUser = $this->getTotal();
        $pageSizeUser = $this->getPageSize();
        $pageUser = $this->getPage();
        $itemsUser = [];
        foreach ($item as $key => $val) {
            $itemsUser[$key]['id'] = $val->id;
            $itemsUser[$key]['userId'] = $val->id;
            $itemsUser[$key]['userName'] = $val->name;
            $itemsUser[$key]['mobile'] = $val->mobile;
            $itemsUser[$key]['userAvatar'] = showImage($val->avatar, 'avatar');
            $itemsUser[$key]['createTime'] = date($val->created_at);
            $itemsUser[$key]['commentNum'] = $val->commentNum;//意见数
            $itemsUser[$key]['follow'] = empty($val->userUnionid->wapOpenId->id) ? '否' : '是';
        }
        $this->setDataUser(compact('totalUser', 'pageSizeUser', 'pageUser', 'itemsUser', 'columnsUser'));

    }

    /**
     * @return array
     */
    public function getDataBox(): array
    {
        return $this->dataBox;
    }

    /**
     * @param array $dataBox
     */
    public function setDataBox(array $dataBox): void
    {
        $this->dataBox = $dataBox;
    }

    /**
     * @return array
     */
    public function getDataUser(): array
    {
        return $this->dataUser;
    }

    /**
     * @param array $dataUser
     */
    public function setDataUser(array $dataUser): void
    {
        $this->dataUser = $dataUser;
    }

    /**
     * @return string
     */
    public function getNewUser(): string
    {
        return $this->newUser;
    }

    /**
     * @param string $newUser
     */
    public function setNewUser(string $newUser): void
    {
        $this->newUser = $newUser;
    }

    /**
     * @return int
     */
    public function getActivityId(): int
    {
        return $this->activityId;
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->filter;
    }

    /**
     * @param bool $filter
     */
    public function setFilter(bool $filter): void
    {
        $this->filter = $filter;
    }

    /**
     * @param int $activityId
     */
    public function setActivityId(int $activityId): void
    {
        $this->activityId = $activityId;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getNumber(): int
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number): void
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getExportData(): string
    {
        return $this->exportData;
    }

    /**
     * @param string $exportData
     */
    public function setExportData(string $exportData): void
    {
        $this->exportData = $exportData;
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getTimeState(): string
    {
        return $this->timeState;
    }

    /**
     * @param string $timeState
     */
    public function setTimeState(string $timeState): void
    {
        $this->timeState = $timeState;
    }

    /**
     * @return string
     */
    public function getActivityType(): string
    {
        return $this->activityType;
    }

    /**
     * @param string $activityType
     */
    public function setActivityType(string $activityType): void
    {
        $this->activityType = $activityType;
    }

    /**
     * @return string
     */
    public function getNumberSelect(): string
    {
        return $this->numberSelect;
    }

    /**
     * @param string $numberSelect
     */
    public function setNumberSelect(string $numberSelect): void
    {
        $this->numberSelect = $numberSelect;
    }

}
