<?php

namespace App\Logic\Statistics;

use App\Contracts\Statistics\EveryDataInterface;
use App\Models\Boxes;
use App\Models\User;
use App\Models\Comments;
use App\Models\Companies;
use App\Models\Replies;

class EveryDataLogic implements EveryDataInterface
{
    protected string $sortColumn = '';
    protected string $sortVal = '';
    protected array $data = [];
    protected array $columns = [];
    protected array $time = [];
    protected string $timeState = '';
    protected bool $filter = true;

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '日期', 'key' => 'time', 'minWidth' => 100, 'sortable' => 'custom', 'align' => 'center'],
            ['title' => '新增用户数', 'key' => 'user', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '新增企业数', 'key' => 'company', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '新增意见箱数', 'key' => 'box', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '新增意见数', 'key' => 'opinion', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '新增意见回复数', 'key' => 'reply', 'minWidth' => 120, 'align' => 'center'],
        ]);
        $start = '';
        $end = '';
        switch ($this->getTimeState()) {
            case'lastSevenDays':
                $start = date('Y-m-d', strtotime("-7 day"));
                $end = date('Y-m-d', strtotime("-1 day"));
                break;
            case'lastThirtyDays':
                $start = date('Y-m-d', strtotime("-30 day"));
                $end = date('Y-m-d', strtotime("-1 day"));
                break;
            case'lastWeek':
                $start = date('Y-m-d', strtotime("last week"));
                $end = date('Y-m-d', strtotime('this week -1 day'));
                break;
            case'lastMonth':
                $start = date('Y-m-01', strtotime("-1 month"));
                $end = date('Y-m-d', strtotime(date('Y-m-01', time())) - 1);
                break;
        }
        $timeData = 0;
        if ($start && $end) {
            $timeData = (strtotime($end) - strtotime($start)) / 86400 + 1;
        } elseif (!empty($this->time[0])) {
            $start = date('Y-m-d', strtotime($this->time[0]));
            $end = date('Y-m-d', strtotime($this->time[1]));
            $timeData = (strtotime($this->time[1]) - strtotime($this->time[0])) / 86400 + 1;
        }
        $columns = $this->getColumns();
        $items = [];
        $userAll = 0;
        $companyAll = 0;
        $boxAll = 0;
        $opinionAll = 0;
        $replyAll = 0;
        for ($i = 0; $i < $timeData; $i++) {
            $show = date('Y-m-d', strtotime($start) + (86400 * $i));
            $items[$i]['time'] = $show;
            if ($this->isFilter()) {
                $dataShow = $this->countNumNot($show);
            } else {
                $dataShow = $this->countNum($show);
            }
            $items[$i]['user'] = $dataShow['user'];
            $userAll = $userAll + $dataShow['user'];
            $items[$i]['company'] = $dataShow['company'];
            $companyAll = $companyAll + $dataShow['company'];
            $items[$i]['box'] = $dataShow['box'];
            $boxAll = $boxAll + $dataShow['box'];
            $items[$i]['opinion'] = $dataShow['opinion'];
            $opinionAll = $opinionAll + $dataShow['opinion'];
            $items[$i]['reply'] = $dataShow['reply'];
            $replyAll = $replyAll + $dataShow['reply'];
        }
        if ($this->getSortVal() == 'desc') {
            $items = array_reverse($items);
        }
        $headerData = [
            ['number' => $userAll, 'name' => '新增用户数'],
            ['number' => $companyAll, 'name' => '新增企业数'],
            ['number' => $boxAll, 'name' => '新增意见箱数'],
            ['number' => $opinionAll, 'name' => '新增意见数'],
            ['number' => $replyAll, 'name' => '新增意见回复数'],
        ];
        $time = [$start, $end];
        $this->setData(compact('items', 'columns', 'time', 'headerData'));
    }

    public function countNumNot($start)
    {
        $user = User::query()
            ->whereNotIn('id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            })
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->count();
        $box = Boxes::query()
            ->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            })
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->count();
        $company = Companies::query()
            ->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            })
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->where('certified_status', 'STATUS_CERTIFIED_PASS')
            ->count();
        $opinion = Comments::query()
            ->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            })
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->count();
        $reply = Replies::query()
            ->whereIn('comment_id', function ($query) use ($start) {
                $query->select('id')->from('comments')
                    ->whereNotIn('user_id', function ($query) {
                        $query->select('user_id')->from('internal_users')
                            ->where('deleted_at', null);
                    })
                    ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59']);
            })
            ->count();

        return ['user' => $user, 'box' => $box, 'company' => $company, 'opinion' => $opinion, 'reply' => $reply];
    }

    public function countNum($start)
    {
        $user = User::query()
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->count();
        $box = Boxes::query()
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->count();
        $company = Companies::query()
            ->where('certified_status', 'STATUS_CERTIFIED_PASS')
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->count();
        $opinion = Comments::query()
            ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59'])
            ->count();
        $reply = Replies::query()
            ->whereIn('comment_id', function ($query) use ($start) {
                $query->select('id')->from('comments')
                    ->whereBetween('created_at', [$start . ' 00:00:00', $start . ' 23:59:59']);
            })
            ->count();
        return ['user' => $user, 'box' => $box, 'company' => $company, 'opinion' => $opinion, 'reply' => $reply];
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->filter;
    }

    /**
     * @param bool $filter
     */
    public function setFilter(bool $filter): void
    {
        $this->filter = $filter;
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getTimeState(): string
    {
        return $this->timeState;
    }

    /**
     * @param string $timeState
     */
    public function setTimeState(string $timeState): void
    {
        $this->timeState = $timeState;
    }

}
