<?php

namespace App\Logic\Statistics;

use App\Contracts\Statistics\DataUsersInterface;
use App\Models\User;
use App\Models\WechatUserSubscribes;
use App\Models\CompanyUsers;
use App\Models\Comments;
use App\Models\Boxes;
use Illuminate\Support\Facades\DB;

class DataUsersLogic implements DataUsersInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $exportData = '';
    protected string $sortColumn = '';
    protected string $sortVal = '';
    protected array $data = [];
    protected array $columns = [];
    protected array $time = [];
    protected string $timeState = '';

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '用户信息', 'key' => 'id', 'minWidth' => 250, 'sortable' => 'custom', 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '所属企业', 'key' => 'company', 'minWidth' => 150, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '意见箱数', 'key' => 'boxNum', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
            ['title' => '提意见数', 'key' => 'opinionNum', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
            ['title' => '超管/管理员', 'key' => 'admin', 'minWidth' => 100, 'slot' => 'admin', 'align' => 'center'],
            ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 120, 'align' => 'center', 'slot' => 'follow'],
            ['title' => '注册时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
        ]);
        $userModel = User::query()
            ->with(['userUnionid' => function ($query) {
                $query->with('wapOpenId');
            }])
            ->with('companyUser')
            ->with('company')
            ->select('mobile', 'id', 'created_at', 'name', 'avatar',
                DB::raw("(select count(id) from boxes where boxes.user_id = users.id and deleted_at is null) as boxNum"),
                DB::raw("(select count(id) from comments where comments.user_id = users.id and deleted_at is null) as opinionNum"),
            );
        $start = '';
        $end = '';
        switch ($this->getTimeState()) {
            case'yesterday':
                $start = date('Y-m-d 00:00:00', strtotime("-1 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastSevenDays':
                $start = date('Y-m-d 00:00:00', strtotime("-7 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastThirtyDays':
                $start = date('Y-m-d 00:00:00', strtotime("-30 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                break;
            case'lastWeek':
                $start = date('Y-m-d 00:00:00', strtotime("last week"));
                $end = date('Y-m-d 23:59:59', strtotime('this week -1 day'));
                break;
            case'lastMonth':
                $start = date('Y-m-01 00:00:00', strtotime("-1 month"));
                $end = date('Y-m-d 23:59:59', strtotime(date('Y-m-01', time())) - 1);
                break;
        }
        if ($start && $end) {
            $userModel->whereBetween('created_at', [$start, $end]);
        } elseif (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:s', strtotime($this->time[1]));
            $userModel->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $userModel->orderBy($this->getSortColumn(), $this->getSortVal());
        } else {
            $userModel->orderBy('id', 'desc');
        }

        if ($this->getExportData() == 1) {
            $item = $userModel->get();
        } else {
            $user = $userModel->paginate($this->getPageSize());
            $item = $user->items();
            $this->setTotal($user->total());
        }
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];

        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['mobile'] = $val->mobile;
            if ($this->getExportData() == 1) {
                $items[$key]['nickName'] = filterEmoji($val->name);
            } else {
                $items[$key]['nickName'] = $val->name;
            }
            $items[$key]['headImg'] = showImage($val->avatar, 'avatar');
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['boxNum'] = $val->boxNum;
            $items[$key]['opinionNum'] = $val->opinionNum;
            $items[$key]['follow'] = empty($val->userUnionid->wapOpenId->id) ? '否' : '是';
            $items[$key]['wapOpenId'] = empty($val->userUnionid->wapOpenId->openid) ? '' : $val->userUnionid->wapOpenId->openid;
            $items[$key]['company'] = '';
            $items[$key]['admin'] = '否';
            foreach ($val->companyUser as $user) {
                if ($user->role == 'ROLE_SUPPER_ADMINISTRATOR' || $user->role == 'ROLE_ADMINISTRATOR') {
                    $items[$key]['admin'] = '是';
                    break;
                } else {
                    $items[$key]['admin'] = '否';
                }
            }
            foreach ($val->company as $va) {
                $items[$key]['company'] .= $va->name . '、';
            }
            $items[$key]['company'] = empty($items[$key]['company']) ? '--' : trim($items[$key]['company'], '、');
        }
        $time = [$start, $end];
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns', 'time'));

    }

    public function picExecute(): void
    {
        $start = '';
        $end = '';
        switch ($this->getTimeState()) {
            case'yesterday':
                $start = date('Y-m-d 00:00:00', strtotime("-1 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                $this->setTime([]);
                break;
            case'lastSevenDays':
                $start = date('Y-m-d 00:00:00', strtotime("-7 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                $this->setTime([]);
                break;
            case'lastThirtyDays':
                $start = date('Y-m-d 00:00:00', strtotime("-30 day"));
                $end = date('Y-m-d 23:59:59', strtotime("-1 day"));
                $this->setTime([]);
                break;
            case'lastWeek':
                $start = date('Y-m-d 00:00:00', strtotime("last week"));
                $end = date('Y-m-d 23:59:59', strtotime('this week -1 day'));
                $this->setTime([]);
                break;
            case'lastMonth':
                $start = date('Y-m-01 00:00:00', strtotime("-1 month"));
                $end = date('Y-m-d 23:59:59', strtotime(date('Y-m-01', time())) - 1);
                $this->setTime([]);
                break;
        }

        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:s', strtotime($this->time[1]));
        }
        $userTotal = User::query()
            ->whereBetween('created_at', [$start, $end])
            ->count('id');
        //关注公众号人数
        $officialAccountTotal = WechatUserSubscribes::query()
            ->whereIn('unionid', function ($query) use ($start, $end) {
                $query->select('unionid')->from('wechat_users')
                    ->whereIn('user_id', function ($query) use ($start, $end) {
                        $query->select('id')->from('users')->whereBetween('created_at', [$start, $end]);
                    });
            })->count();
        //企业人员
        $companyUser = CompanyUsers::query()
            ->whereIn('user_id', function ($query) use ($start, $end) {
                $query->select('id')->from('users')->whereBetween('created_at', [$start, $end]);
            })->count();
        //普通人员
        $commonUser = $userTotal - $companyUser;
        //提意见人数
        $comUser = Comments::query()
            ->whereIn('user_id', function ($query) use ($start, $end) {
                $query->select('id')->from('users')->whereBetween('created_at', [$start, $end]);
            })
            ->whereBetween('created_at', [$start, $end])->count();
        //征集意见人数
        $boxUser = Boxes::query()
            ->whereIn('user_id', function ($query) use ($start, $end) {
                $query->select('id')->from('users')->whereBetween('created_at', [$start, $end]);
            })
            ->whereBetween('created_at', [$start, $end])->count();
        $dataPicColor = unique_array_color(5);
        $dataShow = [
            ['value' => $userTotal, 'name' => '总人数 '],
            ['value' => $officialAccountTotal, 'name' => '关注公众号人数 '],
            ['value' => $companyUser, 'name' => '企业人员 '],
            ['value' => $commonUser, 'name' => '普通人员 '],
            ['value' => $comUser, 'name' => '提意见人数 '],
            ['value' => $boxUser, 'name' => '征集意见人数 ']
        ];
        $dataPic = [
            ['value' => $officialAccountTotal, 'name' => '关注公众号人数 ' . $officialAccountTotal],
            ['value' => $companyUser, 'name' => '企业人员 ' . $companyUser],
            ['value' => $commonUser, 'name' => '普通人员 ' . $commonUser],
            ['value' => $comUser, 'name' => '提意见人数 ' . $comUser],
            ['value' => $boxUser, 'name' => '征集意见人数 ' . $boxUser]
        ];
        $dataRing = [
            ['name' => '关注公众号人数', 'total' => $officialAccountTotal, 'color' => $dataPicColor[0], 'percentage' => empty($userTotal) ? 0 : (float)number_format(($officialAccountTotal / $userTotal) * 100, 2, '.', '')],
            ['name' => '企业人员', 'total' => $companyUser, 'color' => $dataPicColor[1], 'percentage' => empty($userTotal) ? 0 : (float)number_format(($companyUser / $userTotal) * 100, 2, '.', '')],
            ['name' => '普通人员', 'total' => $commonUser, 'color' => $dataPicColor[2], 'percentage' => empty($userTotal) ? 0 : (float)number_format(($commonUser / $userTotal) * 100, 2, '.', '')],
            ['name' => '提意见人数', 'total' => $comUser, 'color' => $dataPicColor[3], 'percentage' => empty($userTotal) ? 0 : (float)number_format(($comUser / $userTotal) * 100, 2, '.', '')],
            ['name' => '征集意见人数', 'total' => $boxUser, 'color' => $dataPicColor[4], 'percentage' => empty($userTotal) ? 0 : (float)number_format(($boxUser / $userTotal) * 100, 2, '.', '')],
        ];
        $this->setData(compact('dataPicColor', 'dataPic', 'userTotal', 'dataRing','dataShow'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getExportData(): string
    {
        return $this->exportData;
    }

    /**
     * @param string $exportData
     */
    public function setExportData(string $exportData): void
    {
        $this->exportData = $exportData;
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getTimeState(): string
    {
        return $this->timeState;
    }

    /**
     * @param string $timeState
     */
    public function setTimeState(string $timeState): void
    {
        $this->timeState = $timeState;
    }

}
