<?php

namespace App\Logic\Statistics;

use App\Contracts\Statistics\ActivityOpInterface;
use App\Models\Comments;
use Illuminate\Support\Facades\DB;

class ActivityOpLogic implements ActivityOpInterface
{
    protected int $boxId = 0;
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected array $data = [];
    protected array $columns = [];
    protected string $exportData = '';
    protected bool $filter = true;

    public function execute(): void
    {
        if ($this->getExportData() == 1) {
            $this->setColumns([
                ['title' => '用户信息', 'key' => 'createUser', 'minWidth' => 150, 'slot' => 'header', 'align' => 'left', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 120, 'align' => 'center'],
                ['title' => '注册时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
                ['title' => '意见内容', 'key' => 'content', 'width' => 120, 'align' => 'center', 'slot' => 'edit'],
            ]);
        } else {
            $this->setColumns([
                ['title' => '用户信息', 'key' => 'createUser', 'minWidth' => 120, 'slot' => 'header', 'align' => 'left', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '是否关注公众号', 'key' => 'follow', 'width' => 120, 'align' => 'center'],
                ['title' => '注册时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '意见内容', 'key' => 'content', 'minWidth' => 350, 'align' => 'left', 'slot' => 'content'],
            ]);
        }
        $list = Comments::query()
            ->where('box_id', $this->getBoxId())
            ->with(['user' => function ($query) {
                $query->with(['userUnionid' => function ($query) {
                    $query->with('wapOpenId');
                }]);
            }]);
        if ($this->isFilter()) {
            $list->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at' . null);
            });
        }
        if ($this->getExportData() == 1) {
            $item = $list->get();
        } else {
            $boxList = $list->paginate($this->getPageSize());
            $item = $boxList->items();
            $this->setTotal($boxList->total());
        }

        $totalList = $this->getTotal();
        $pageSizeList = $this->getPageSize();
        $pageList = $this->getPage();
        $columnsList = $this->getColumns();
        $itemsList = [];
        foreach ($item as $key => $val) {
            $itemsList[$key]['userId'] = $val->user->id;
            $itemsList[$key]['userName'] = $val->user->name;
            $itemsList[$key]['mobile'] = $val->user->mobile;
            $itemsList[$key]['avatar'] = $val->user->avatar;
            $itemsList[$key]['follow'] = empty($val->user->userUnionid->wapOpenId->id) ? '否' : '是';;
            $itemsList[$key]['createTime'] = date($val->user->created_at);
            $content = json_decode($val->content);
            $itemsList[$key]['content'] = empty($content->content) ? '' : $content->content;
            $itemsList[$key]['title'] = empty($content->title) ? '' : $content->title;
            $itemsList[$key]['images'] = $content->images;
            $itemsList[$key]['videos'] = $content->videos;
        }
        $this->setData(compact('totalList', 'pageSizeList', 'pageList', 'itemsList', 'columnsList'));
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->filter;
    }

    /**
     * @param bool $filter
     */
    public function setFilter(bool $filter): void
    {
        $this->filter = $filter;
    }

    /**
     * @return string
     */
    public function getExportData(): string
    {
        return $this->exportData;
    }

    /**
     * @param string $exportData
     */
    public function setExportData(string $exportData): void
    {
        $this->exportData = $exportData;
    }

    /**
     * @return int
     */
    public function getBoxId(): int
    {
        return $this->boxId;
    }

    /**
     * @param int $boxId
     */
    public function setBoxId(int $boxId): void
    {
        $this->boxId = $boxId;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
