<?php

namespace App\Logic\CallBack;

use App\Contracts\CallBack\CallbackInterface;
use App\Contracts\WechatTemplate\SendTemplateInterface;
use App\Models\User;
use App\Models\WechatUsers;
use App\Models\WechatUserSubscribes;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class CallbackLogic implements CallbackInterface
{
    protected array $message = [];
    protected string $data = '';
    private SendTemplateInterface $send;

    public function __construct(SendTemplateInterface $send)
    {
        $this->send = $send;
    }

    public function execute(): void
    {
        $postObj = $this->getMessage();
        $keyword = empty($postObj['Content']) ? '' : trim($postObj['Content']);//用户聊天内容
        $time = time();
        //若为事件类型（关注、点击菜单等）
        $content = "您好，欢迎关注超级意见箱！\r\n超级意见箱服务号将会为您推送小程序中的消息!";
        if (trim($postObj['MsgType']) == "event") {
            switch (trim($postObj['Event'])) {
                case'subscribe'://关注事件
                    $mess = $this->wxSetUserInfo($postObj['FromUserName'], 'insert');
                    if ($mess['code'] != 0) {
                        $content = $mess['message'];
                    }
                    break;
                case "unsubscribe"://取消关注
                    $content = "谢谢你的一直陪伴，欢迎下次关注！!";
                    $this->wxSetUserInfo($postObj['FromUserName'], 'del');
                    Log::error('谢谢你的一直陪伴，欢迎下次关注', [$postObj['FromUserName']]);
                    break;
                case'CLICK':
                    //菜单的自定义的key值，可以根据此值判断用户点击了什么内容，从而推送不同信息
                    $EventKey = $postObj['EventKey'];
                    switch ($EventKey) {
                        case'message1':
                            $content = "您好，欢迎！\r\n信息：$EventKey";
                            break;
                        case'message2':
                            $content = "您好，欢迎关注超级意见箱！\r\n信息：$EventKey";
                            break;
                    }
                    break;
            }
            $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "text", $content);
            $this->setData($output);
        } elseif (trim($postObj['MsgType']) == "text") {//若为文字类型
            if ($keyword == "openid") {
                //获取用户信息
                $content = $postObj['FromUserName'];
            } else {
                $mess = $this->wxSetUserInfo($postObj['FromUserName'], 'insert');
                if ($mess['code'] != 0) {
                    $content = $mess['message'];
                } else {
                    $content = "谢谢，你的消息已经收到!";
                }
            }
            //给用户回复内容
            $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "text", $content);
            $this->setData($output);
        } else {
            $content = "您好，欢迎关注超级意见箱！\r\n超级意见箱服务号将会为您推送小程序中的消息。";
            $output = $this->get_retXml($postObj['FromUserName'], $postObj['ToUserName'], $time, "text", $content);
            $this->setData($output);
        }
    }

    /**
     * @return array
     */
    public function getMessage(): array
    {
        return $this->message;
    }

    /**
     * @param array $message
     */
    public function setMessage(array $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    /**
     * @param $fromUsername
     * @param $toUsername
     * @param $time
     * @param $msgType
     * @param $msg
     * @return string
     */
    public function get_retXml($fromUsername, $toUsername, $time, $msgType, $msg)
    {
        $imageTpl = '';
        if ($msgType == "image") {
            $imageTpl = /** @lang text */
                "
				<xml>
					<ToUserName><![CDATA[%s]]></ToUserName>
					<FromUserName><![CDATA[%s]]></FromUserName>
					<CreateTime>%s</CreateTime>
					<MsgType><![CDATA[%s]]></MsgType>
					<Image>
						<MediaId><![CDATA[%s]]></MediaId>
					</Image>
				</xml>";
        } else if ($msgType == "text") {
            $imageTpl = /** @lang text */
                "
				<xml>
					<ToUserName><![CDATA[%s]]></ToUserName>
					<FromUserName><![CDATA[%s]]></FromUserName>
					<CreateTime>%s</CreateTime>
					<MsgType><![CDATA[%s]]></MsgType>
					<Content><![CDATA[%s]]></Content>
				</xml>";
        }
        return sprintf($imageTpl, $fromUsername, $toUsername, $time, $msgType, $msg);
    }

    public function wxSetUserInfo($openId, $type = 'insert')
    {
        $token = $this->send->getSAppAccessToken();
        Log::error('获取token', $token);
        if (empty($token['token'])) {
            return ['code' => 1, 'message' => 'token不存在！', 'data' => ['errCode' => 1]];
        }
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" . $token['token'] . "&openid=$openId&lang=zh_CN";
        $mes = $this->send->curl_get($url, []);
        $message = json_decode($mes);
        if (empty($message->openid)) {
            return ['code' => -1, 'message' => 'openid数据错误！', 'data' => ['errCode' => 1]];
        }
        if ($type == 'insert') {
            $have = WechatUserSubscribes::withTrashed()
                ->where([
                    'openid' => $openId,
                    'unionid' => $message->unionid,
                ])->first();
            if(!empty($have->id)){
                WechatUserSubscribes::withTrashed()
                    ->where([
                        'openid' => $openId,
                        'unionid' => $message->unionid,
                    ])->restore();
            }else{
                WechatUserSubscribes::query()
                    ->firstOrCreate([
                        'openid' => $openId,
                        'unionid' => $message->unionid,
                    ]);
            }
        } else {
            $del = WechatUserSubscribes::query()
                ->where('openid', $openId)
                ->delete();
            Log::error('取消关注', [
                'openid' => $openId,
                'unionid' => $message->unionid,
                'delete' => $del
            ]);
        }

        return ['code' => 0, 'message' => 'success！', 'data' => ['errCode' => '']];
    }


}
