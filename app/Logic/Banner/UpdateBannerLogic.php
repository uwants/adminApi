<?php


namespace App\Logic\Banner;


use App\Contracts\Banner\UpdateBannerInterface;
use App\Models\Banner;

class UpdateBannerLogic implements UpdateBannerInterface
{
    public function execute(): void
    {
        $model = Banner::query()
            ->where('id', $this->getId())
            ->first();
        $model->fill([
            'image' => $this->getImage(),
            'content' => $this->getContent(),
            'sort' => $this->getSort(),
            'type' => $this->getType(),
            'suspend' => $this->isSuspend(),
        ]);
        $model->save();
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isSuspend(): bool
    {
        return $this->suspend;
    }

    /**
     * @param bool $suspend
     */
    public function setSuspend(bool $suspend): void
    {
        $this->suspend = $suspend;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    private string $content = '';

    private string $image = '';

    private string $type = '';

    private bool $suspend = false;

    private int $sort = 0;

    private int $id = 0;
}
