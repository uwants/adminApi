<?php


namespace App\Logic\Banner;


use App\Contracts\Banner\ListBannerInterface;
use App\Models\Banner;

class ListBannerLogic implements ListBannerInterface
{

    public function execute(): void
    {
        $model = Banner::query()
            ->orderBy('sort', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate($this->getPageSize(), ['*'], 'page', $this->getPage());
        $list = $model->items();
        $items = [];
        foreach ($list as $key => $val) {
            $items[$key]['id'] = empty($val->id) ? '' : $val->id;
            $items[$key]['image'] = empty($val->image) ? '' : $val->image;
            $items[$key]['content'] = empty($val->content) ? '' : $val->content;
            $items[$key]['sort'] = $val->sort;
            $items[$key]['type'] = $val->type;
            if ($val->suspend == 0) {
                $items[$key]['status'] = true;
            } else {
                $items[$key]['status'] = false;
            }
        }
        $this->setColumns([
                ['title' => 'ID', 'key' => 'id', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '图片', 'key' => 'image', 'minWidth' => 200, 'align' => 'center', 'slot' => 'image'],
                ['title' => '内容', 'key' => 'content', 'minWidth' => 120, 'align' => 'center'],
                ['title' => '排序', 'key' => 'sort', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '类型', 'key' => 'type', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '状态', 'key' => 'status', 'minWidth' => 80, 'align' => 'center', 'slot' => 'switch'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 180, 'align' => 'center', 'slot' => 'edit'],
            ]
        );
        $total = $model->total();
        $page = $this->getPage();
        $pageSize = $this->getPageSize();
        $columns = $this->getColumns();
        $this->setData(compact('items', 'total', 'page', 'pageSize', 'columns'));

    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    private int $page = 1;

    private int $pageSize = 10;

    private array $data = [];

    private array $columns = [];
}
