<?php


namespace App\Logic\Banner;


use App\Contracts\Banner\DeleteBannerInterface;
use App\Models\Banner;

class DeleteBannerLogic implements DeleteBannerInterface
{

    public function execute(): void
    {
        $model = Banner::query()
            ->findOrFail($this->getId());
        $model->delete();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    private int $id = 0;

}
