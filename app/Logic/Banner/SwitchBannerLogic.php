<?php


namespace App\Logic\Banner;


use App\Contracts\Banner\SwitchBannerInterface;
use App\Models\Banner;

class SwitchBannerLogic implements SwitchBannerInterface
{
    public function execute(): void
    {
        Banner::query()
            ->where('id', $this->getId())
            ->update([
                'suspend' => $this->getStatus() == true ? 0 : 1,
            ]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    private int $id = 0;

    private string $status = '';

    private array $data = [];

}
