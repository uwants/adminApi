<?php


namespace App\Logic\Banner;


use App\Contracts\Banner\GetBannerInterface;
use App\Models\Banner;

class GetBannerLogic implements GetBannerInterface
{
    public function execute(): void
    {
        $model = Banner::query()
            ->firstOrFail($this->id);
        $this->setData($model->toArray());
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    private int $id = 0;

    private array $data = [];
}
