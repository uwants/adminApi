<?php

namespace App\Logic\Keyword;

use App\Contracts\Keyword\DelAllKeywordInterface;
use App\Models\AppletKeywords;

class DelAllKeywordLogic implements DelAllKeywordInterface
{
    protected array $ids = [];

    public function execute(): void
    {
        AppletKeywords::query()
            ->whereIn('id', $this->getIds())
            ->delete();
    }

    /**
     * @return array
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * @param array $ids
     */
    public function setIds(array $ids): void
    {
        $this->ids = $ids;
    }


}
