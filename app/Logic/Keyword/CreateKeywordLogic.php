<?php

namespace App\Logic\Keyword;

use App\Contracts\Keyword\CreateKeywordInterface;
use App\Models\AppletKeywords;

class CreateKeywordLogic implements CreateKeywordInterface
{
    protected string $value = '';
    protected array $data = [];

    public function execute(): void
    {
        $list = explode('/', $this->getValue());
        //判断词是否存在
        $haveMsg = AppletKeywords::withTrashed()
            ->whereIn('value', $list)
            ->get()
            ->toArray();
        $j = 0;
        $i = 0;
        $haveValue = '';
        $insertValue = [];
        $restore = [];
        foreach ($list as $key => $val) {
            if (empty($val)) {
                continue;
            }
            $check = $this->checkMsg($haveMsg, $val);
            if ($check['type'] == 'have') {
                $haveValue .= $val . ',';
                $j++;
            } elseif ($check['type'] == 'delete') {
                $restore[] = $check['id'];
                $i++;
            } else {
                $insertValue[$key]['value'] = $val;
                $insertValue[$key]['created_at'] = date('Y-m-d H:i:s', time());
                $insertValue[$key]['updated_at'] = date('Y-m-d H:i:s', time());
                $i++;
            }
        }
        if ($insertValue) {
            AppletKeywords::query()
                ->insert($insertValue);
        }
        if (!empty($restore)) {
            AppletKeywords::withTrashed()
                ->whereIn('id', $restore)
                ->restore();
        }
        if (!empty($haveValue)) {
            $message = '成功添加 ' . $j . ' 关键字，数据库已经存在关键字：' . $haveValue;
        } else {
            $message = '成功添加 ' . $i . ' 关键字';
        }
        $list = AppletKeywords::query()
            ->select('value', 'id')
            ->orderBy('id', 'desc')
            ->get()
            ->toArray();
        $this->setData(compact('message', 'list'));

    }

    public function checkMsg($data, $value)
    {
        $type = '';
        $id = 0;
        foreach ($data as $key => $val) {
            if ($value == $val['value'] && $val['deleted_at'] == '') {
                $type = 'have';
                $id = $val['id'];
                break;
            }
            if ($value == $val['value'] && $val['deleted_at'] != '') {
                $type = 'delete';
                $id = $val['id'];
                break;
            }
        }
        return ['id' => $id, 'type' => $type];
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }


}
