<?php

namespace App\Logic\Keyword;

use App\Contracts\Keyword\DeleteKeywordInterface;
use App\Models\AppletKeywords;

class DeleteKeywordLogic implements DeleteKeywordInterface
{
    protected int $id = 0;

    public function execute(): void
    {
        AppletKeywords::query()
            ->where('id', '=', $this->getId())
            ->delete();
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}
