<?php

namespace App\Logic\Keyword;

use App\Contracts\Keyword\GetKeywordInterface;
use App\Models\AppletKeywords;

class GetKeywordLogic implements GetKeywordInterface
{
    protected string $keyword = '';
    protected array $data = [];

    public function execute(): void
    {
        $model = AppletKeywords::query();
        if ($this->getKeyword()) {
            $model->where('value', 'like', '%' . $this->getKeyword() . '%');
        }
        $model->orderBy('id', 'desc');

        $items = $model->get()->toArray();
        $total = count($items);
        $this->setData(compact('total', 'items'));
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
