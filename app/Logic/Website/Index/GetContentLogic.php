<?php

namespace App\Logic\Website\Index;

use App\Contracts\Website\Index\GetContentInterface;

class GetContentLogic implements GetContentInterface
{
    protected array $data = [];
    protected string $mediaId = '';
    protected string $title = '';

    public function execute(): void
    {
        $access_token = newAccessToken();

        $postOne = array('media_id' => $this->getMediaId());

        $req_str = json_encode($postOne, JSON_UNESCAPED_UNICODE);
        $url = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=$access_token";
        $code_json_list = curl_post($url, $req_str);
        $code_arr_tpList = json_decode($code_json_list, true);
        $returnList = [];
        foreach ($code_arr_tpList['news_item'] as $key => $val) {
            if ($val['title'] == $this->getTitle()) {
                $val['content'] = str_replace("data-src", "src", $val['content']);
                $returnList = $val;
            }
        }
        $this->setData($returnList);

    }


    public function getData(): array
    {
        return $this->data;
    }


    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getMediaId(): string
    {
        return $this->mediaId;
    }

    /**
     * @param string $mediaId
     */
    public function setMediaId(string $mediaId): void
    {
        $this->mediaId = $mediaId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }


}
