<?php

namespace App\Logic\Website\Index;

use App\Contracts\Website\Index\ListSortInterface;
use App\Models\Comments;
use App\Models\Companies;
use Illuminate\Support\Facades\DB;

class ListSortLogic implements ListSortInterface
{
    protected array $data = [];

    public function execute(): void
    {
        $seven = date("Y-m-d 00:00:00", strtotime("-7 day"));
        $today = date("Y-m-d 23:59:59", strtotime("-1 day"));
        $showTime = [date("m/d", strtotime("-7 day")), date("m/d", strtotime("-1 day"))];
        //企业收到的意见
        $company = Companies::query()
            ->select('id', 'name',
                DB::raw("(select count(id) from `comments` where `box_id` in (select `id` from `boxes` where boxes.company_id = companies.id) and comments.created_at between '" . $seven . "' and '" . $today . "' and `comments`.`deleted_at` is null ) as number"))
            ->orderBy('number', 'desc')
            ->limit(10)
            ->get();
        $this->setData(compact('company', 'showTime'));
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
