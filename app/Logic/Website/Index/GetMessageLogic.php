<?php

namespace App\Logic\Website\Index;

use App\Contracts\Website\Index\GetMessageInterface;
use App\Models\Boxes;
use App\Models\Comments;

class GetMessageLogic implements GetMessageInterface
{
    protected array $data = [];

    public function execute(): void
    {
        //个人意见箱数量
        $personalNum = Boxes::query()
            ->where('company_id', 0)
            ->count();
        //企业意见箱数量
        $enterpriseNum = Boxes::query()
            ->where('company_id', '>', 0)
            ->count();
        //用户收到的意见
        $individualOp = Comments::query()
            ->whereIn('box_id', function ($query) {
                $query->from('boxes')
                    ->select('id')
                    ->where('company_id', 0);
            })
            ->count();
        //企业收到的意见
        $enterpriseOp = Comments::query()
            ->whereIn('box_id', function ($query) {
                $query->from('boxes')
                    ->select('id')
                    ->where('company_id', '>', 0);
            })
            ->count();
        $this->setData(compact('personalNum', 'enterpriseNum', 'individualOp', 'enterpriseOp'));
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
