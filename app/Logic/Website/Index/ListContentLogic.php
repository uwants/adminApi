<?php

namespace App\Logic\Website\Index;

use App\Contracts\Website\Index\ListContentInterface;

class ListContentLogic implements ListContentInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected array $data = [];

    public function execute(): void
    {
        $access_token = newAccessToken();
        $offset = $this->getPageSize() * ($this->getPage() - 1);
        $postOne = array('type' => 'news', 'offset' => $offset, 'count' => $this->getPageSize());
        $req_str = json_encode($postOne, JSON_UNESCAPED_UNICODE);
        $url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=$access_token";
        $code_json_list = curl_post($url, $req_str);
        $code_arr_tpList = json_decode($code_json_list, true);
        $list = [];
        foreach ($code_arr_tpList['item'] as $key => $val) {
            foreach ($val['content']['news_item'] as $ke => $va) {
                $list[$key]['createYear'] = date('Y', $val['content']['create_time']);
                $list[$key]['createDay'] = date('m-d', $val['content']['create_time']);
                $list[$key]['title'] = $va['title'];
                $list[$key]['author'] = $va['author'];
                $list[$key]['digest'] = $va['digest'];
                $list[$key]['content'] = str_replace("data-src", "src", $va['content']);
                $list[$key]['mediaId'] = $val['media_id'];
                $list[$key]['thumbUrl'] = $va['thumb_url'];
                $list[$key]['url'] = $va['url'];
            }
        }
        $totalCount = empty($code_arr_tpList['total_count']) ? 0 : $code_arr_tpList['total_count'];
        $itemCount = empty($code_arr_tpList['item_count']) ? 0 : $code_arr_tpList['item_count'];
        $this->setData(compact('totalCount', 'itemCount', 'list'));

    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
