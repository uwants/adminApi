<?php

namespace App\Logic\Website\Index;

use App\Contracts\Website\Index\LoginCodeInterface;
use App\Models\WechatUsers;
use App\Models\User;
use Illuminate\Support\Carbon;

class LoginCodeLogic implements LoginCodeInterface
{
    protected array $data = [];
    protected string $code = '';

    public function execute(): void
    {
        $error = 0;
        $message = openPlatformToken($this->getCode());
        if (!empty($message->unionid)) {
            $model = WechatUsers::query()
                ->where('unionid', $message->unionid)
                ->first();
            if (empty($model->id)) {
                $token = '';
                $error = 10001;
                $wechat = '用户不存在，登录失败！';
                $this->setData(compact('token', 'error','wechat'));
            } else {
                //登录获取用户token
                $tokenExpiredAt = Carbon::now()->addDays(10)->toDateTimeString();
                $token = getWebToken();
                User::query()
                    ->where('id', $model->user_id)
                    ->update(['token' => $token, 'token_expired_at' => $tokenExpiredAt]);
                $this->setData(compact('token', 'error'));
            }
        } else {
            $error = 10002;
            $wechat = $message;
            $token = '';
            $this->setData(compact('token', 'error', 'wechat'));
        }
    }

    public function register(): void
    {
        $error = 0;
        $message = openPlatformToken($this->getCode());
        if (!empty($message->unionid)) {
            $model = WechatUsers::query()
                ->where('unionid', $message->unionid)
                ->first();
            if (empty($model->id)) {
                //注册
                $token = $this->insertUser($message);
                $error = 0;
                $wechat = '登录成功！';
                $this->setData(compact('token', 'error','wechat'));
            } else {
                //登录获取用户token
                $tokenExpiredAt = Carbon::now()->addDays(10)->toDateTimeString();
                $token = getWebToken();
                User::query()
                    ->where('id', $model->user_id)
                    ->update(['token' => $token, 'token_expired_at' => $tokenExpiredAt]);
                $this->setData(compact('token', 'error'));
            }
        } else {
            $error = 10002;
            $wechat = $message;
            $token = '';
            $this->setData(compact('token', 'error', 'wechat'));
        }
    }

    public function insertUser($data)
    {
        $tokenExpiredAt = Carbon::now()->addDays(10)->toDateTimeString();
        $token = getWebToken();
        $id = User::query()
            ->insertGetId(array(
                'name' => $data->nickname,
                'avatar' => $data->headimgurl,
                'mobile' => '',
                'country_code' => 86,
                'token' => $token,
                'token_expired_at' => $tokenExpiredAt,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ));
        WechatUsers::query()
            ->insert([
                'user_id' => $id,
                'unionid' => $data->unionid,
                'session_key' => '',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ]);
        return $token;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

}
