<?php

namespace App\Logic\Website\Home;

use App\Contracts\Website\Home\GetHomeInterface;
use App\Models\Boxes;
use App\Models\BoxVisits;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;

class GetHomeLogic implements GetHomeInterface
{
    protected array $data = [];
    protected int $id = 0;

    public function execute(): void
    {
        //个人
        $personalBox = Boxes::query()
            ->where('user_id', $this->getId())
            ->where('company_id', '=', 0)
            ->count('id');
        //企业
        $companyBox = Boxes::query()
            ->where('user_id', $this->getId())
            ->where('company_id', '>', 0)
            ->count('id');
        $totalBox = $personalBox + $companyBox;
        $boxList = compact('personalBox', 'companyBox', 'totalBox');
        $suggestions = $this->suggestions();//意见箱累计访问量
        $opinionList = $this->opinionList();//回收意见累计数量
        $newOpinion = $this->newOpinion();
        $statisticalTime = date('Y-m-d H:i:s', time());
        $this->setData(compact('statisticalTime', 'boxList', 'suggestions', 'opinionList', 'newOpinion'));
    }

    public function suggestions()
    {
        $timeList = [];
        $dataList = [];
        $total = 0;
        for ($i = 1; $i <= 7; $i++) {
            $start = date("Y-m-d 00:00:00", strtotime("-$i day"));
            $end = date("Y-m-d 23:59:59", strtotime("-$i day"));
            $model = BoxVisits::query()
                ->whereIn('box_id', function ($query) {
                    $query->select('id')
                        ->from('boxes')
                        ->where('user_id', $this->getId());
                })
                ->whereBetween('created_at', [$start, $end])
                ->count('id');
            $total = $total + $model;
            $dataList[] = $model;
            $timeList[] = date('m/d', strtotime($start));
        }
        $timeList = array_reverse($timeList);
        $dataList = array_reverse($dataList);
        return compact('total', 'timeList', 'dataList');
    }

    public function opinionList()
    {
        $timeList = [];
        $dataList = [];
        $total = 0;
        for ($i = 1; $i <= 7; $i++) {
            $start = date("Y-m-d 00:00:00", strtotime("-$i day"));
            $end = date("Y-m-d 23:59:59", strtotime("-$i day"));
            $model = Comments::query()
                ->whereIn('box_id', function ($query) {
                    $query->select('id')
                        ->from('boxes')
                        ->where('user_id', $this->getId());
                })
                ->whereBetween('created_at', [$start, $end])
                ->count('id');
            $total = $total + $model;
            $dataList[] = $model;
            $timeList[] = date('m/d', strtotime($start));
        }
        $timeList = array_reverse($timeList);
        $dataList = array_reverse($dataList);
        return compact('total', 'timeList', 'dataList');

    }

    public function newOpinion(): array
    {
        $model = Comments::query()
            ->select('id', 'user_id', 'content', 'created_at', 'box_id')
            ->with('user')
            ->whereIn('box_id', function ($query) {
                $query->select('id')
                    ->from('boxes')
                    ->where('user_id', $this->getId());
            })
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
        $list = [];
        foreach ($model as $key => $val) {
            $list[$key]['nickName'] = empty($val->user->name) ? '' : $val->user->name;
            $list[$key]['avatar'] = empty($val->user->avatar) ? '' : $val->user->avatar;
            $msg = json_decode($val->content);
            $list[$key]['content'] = empty($msg->content) ? '' : $msg->content;
            $list[$key]['createTime'] = date('Y-m-d', strtotime($val->created_at));
        }
        return $list;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
