<?php

namespace App\Logic\WechatTemplate;

use App\Contracts\WechatTemplate\SendTemplateInterface;
use App\Models\WechatTemplates;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Exception;

class SendTemplateLogic implements SendTemplateInterface
{
    protected array $ids = [];
    protected string $templateId = '';
    protected string $jumpUrl = '';
    protected string $jumpType = '';
    protected array $message = [];
    protected array $data = [];

    public function execute(): void
    {
        $params = $this->getMessage();
        if (empty($params['keyword1']['value'])) {
            $keyword1 = '';
        } else {
            $keyword1 = $params['keyword1']['open'] ?
                date('Y-m-d H:i', strtotime($params['keyword1']['value'])) :
                $params['keyword1']['value'];
        }

        if (empty($params['keyword2']['value'])) {
            $keyword2 = '';
        } else {
            $keyword2 = $params['keyword2']['open'] ?
                date('Y-m-d H:i', strtotime($params['keyword2']['value'])) :
                $params['keyword2']['value'];
        }

        if (empty($params['keyword3']['value'])) {
            $keyword3 = '';
        } else {
            $keyword3 = $params['keyword3']['open'] ?
                date('Y-m-d H:i', strtotime($params['keyword3']['value'])) :
                $params['keyword3']['value'];
        }

        if (empty($params['keyword4']['value'])) {
            $keyword4 = '';
        } else {
            $keyword4 = $params['keyword4']['open'] ?
                date('Y-m-d H:i', strtotime($params['keyword4']['value'])) :
                $params['keyword4']['value'];
        }
        $remark = empty($params['remark']['value']) ? '' : $params['remark']['value'];
        $sendMsg = array(
            'userOpenid' => '',
            'template_id' => $this->getTemplateId(),
            'url' => empty($this->getJumpUrl()) ? '' : $this->getJumpUrl(),
            'first' => $params['first']['value'],
            'keyword1' => $keyword1,
            'keyword2' => $keyword2,
            'keyword3' => $keyword3,
            'keyword4' => $keyword4,
            'remark' => $remark
        );
        //跳转小程序
        if ($this->getJumpType()) {
            $sendMsg["pagepath"] = empty($this->getJumpUrl()) ? '' : $this->getJumpUrl();
        }
        $sendLost = 0;
        $message = '';
        foreach ($this->getIds() as $key => $val) {
            $sendMsg['userOpenid'] = $val['openId'];
            if (!empty($val['openId'])) {
                $msg = $this->sendTemplate($sendMsg);
                if ($msg['code'] != 0) {
                    $sendLost++;
                    $message .= $val['nickName'] . '(' . $msg['data'] . ')' . '，';
                }
            } else {
                $sendLost++;
                $message .= $val['nickName'] . '，';
            }

        }
        if($sendLost>0){
            $message='发送失败用户：'.trim($message,'，');
        }else{
            $message='发送成功！';
        }
        $this->setData(compact('message'));

    }

    /**
     * @return array
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * @param array $ids
     */
    public function setIds(array $ids): void
    {
        $this->ids = $ids;
    }

    /**
     * @return string
     */
    public function getTemplateId(): string
    {
        return $this->templateId;
    }

    /**
     * @param string $templateId
     */
    public function setTemplateId(string $templateId): void
    {
        $this->templateId = $templateId;
    }

    /**
     * @return array
     */
    public function getMessage(): array
    {
        return $this->message;
    }

    /**
     * @param array $message
     */
    public function setMessage(array $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getJumpUrl(): string
    {
        return $this->jumpUrl;
    }

    /**
     * @param string $jumpUrl
     */
    public function setJumpUrl(string $jumpUrl): void
    {
        $this->jumpUrl = $jumpUrl;
    }

    /**
     * @return string
     */
    public function getJumpType(): string
    {
        return $this->jumpType;
    }

    /**
     * @param string $jumpType
     */
    public function setJumpType(string $jumpType): void
    {
        $this->jumpType = $jumpType;
    }


    public function sendTemplate(array $sendMsg): array
    {
        $post = array(
            "touser" => $sendMsg['userOpenid'],
            "template_id" => $sendMsg['template_id'], // 模板 id
            "url" => empty($sendMsg['url']) ? '' : $sendMsg['url'],
            "data" => array(
                "first" => array(
                    "value" => empty($sendMsg['first']) ? '超级意见箱' : $sendMsg['first'],
                    "color" => "#173177"
                ),
                "keyword1" => array(
                    "value" => empty($sendMsg['keyword1']) ? '' : $sendMsg['keyword1'],
                    "color" => "#173177",
                ),
                "keyword2" => array(
                    "value" => empty($sendMsg['keyword2']) ? '' : $sendMsg['keyword2'],
                    "color" => "#173177",
                ),
                "keyword3" => array(
                    "value" => empty($sendMsg['keyword3']) ? '' : $sendMsg['keyword3'],
                    "color" => "#173177",
                ),
                "keyword4" => array(
                    "value" => empty($sendMsg['keyword4']) ? '' : $sendMsg['keyword4'],
                    "color" => "#173177",
                ),
                "remark" => array(
                    "value" => empty($sendMsg['remark']) ? '点击查看详情！' : $sendMsg['remark'],
                    "color" => "#173177",
                )
            ),
        );

        //小程序跳转
        if (!empty($sendMsg['pagepath'])) {
            $post['miniprogram'] = array(
                "appid" => env('WX_APP_ID'),
                "pagepath" => empty($sendMsg['pagepath']) ? '' : $sendMsg['pagepath']
            );
        }
        try {
            $token = $this->getSAppAccessToken();
        } catch (Exception $e) {
        }
        /** @var  $token */
        $req_url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $token['token'];
        $req_str = json_encode($post);
        $res_str = $this->curl_post($req_url, $req_str);
        $res = json_decode($res_str, true);

        if ($res['errcode'] != "0") {
            DB::table('wechat_template_logs')->insert([
                'msgid' => '',
                'openId' => $sendMsg['userOpenid'],
                'template_id' => $sendMsg['template_id'],
                'errcode' => $res['errcode'],
                'errmsg' => $res['errmsg'],
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            return ['code' => -1, 'message' => '', 'data' => $res['errmsg']];
        } else {
            DB::table('wechat_template_logs')->insert([
                'msgid' => $res['msgid'],
                'openid' => $sendMsg['userOpenid'],
                'template_id' => $sendMsg['template_id'],
                'errcode' => $res['errcode'],
                'errmsg' => $res['errmsg'],
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);
            return ['code' => 0, 'message' => '', 'data' => ''];
        }

    }

    /**
     * @return array
     * @throws Exception
     */
    public function getSAppAccessToken(): array
    {
        $appId = env('WX_SER_APP_ID');
        $secret = env('WX_SER_APP_SECRET');

        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appId&secret=$secret";

        $accessToken = Cache::get('app_access_token_' . $appId);

        if (!empty($accessToken)) {
            $access_token = $accessToken;
        } else {
            $mes = $this->curl_get($url, []);
            $re_mes = json_decode($mes);
            if (!empty($re_mes->errcode)) {
                return ['token' => '', 'message' => $re_mes->errmsg];
            }
            Cache::put('app_access_token_' . $appId, $re_mes->access_token, '100');
            $access_token = $re_mes->access_token;
        }
        return ['token' => $access_token, 'message' => ''];
    }

    public function curl_get($url, $headOPT = [])
    {
        if (empty($headOPT)) {
            $headOPT = array('platform_src:WAP', 'cookie_id:', 'systype:wap');
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 获取数据返回
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true); // 在启用  时候将获取数据返回
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headOPT);//修改header信息
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 8);

        $out_put = curl_exec($ch);
        curl_close($ch);
        unset($ch);
        return $out_put;
    }

    public function curl_post($url, $curlPost, $headerOpt = [])
    {

        if (empty($headerOpt)) {
            $headerOpt = array('platform_src:WAP', 'cookie_id:', 'systype:wap');
        }
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerOpt);//修改header信息
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $out_put = curl_exec($ch);//运行curl
        curl_close($ch);
        return $out_put;
    }


}
