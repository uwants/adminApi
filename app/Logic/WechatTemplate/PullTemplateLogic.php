<?php

namespace App\Logic\WechatTemplate;

use App\Contracts\WechatTemplate\PullTemplateInterface;
use App\Models\WechatTemplates;
use App\Contracts\WechatTemplate\SendTemplateInterface;
use Illuminate\Support\Facades\DB;

class PullTemplateLogic implements PullTemplateInterface
{
    protected array $data = [];
    private SendTemplateInterface $send;

    public function __construct(SendTemplateInterface $send)
    {
        $this->send = $send;
    }

    public function execute(): void
    {
        $token = $this->send->getSAppAccessToken();
        //获取模板列表
        $url = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=" . $token['token'];
        $post = '{}';
        $code_json_list = $this->send->curl_post($url, $post);
        $code_arr_list = json_decode($code_json_list, true);

        DB::beginTransaction();
        WechatTemplates::withTrashed()->forceDelete();
        $insertList = [];
        foreach ($code_arr_list['template_list'] as $key => $val) {
            $insertList[$key]['template_title'] = $val['title'];
            $insertList[$key]['template_id'] = $val['template_id'];
            $insertList[$key]['content'] = $val['content'];
            $insertList[$key]['primary_industry'] = $val['primary_industry'];
            $insertList[$key]['deputy_industry'] = $val['deputy_industry'];
            $insertList[$key]['example'] = $val['example'];
            $insertList[$key]['created_at'] = date('Y-m-d H:i:s',time());
        }
        if (WechatTemplates::query()->insert($insertList)) {
            DB::commit();
        }

    }

    public function getData(): array
    {
        return $this->data;
    }

}
