<?php

namespace App\Logic\WechatTemplate;

use App\Contracts\WechatTemplate\GetTemplateInterface;
use App\Models\WechatTemplates;

class GetTemplateLogic implements GetTemplateInterface
{
    protected int $id = 0;
    protected array $data = [];

    public function execute(): void
    {
        $model = WechatTemplates::query()
            ->where('id', $this->getId())
            ->first();
        if (empty($model->id)) {
            throw new Exception('模板消息不存在！');
        }
        $new_explode = str_replace(array("\r\n", "\r", "\n"), "<br>", $model->example);
        $show_explode = explode("<br>", $new_explode);
        $show_array = [];
        $count_num = count($show_explode);
        foreach ($show_explode as $ke => $va) {
            if ($ke == 0) {
                $show_array['first']['title'] = "副标题：";
                $show_array['first']['value'] = $va;
                $show_array['first']['open'] = false;
                $show_array['first']['button'] = false;
                continue;
            }
            if ($ke == $count_num - 1) {
                $show_array['remark']['title'] = "备注：";
                $show_array['remark']['value'] = $va;
                $show_array['remark']['open'] = false;
                $show_array['remark']['button'] = false;
                continue;
            }
            $list = explode("：", $va);
            $show_array["keyword" . $ke]['title'] = $list[0] . "：";
            $show_array["keyword" . $ke]['value'] = $list[1];
            $show_array["keyword" . $ke]['open'] = false;
            $show_array["keyword" . $ke]['button'] = true;
        }
        $msgHeader = $show_array;
        $title = $model->template_title;
        $templateId = $model->template_id;
        $this->setData(compact('title', 'msgHeader', 'templateId'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
