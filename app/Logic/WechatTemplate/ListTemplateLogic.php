<?php

namespace App\Logic\WechatTemplate;

use App\Contracts\WechatTemplate\ListTemplateInterface;
use App\Models\WechatTemplates;

class ListTemplateLogic implements ListTemplateInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '模板标题', 'key' => 'templateTitle','minWidth' => 100,'ellipsis'=>true,'tooltip'=>true],
            ['title' => '第一产业', 'key' => 'primaryIndustry', 'minWidth' => 80,'align' => 'center','ellipsis'=>true,'tooltip'=>true],
            ['title' => '副业', 'key' => 'deputyIndustry', 'minWidth' => 80,'align' => 'center'],
            ['title' => '更新时间', 'key' => 'updateTime', 'minWidth' => 150,'align' => 'center'],
            ['title' => '范例', 'key' => 'showExplode', 'minWidth' => 220,'slot'=>'header',],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 120,'align' => 'center','slot'=>'edit',],
        ]);
        $model = WechatTemplates::query()->where('example','!=','');
        //订单关键字查找
        if ($this->getKeyword()) {
            $model->where('template_title', 'like', "%" . $this->getKeyword() . "%");
        }
        $model->orderBy('id', 'desc');
        $article = $model->paginate($this->getPageSize());
        $item = $article->items();
        $this->setTotal($article->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['newExplode'] = str_replace(array("\r\n", "\r", "\n"), "<br>", $val->example);
            $items[$key]['showExplode'] = explode("<br>",$items[$key]['newExplode']);
            $items[$key]['updateTime'] = date($val->created_at);
            $items[$key]['templateTitle']=$val->template_title;
            $items[$key]['primaryIndustry']=$val->primary_industry;
            $items[$key]['deputyIndustry']=$val->deputy_industry;
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }





}
