<?php

namespace App\Logic\Users;

use App\Contracts\Users\ListUserOpInterface;
use Illuminate\Support\Facades\DB;
use App\Models\Comments;

class ListUserOpLogic implements ListUserOpInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $userId = 0;
    protected int $total = 0;
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '意见箱', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '提出人', 'key' => 'userName', 'minWidth' => 110, 'align' => 'center'],
            ['title' => '意见内容', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '是否匿名', 'key' => 'anonymous', 'minWidth' => 80, 'align' => 'center'],
            ['title' => '提出时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
        ]);
        $opinion = Comments::query()
            ->with('box')
            ->with('user')
            ->where('user_id', $this->getUserId());
        $opinion->orderBy('created_at', 'desc');
        $opList = $opinion->paginate($this->getPageSize());
        $item = $opList->items();
        $this->setTotal($opList->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->box->name;
            $items[$key]['userName'] = $val->user->name;
            $content = json_decode($val->content);
            $items[$key]['content'] = empty($content->content) ? '' : $content->content;
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['anonymous'] = $val->anonymous == 1 ? '是' : '否';
            $items[$key]['adopt'] = $val->adopt == 1 ? '是' : '否';
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
