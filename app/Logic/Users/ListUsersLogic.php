<?php

namespace App\Logic\Users;

use App\Contracts\Users\ListUsersInterface;
use App\Models\InternalUser;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class ListUsersLogic implements ListUsersInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $searchType = '';
    protected string $exportData = '';
    protected string $sortColumn = '';
    protected string $sortVal = '';
    protected array $data = [];
    protected array $columns = [];
    protected array $time = [];
    protected bool $filter = true;

    public function execute(): void
    {
        if ($this->getExportData() == 1) {
            $this->setColumns([
                ['title' => '用户ID', 'key' => 'id'],
                ['title' => '用户昵称', 'key' => 'nickName'],
                ['title' => '用户手机', 'key' => 'mobile'],
                ['title' => '是否企业超级管理员、管理员', 'key' => 'admin'],
                ['title' => '创建意见箱数量', 'key' => 'boxNum'],
                ['title' => '发表意见数量', 'key' => 'opinionNum'],
            ]);
        } elseif ($this->getExportData() == 2) {
            $this->setColumns([
                ['type' => 'selection', 'width' => 50, 'align' => 'center'],
                ['title' => '用户信息', 'key' => 'id', 'minWidth' => 250, 'align' => 'left', 'slot' => 'header'],
                ['title' => '所属企业', 'key' => 'company', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '意见箱数', 'key' => 'boxNum', 'minWidth' => 85, 'align' => 'center'],
                ['title' => '提意见数', 'key' => 'opinionNum', 'minWidth' => 85, 'align' => 'center'],
                ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 120, 'align' => 'center'],
                ['title' => '注册时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ]);
        } else {
            $this->setColumns([
                ['title' => '用户信息', 'key' => 'id', 'minWidth' => 250, 'sortable' => 'custom', 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '所属企业', 'key' => 'company', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
                ['title' => '意见箱数', 'key' => 'boxNum', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '提意见数', 'key' => 'opinionNum', 'minWidth' => 110, 'sortable' => 'custom', 'align' => 'center'],
                ['title' => '超管/管理员', 'key' => 'admin', 'minWidth' => 100, 'slot' => 'admin', 'align' => 'center'],
                ['title' => '是否关注公众号', 'key' => 'follow', 'minWidth' => 120, 'align' => 'center', 'slot' => 'follow'],
                ['title' => '注册时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'minWidth' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
            ]);
        }
        $userModel = User::query()
            ->with(['userUnionid' => function ($query) {
                $query->with('wapOpenId');
            }])
            ->with('companyUser')
            ->with('company')
            ->select('mobile', 'id', 'created_at', 'name', 'avatar',
                DB::raw("(select count(id) from boxes where boxes.user_id = users.id and deleted_at is null) as boxNum"),
                DB::raw("(select count(id) from comments where comments.user_id = users.id and deleted_at is null) as opinionNum"),
            );
        //
        if ($this->getExportData() == 2) {
            $userModel->whereIn('id', function ($query) {
                $query->select('user_id')->from('wechat_users')
                    ->whereIn('unionid', function ($wechat) {
                        $wechat->select('unionid')->from('wechat_user_subscribes')
                            ->where('deleted_at', null);
                    });
            });
        }
//        过滤代码
        if ($this->isFilter()) {
            $userModel->whereNotIn('id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }

        //订单关键字查找
        if ($this->getKeyword()) {
            switch ($this->getSearchType()) {
                case'mobile'://订单号
                    $userModel->where('mobile', 'like', "%" . $this->getKeyword() . "%");
                    break;
                case'ID':
                    $userModel->where('id', $this->getKeyword());
                    break;
                case'nickName':
                    $userModel->where('name', 'like', "%" . $this->getKeyword() . "%");
                    break;
            }
        }
        if (!empty($this->time[0])) {
            $start = date('Y - m - d H:i:s', strtotime($this->time[0]));
            $end = date('Y - m - d H:i:s', strtotime($this->time[1]));
            $userModel->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $userModel->orderBy($this->getSortColumn(), $this->getSortVal());
        } else {
            $userModel->orderBy('id', 'desc');
        }

        if ($this->getExportData() == 1) {
            $item = $userModel->get();
        } else {
            $user = $userModel->paginate($this->getPageSize());
            $item = $user->items();
            $this->setTotal($user->total());
        }
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];

        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['mobile'] = $val->mobile;
            if ($this->getExportData() == 1) {
                $items[$key]['nickName'] = filterEmoji($val->name);
            } else {
                $items[$key]['nickName'] = $val->name;
            }
            $items[$key]['headImg'] = showImage($val->avatar, 'avatar');
            $items[$key]['createTime'] = date($val->created_at);
            $items[$key]['boxNum'] = $val->boxNum;
            $items[$key]['opinionNum'] = $val->opinionNum;
            $items[$key]['follow'] = empty($val->userUnionid->wapOpenId->id) ? '否' : '是';
            $items[$key]['wapOpenId'] = empty($val->userUnionid->wapOpenId->openid) ? '' : $val->userUnionid->wapOpenId->openid;
            $items[$key]['company'] = '';
            $items[$key]['admin'] = '否';
            foreach ($val->companyUser as $user) {
                if ($user->role == 'ROLE_SUPPER_ADMINISTRATOR' || $user->role == 'ROLE_ADMINISTRATOR') {
                    $items[$key]['admin'] = '是';
                    break;
                } else {
                    $items[$key]['admin'] = '否';
                }
            }
            foreach ($val->company as $va) {
                $items[$key]['company'] .= $va->name . ',';
            }
            $items[$key]['company'] = empty($items[$key]['company']) ? '--' : trim($items[$key]['company'], ',');
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getSearchType(): string
    {
        return $this->searchType;
    }

    /**
     * @param string $searchType
     */
    public function setSearchType(string $searchType): void
    {
        $this->searchType = $searchType;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return string
     */
    public function getExportData(): string
    {
        return $this->exportData;
    }

    /**
     * @param string $exportData
     */
    public function setExportData(string $exportData): void
    {
        $this->exportData = $exportData;
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->filter;
    }

    /**
     * @param bool $filter
     */
    public function setFilter(bool $filter): void
    {
        $this->filter = $filter;
    }


}
