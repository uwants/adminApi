<?php

namespace App\Logic\Users;

use App\Contracts\Users\ListUserOrderInterface;
use App\Models\Orders;
use Illuminate\Support\Arr;

class ListUserOrderLogic implements ListUserOrderInterface
{
    protected int $id = 0;
    protected string $status = '';
    protected array $data = [];

    public function execute(): void
    {
        $items = [];
        $type = [];
        switch ($this->getStatus()) {
            case'wait':
                $type = ['WAIT_ASSESSMENT'];
                break;
            case'service':
                $type = ['ARRANGE_WORKER', 'WAITER_PAYMENT', 'PAID_DEPOSIT'];
                break;
            case'finish':
                $type = ['TRADE_CLOSE'];
                break;
            case'cancel':
                $type = ['TRADE_CANCEL'];
                break;
            case'after':
                $type = ['AFTER_SALES', 'AFTER_SERVICE', 'AFTER_CLOSE'];
                break;
        }
        $item = Orders::query()
            ->with(['orderWorkers' => function ($query) {
                $query->with('worker');
            }])
            ->with(['address' => function ($query) {
                $query->select('id', 'contact_name', 'mobile', 'province_name', 'city_name', 'county_name', 'detail');
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with('evaluate')
            ->where('user_id', $this->getId())
            ->whereIn('status', $type)
            ->orderBy('id', 'desc')
            ->get();
        foreach ($item as $ley => $val) {
            $video = json_decode($val->videos);
            $orderData = [
                'id' => $val->id,
                'description' => $val->description,
                'videos' => empty($video[0]) ? '' : $video[0],
                'images' => json_decode($val->images),
                'orderNumber' => $val->order_number,
                'serviceTime' => $val->service_time,
                'notifyMessage' => $val->notify_message,
                'amount' => $val->amount,
                'paidAmount' => $val->paid_amount,
                'statusDesc' => $val->status_desc,
                'status' => $val->status,
                'depositAmount' => $val->deposit_amount,
                'unpaidAmount' => $val->unpaid_amount,
                'isAfterSale' => $val->parent_id ? true : false,
                'createTime' => date($val->created_at),
                'cancelReason' => $val->cancel_reason ? $val->cancel_reason : '',
                'applyRefundTime' => $val->apply_refund_time ? $val->apply_refund_time : '',
                'depositPaidSn' => $val->deposit_paid_sn ? $val->deposit_paid_sn : '',//定金
                'balancePaidSn' => $val->balance_paid_sn ? $val->balance_paid_sn : '',//尾款
                'depositRate' => number_format($val->deposit_rate * 100, 2),
            ];
            $address = $val->address;
            if ($address) {
                Arr::set($orderData, 'address.contactName', empty($address->contact_name) ? '' : $address->contact_name);
                Arr::set($orderData, 'address.mobile', $address->mobile);
                Arr::set($orderData, 'address.address', Arr::get($address, 'full_address'));
            }
            $workers = $val->orderWorkers;
            if ($workers) {
                $workerList = '';
                foreach ($workers as $ke => $va) {
                    $workerList .= $va->worker->name . '、';
                }
                Arr::set($orderData, 'worker', trim($workerList, '、'));
            } else {
                Arr::set($orderData, 'worker', 0);
            }
            //评价
            $evaluate = $val->evaluate;
            if ($evaluate) {
                foreach ($evaluate as $k => $v) {
                    Arr::set($orderData, "evaluate.$k.starNumber", $v->star_number);
                    Arr::set($orderData, "evaluate.$k.comment", $v->comment);
                }
            }
            $items[] = $orderData;
        }
        $this->setData($items);
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

}
