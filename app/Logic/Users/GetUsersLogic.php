<?php

namespace App\Logic\Users;

use App\Contracts\Users\GetUsersInterface;
use App\Models\User;
use App\Models\Orders;
use App\Models\Address;
use Illuminate\Support\Facades\DB;

class GetUsersLogic implements GetUsersInterface
{
    protected int $id = 0;
    protected array $data = [];
    protected array $address = [];

    public function execute(): void
    {
        $user = User::query()
            ->with('company')
            ->where('id', $this->getId())
            ->first();
        if (!empty($user->id)) {
            $returnList['id'] = $user->id;
            $returnList['name'] = $user->name;
            $returnList['mobile'] = $user->mobile;
            $returnList['avatar'] = $user->avatar;
            $returnList['createTime'] = date($user->created_at);
            $returnList['updateTime'] = date($user->updated_at);
            $company = '';
            foreach ($user->company as $ke => $va) {
                $company .= $va->name . '、';
            }
            $returnList['company'] = empty($company) ? '--' : trim($company, '、');
        } else {
            $returnList = [];
        }
        $this->setData($returnList);
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

}
