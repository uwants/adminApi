<?php

namespace App\Logic\Company;

use App\Contracts\Company\ListHotCompanyInterface;
use App\Models\CompanySearchHots;
use Illuminate\Support\Facades\DB;

class ListHotCompanyLogic implements ListHotCompanyInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected array $data = [];
    protected array $columns = [];
    protected string $name = '';
    protected string $logo = '';
    protected int $id = 0;
    protected int $sort = 0;
    protected bool $status = true;
    protected string $sortColumn = '';
    protected string $sortVal = '';

    public function execute(): void
    {
        $this->setColumns([
            ['title' => 'ID', 'key' => 'id', 'width' => 50, 'align' => 'center'],
            ['title' => '企业logo', 'key' => 'logo', 'minWidth' => 120, 'align' => 'center', 'slot' => 'logo'],
            ['title' => '企业名字', 'key' => 'name', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '排序', 'key' => 'sort', 'minWidth' => 150, 'align' => 'center','sortable' => 'custom'],
            ['title' => '状态', 'key' => 'statusType', 'minWidth' => 150, 'align' => 'center', 'slot' => 'switch'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 150, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'minWidth' => 220, 'align' => 'center', 'slot' => 'edit'],
        ]);
        $model = CompanySearchHots::query();
        //关键字查找企业
        if ($this->getKeyword()) {
            $model->where('name', 'like', "%" . $this->getKeyword() . "%");
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $model->orderBy('sort', $this->getSortVal());
            $model->orderByRaw("FIND_IN_SET(status,'1,0')");
            $model->orderBy('id', 'desc');
        } else {
            $model->orderBy('sort', 'desc');
            $model->orderByRaw("FIND_IN_SET(status,'1,0')");
            $model->orderBy('id', 'desc');
        }

        $hot = $model->paginate($this->getPageSize());
        $item = $hot->items();
        $this->setTotal($hot->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['sort'] = empty($val->sort) ? 0 : $val->sort;
            $items[$key]['statusType'] = !empty($val->status);
            $items[$key]['logo'] = showImage($val->logo, 'company');
            $items[$key]['createTime'] = date($val->created_at);
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    public function update(): void
    {
        if ($this->getId() > 0) {
            CompanySearchHots::query()
                ->where('id', $this->getId())
                ->update([
                    'name' => $this->getName(),
                    'logo' => $this->getLogo(),
                    'status' => $this->isStatus(),
                    'sort' => $this->getSort(),
                ]);
        } else {
            CompanySearchHots::query()->firstOrCreate([
                'name' => $this->getName(),
                'logo' => $this->getLogo(),
                'status' => $this->isStatus(),
                'sort' => $this->getSort(),
            ]);
        }
    }

    public function delete(): void
    {
        CompanySearchHots::query()
            ->where('id', $this->getId())
            ->delete();
    }

    public function switch()
    {
        CompanySearchHots::query()
            ->where('id', $this->getId())
            ->update(['status' => $this->isStatus()]);
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo(string $logo): void
    {
        $this->logo = $logo;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
