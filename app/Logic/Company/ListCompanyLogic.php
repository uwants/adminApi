<?php

namespace App\Logic\Company;

use App\Contracts\Company\ListCompanyInterface;
use App\Models\Companies;
use Illuminate\Support\Facades\DB;

class ListCompanyLogic implements ListCompanyInterface
{

    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $status = '';
    protected string $companyStatus = '';
    protected string $exportData = '';
    protected string $sortColumn = '';
    protected string $sortVal = '';
    protected array $time = [];
    protected array $data = [];
    protected array $columns = [];
    protected bool $Filter = true;


    public function execute(): void
    {
        if ($this->getExportData() == 1) {
            set_time_limit(0);
            $this->setColumns([
                ['title' => '企业ID', 'key' => 'id'],
                ['title' => '企业名称', 'key' => 'companyName'],
                ['title' => '组织人数', 'key' => 'companyUser'],
                ['title' => '企业管理员', 'key' => 'admin'],
                ['title' => '管理员电话', 'key' => 'adminMobile'],
                ['title' => '累计创建意见箱条数', 'key' => 'companyBox'],
            ]);
        } else {
            $this->setColumns([
                ['title' => '公司信息', 'key' => 'id', 'minWidth' => 280, 'align' => 'left', 'slot' => 'header', 'ellipsis' => true, 'sortable' => 'custom'],
                ['title' => '所属行业', 'key' => 'industry', 'minWidth' => 100, 'align' => 'center'],
                ['title' => '组织人数', 'key' => 'companyUser', 'minWidth' => 110, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '意见箱', 'key' => 'companyBox', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '意见数', 'key' => 'companyOp', 'minWidth' => 100, 'align' => 'center', 'sortable' => 'custom'],
                ['title' => '企业状态', 'key' => 'companyStatus', 'minWidth' => 100, 'align' => 'center', 'slot' => 'status'],
                ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
                ['title' => '操作', 'key' => 'edit', 'width' => 210, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
            ]);
        }

        $model = Companies::query()
            ->select('*',
                DB::raw("(select count(id) from company_users where company_users.company_id=companies.id and deleted_at is null) as companyUser"),
                DB::raw("(select count(id) from boxes where boxes.company_id=companies.id and deleted_at is null) as companyBox"),
                DB::raw("(select count(id) from comments where box_id in (select id from boxes where boxes.company_id=companies.id and deleted_at is null) and deleted_at is null) as companyOp"),
            )
            ->with('user');

        //        过滤代码
        if ($this->isFilter()) {
            $model->whereNotIn('user_id', function ($query) {
                $query->select('user_id')->from('internal_users')
                    ->where('deleted_at', null);
            });
        }

        //关键字查找企业
        if ($this->getKeyword()) {
            $keyword = $this->getKeyword();
            switch ($this->getStatus()) {
                case'companyName':
                    $model->where('name', 'like', "%$keyword%");
                    break;
                case'industry':
                    $model->where('industry', 'like', "%$keyword%");
                    break;
            }
        }
        switch ($this->getCompanyStatus()) {
            case'certified':
                $model->where('certified_status', 'STATUS_CERTIFIED_PASS');
                break;
            case'noCertified':
                $model->where('certified_status', '!=', 'STATUS_CERTIFIED_PASS');
                break;
        }
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:s', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:s', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        // 排序
        if (!empty($this->getSortColumn()) && !empty($this->getSortVal())) {
            $model->orderBy($this->getSortColumn(), $this->getSortVal());
        } else {
            $model->orderBy('id', 'desc');
        }
        if ($this->getExportData() == 1) {
            $item = $model->get();
        } else {
            $apply = $model->paginate($this->getPageSize());
            $item = $apply->items();
            $this->setTotal($apply->total());
        }
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['companyName'] = $val->name;
            $items[$key]['companyUser'] = $val->companyUser;
            $items[$key]['companyBox'] = $val->companyBox;
            $items[$key]['companyOp'] = $val->companyOp;
            $items[$key]['logo'] = showImage($val->logo, 'company');
            $items[$key]['poster'] = showImage($val->poster);
            $items[$key]['profile'] = $val->profile;
            $items[$key]['alias'] = $val->alias;
            $items[$key]['industry'] = empty($val->industry) ? '--' : $val->industry;
            $items[$key]['admin'] = empty($val->user->name) ? '--' : $val->user->name;
            $items[$key]['adminMobile'] = empty($val->user->mobile) ? '--' : $val->user->mobile;
            $items[$key]['companyStatus'] = $val->certified_status == 'STATUS_CERTIFIED_PASS' ? '1' : '0';
            $items[$key]['createTime'] = date($val->created_at);
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    public function allList(): void
    {
        $model = Companies::query()
            ->select('name', 'logo')
            ->where('deleted_at',null)
            ->get();
        $items = [];
        foreach ($model as $key => $val) {
            $items[$key]['value'] = showImage($val->logo, 'company');
            $items[$key]['label'] = $val->name;
        }
        $this->setData(compact('items'));
    }

    /**
     * @return string
     */
    public function getExportData(): string
    {
        return $this->exportData;
    }

    /**
     * @param string $exportData
     */
    public function setExportData(string $exportData): void
    {
        $this->exportData = $exportData;
    }

    /**
     * @return string
     */
    public function getSortColumn(): string
    {
        return $this->sortColumn;
    }

    /**
     * @param string $sortColumn
     */
    public function setSortColumn(string $sortColumn): void
    {
        $this->sortColumn = $sortColumn;
    }

    /**
     * @return string
     */
    public function getSortVal(): string
    {
        return $this->sortVal;
    }

    /**
     * @param string $sortVal
     */
    public function setSortVal(string $sortVal): void
    {
        $this->sortVal = $sortVal;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCompanyStatus(): string
    {
        return $this->companyStatus;
    }

    /**
     * @param string $companyStatus
     */
    public function setCompanyStatus(string $companyStatus): void
    {
        $this->companyStatus = $companyStatus;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return bool
     */
    public function isFilter(): bool
    {
        return $this->Filter;
    }

    /**
     * @param bool $Filter
     */
    public function setFilter(bool $Filter): void
    {
        $this->Filter = $Filter;
    }
}
