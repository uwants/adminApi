<?php

namespace App\Logic\Company;

use App\Contracts\Company\ListMemberInterface;
use Illuminate\Support\Facades\DB;
use App\Models\CompanyUsers;

class ListMemberLogic implements ListMemberInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $companyId = 0;
    protected int $total = 0;
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '信息', 'key' => 'user', 'minWidth' => 120, 'align' => 'center','slot' => 'avatar'],
            ['title' => '名称', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '角色', 'key' => 'role', 'minWidth' => 110, 'align' => 'center','slot' => 'message'],
            ['title' => '创建时间', 'key' => 'createTime', 'minWidth' => 85, 'align' => 'center'],
        ]);
        $model = CompanyUsers::query()
            ->with('user')
            ->where('company_id', $this->getCompanyId());
        $model->orderBy('role', 'desc');
        $apply = $model->paginate($this->getPageSize());
        $item = $apply->items();
        $this->setTotal($apply->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['nickName'] = $val->user->name;
            $items[$key]['avatar'] = showImage($val->avatar);
            $items[$key]['role'] = $val->role_desc;
            $items[$key]['roleValue'] = $val->role_value;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
