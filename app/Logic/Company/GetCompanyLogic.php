<?php

namespace App\Logic\Company;

use App\Contracts\Company\GetCompanyInterface;
use App\Models\Companies;

class GetCompanyLogic implements GetCompanyInterface
{
    protected int $id = 0;
    protected array $data = [];

    public function execute(): void
    {
        $company = Companies::query()
            ->select('id', 'name', 'alias', 'logo', 'industry', 'profile', 'updated_at', 'user_id','certified_status')
            ->with('user')
            ->where('id', $this->getId())
            ->first();
        if (!empty($company->id)) {
            $returnList['id'] = $company->id;
            $returnList['name'] = $company->name;
            $returnList['alias'] = $company->alias;
            $returnList['logo'] = showImage($company->logo, 'company');
            $returnList['industry'] = $company->industry;
            $returnList['profile'] = $company->profile;
            $returnList['updateTime'] = date($company->updated_at);
            $returnList['auth'] = $company->certified_status == 'STATUS_CERTIFIED_PASS' ? '是' : '否';
            if($company->certified_status == 'STATUS_CERTIFIED_PASS'){
                $returnList['authName'] = empty($company->user->name) ? '' : $company->user->name;
                $returnList['mobile'] = empty($company->user->mpbile) ? '' : $company->user->mobile;
            }else{
                $returnList['authName'] = '';
                $returnList['mobile'] = '';
            }

        } else {
            $returnList = [];
        }

        $this->setData($returnList);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }


}
