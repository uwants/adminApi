<?php

namespace App\Logic\Company;

use App\Contracts\Company\UpdateCompanyInterface;
use App\Models\Companies;
use Illuminate\Support\Arr;

class UpdateCompanyLogic implements UpdateCompanyInterface
{
    protected array $data = [];
    protected int $id = 0;


    public function execute(): void
    {
        $data = $this->getData();
        Companies::query()
            ->where('id', '=', $this->getId())
            ->update([
                'name' => $data['companyName'],
                'alias' => empty($data['alias']) ? '' : $data['alias'],
                'logo' => checkImage($data['logo']),
                'industry' => $data['industry'],
                'profile' => $data['profile'],
                'poster' => checkImage($data['poster']),
            ]);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


}
