<?php

namespace App\Logic\Company;

use App\Contracts\Company\ListCompanyApplyInterface;
use App\Models\CompanyApplies;

class ListCompanyApplyLogic implements ListCompanyApplyInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected string $keyword = '';
    protected string $status = '';
    protected array $time = [];
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '公司信息', 'key' => 'id', 'minWidth' => 250, 'align' => 'left', 'slot' => 'company', 'ellipsis' => true],
            ['title' => '认证人', 'key' => 'title', 'minWidth' => 230, 'align' => 'left', 'slot' => 'user'],
            ['title' => '审核状态', 'key' => 'status', 'width' => 100, 'align' => 'center', 'slot' => 'status'],
            ['title' => '身份证正面', 'key' => 'image', 'minWidth' => 100, 'align' => 'center', 'slot' => 'idCard'],
            ['title' => '身份证反面', 'key' => 'image', 'minWidth' => 100, 'align' => 'center', 'slot' => 'idCardBack'],
            ['title' => '证件', 'key' => 'image', 'minWidth' => 100, 'align' => 'center', 'slot' => 'buss'],
            ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
            ['title' => '操作', 'key' => 'edit', 'width' => 120, 'align' => 'center', 'slot' => 'edit', 'fixed' => 'right'],
        ]);
        $model = CompanyApplies::withTrashed()
            ->with(['company' => function ($query) {
                return call_user_func([$query, 'withTrashed']);
            }])
            ->with(['box' => function ($query) {
                $query->with('user');
                return call_user_func([$query, 'withTrashed']);
            }]);

        //关键字查找企业
        if ($this->getKeyword()) {
            $keyword = $this->getKeyword();
            $model->whereIn('company_id', function ($query) use ($keyword) {
                $query->select('id')
                    ->from('companies')
                    ->where('name', 'like', "%$keyword%");
            });
        }
        if ($this->getStatus() == 'pass') {//已审核状态
            $model->Where('box_id', '>', 0)
                ->where('deleted_at', '=', null);
        }
        if ($this->getStatus() == 'wait') {//待审核状态
            $model->where('box_id', 0)
                ->where('deleted_at', '=', null);
        }
        if($this->getStatus() == 'noPass'){
            $model->where('deleted_at', '!=', null);
        }
        if (!empty($this->time[0])) {
            $start = date('Y-m-d H:i:S', strtotime($this->time[0]));
            $end = date('Y-m-d H:i:S', strtotime($this->time[1]));
            $model->whereBetween('created_at', [$start, $end]);
        }
        $model->orderBy('id', 'desc');
        $model->orderBy('box_id', 'desc');
        $apply = $model->paginate($this->getPageSize());
        $item = $apply->items();
        $this->setTotal($apply->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['companyName'] = $val->company->name;
            $items[$key]['companyLogo'] = showImage($val->company->logo, 'company');
            $items[$key]['industry'] = $val->company->industry;
            $items[$key]['userMobile'] = empty($val->box->user->mobile) ? '' : $val->box->user->mobile;
            $items[$key]['userName'] = empty($val->box->user->name) ? '' : $val->box->user->name;
            $items[$key]['userAvatar'] = showImage(empty($val->box->user->avatar) ? '' : $val->box->user->avatar, 'avatar');
            $items[$key]['image'] = showImage($val->image);
            $items[$key]['idCard'] = showImage($val->id_card_face);
            $items[$key]['idCardBack'] = showImage($val->id_card_overleaf);
            $items[$key]['buss'] = showImage($val->business);
            $items[$key]['reason'] = $val->reject_reason;
            if (!empty($val->deleted_at)) {
                $items[$key]['status'] = 2;
                $items[$key]['statusValue'] = 1;//审核按钮屏蔽
            } elseif (!empty($val->box_id)) {
                $items[$key]['status'] = 1;
                $items[$key]['statusValue'] = 1;
            } else {
                $items[$key]['status'] = 0;
                $items[$key]['statusValue'] = 0;
            }
            $items[$key]['createTime'] = date($val->created_at);
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));

    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getKeyword(): string
    {
        return $this->keyword;
    }

    /**
     * @param string $keyword
     */
    public function setKeyword(string $keyword): void
    {
        $this->keyword = $keyword;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getTime(): array
    {
        return $this->time;
    }

    /**
     * @param array $time
     */
    public function setTime(array $time): void
    {
        $this->time = $time;
    }


}
