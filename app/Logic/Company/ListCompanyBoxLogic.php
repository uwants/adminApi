<?php

namespace App\Logic\Company;

use App\Contracts\Company\ListCompanyBoxInterface;
use App\Models\Boxes;
use Illuminate\Support\Facades\DB;

class ListCompanyBoxLogic implements ListCompanyBoxInterface
{
    protected int $page = 0;
    protected int $pageSize = 0;
    protected int $total = 0;
    protected int $companyId = 0;
    protected array $data = [];
    protected array $columns = [];

    public function execute(): void
    {
        $this->setColumns([
            ['title' => '意见箱', 'key' => 'name', 'minWidth' => 120, 'align' => 'center'],
            ['title' => '意见箱状态', 'key' => 'status', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '收到意见', 'key' => 'number', 'minWidth' => 85, 'align' => 'center'],
            ['title' => '允许匿名', 'key' => 'allow', 'minWidth' => 85, 'align' => 'center'],
            ['title' => '仅限提一次', 'key' => 'only', 'minWidth' => 100, 'align' => 'center'],
            ['title' => '简介', 'key' => 'content', 'minWidth' => 200, 'align' => 'center', 'ellipsis' => true, 'tooltip' => true],
            ['title' => '创建时间', 'key' => 'createTime', 'width' => 100, 'align' => 'center'],
        ]);
        $box = Boxes::query()
            ->select('boxes.*', DB::raw("(select count(id) from comments where comments.box_id=boxes.id and deleted_at is null) as number"))
            ->where('company_id', $this->getCompanyId());
        $box->orderBy('id', 'desc');
        $box->orderBy('system', 'desc');
        $boxList = $box->paginate($this->getPageSize());
        $item = $boxList->items();
        $this->setTotal($boxList->total());
        $total = $this->getTotal();
        $columns = $this->getColumns();
        $pageSize = $this->getPageSize();
        $page = $this->getPage();
        $items = [];
        foreach ($item as $key => $val) {
            $items[$key]['id'] = $val->id;
            $items[$key]['name'] = $val->name;
            $items[$key]['number'] = $val->number;
            $items[$key]['status'] = $val->status == 'STATUS_ENABLE' ? '开启' : '关闭';
            $items[$key]['allow'] = $val->is_allow_anonymous == 1 ? '是' : '否';
            $items[$key]['only'] = $val->is_only_one == 1 ? '是' : '否';
            $content = json_decode($val->content);
            $items[$key]['content'] = empty($content->content) ? '' : $content->content;
            $items[$key]['createTime'] = date($val->created_at);
        }
        $this->setData(compact('total', 'pageSize', 'page', 'items', 'columns'));
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns(array $columns): void
    {
        $this->columns = $columns;
    }

}
