<?php

namespace App\Logic\Company;

use App\Contracts\Company\UpdateCompanyApplyInterface;
use App\Contracts\WechatTemplate\SendTemplateInterface;
use App\Models\CompanyApplies;
use App\Models\Boxes;
use App\Models\Companies;
use App\Models\WechatUsers;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateCompanyApplyLogic implements UpdateCompanyApplyInterface
{
    protected string $status = '';
    protected int $id = 0;
    protected string $reason = '';
    protected string $name = '';
    protected string $oldName = '';
    private SendTemplateInterface $send;

    public function __construct(SendTemplateInterface $send)
    {
        $this->send = $send;
    }

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        DB::beginTransaction();
        $apply = CompanyApplies::query()
            ->with(['company' => function ($query) {
                return call_user_func([$query, 'withTrashed']);
            }])
            ->where('id', $this->getId())
            ->first();
        if (empty($apply->id)) {
            throw new Exception('申请信息异常！');
        }

        if ($apply->company->certified_status == 'STATUS_CERTIFIED_PASS') {
            throw new Exception('该组织已经被认证！');
        }

        if ($this->getStatus() == 'DELETE') {//不通过
            CompanyApplies::query()
                ->where('id', $this->getId())
                ->update(['reject_reason' => $this->getReason(), 'deleted_at' => date('Y-m-d H:i:s', time())]);
            Companies::query()
                ->where('id', $apply->company_id)
                ->update(['certified_status' => 'STATUS_CERTIFIED_FAILED']);
            $this->sendTemplateMsg($apply->company->user_id, $apply->company->name, $apply->company->id, '认证失败！' . $this->getReason());
        } else {//
            $company = Companies::query()
                ->where('id', $apply->company_id)
                ->update([
                    'certified_status' => 'STATUS_CERTIFIED_PASS',
                    'name' => $this->getName()
                ]);
            $box = Boxes::query()
                ->where('user_id', 0)
                ->where('name', $this->getOldName())
                ->where('system', 1)
                ->first();
            if (!$company) {
                DB::rollBack();
                throw new Exception('企业信息不存在审核失败！');
            }
            if (empty($box->id)) {
                //创建意见箱
                $model = Boxes::query()->firstOrCreate([
                    'user_id' => $apply->company->user_id,
                    'name' => $this->getName(),
                    'status' => 'STATUS_ENABLE',
                    'is_only_once' => 0,
                    'is_allow_anonymous' => 1,
                    'content' => '[]',
                    'company_id' => $apply->company_id,
                    'system' => 1,
                    'open' => 1,
                    'notify' => 1,
                    'show' => 1,
                ]);
                CompanyApplies::query()
                    ->where('id', $this->getId())
                    ->update(['box_id' => $model->getOriginal('id')]);
                CompanyApplies::query()
                    ->where('id', '!=', $this->getId())
                    ->where('company_id', $apply->company_id)
                    ->update(['reject_reason' => '已经被认证', 'deleted_at' => date('Y-m-d H:i:s', time())]);
            } else {
                Boxes::query()
                    ->where('id', $box->id)
                    ->update([
                        'user_id' => $apply->company->user_id,
                        'name' => $this->getName(),
                        'company_id' => $apply->company_id,
                        'is_only_once' => 0,
                        'is_allow_anonymous' => 1,
                        'open' => 1,
                        'notify' => 1,
                        'show' => 1,
                    ]);
                CompanyApplies::query()
                    ->where('id', $this->getId())
                    ->update(['box_id' => $box->id]);
                CompanyApplies::query()
                    ->where('id', '!=', $this->getId())
                    ->where('company_id', $apply->company_id)
                    ->update(['reject_reason' => '已经被认证', 'deleted_at' => date('Y-m-d H:i:s', time())]);
            }
            $this->sendTemplateMsg($apply->company->user_id, $apply->company->name, $apply->company->id, '认证成功！');
        }
        DB::commit();
    }

    /**
     * @return string
     */
    public function getOldName(): string
    {
        return $this->oldName;
    }

    /**
     * @param string $oldName
     */
    public function setOldName(string $oldName): void
    {
        $this->oldName = $oldName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getReason(): string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function sendTemplateMsg(int $userId, $name, $companyId, $message): void
    {
        $user = WechatUsers::query()
            ->with('wapOpenId')
            ->where('user_id', $userId)
            ->first();
        if (!empty($user->wapOpenId->openid)) {
            $sendMsg = array(
                'userOpenid' => $user->wapOpenId->openid,
                'template_id' => 'qy2939c6K1i2Wf474sUQIctE8i1BeM4JpBbSP8a4bhg',
                'url' => '',
                'pagepath' => 'pages/company/company?id='.$companyId,
                'first' => '【' . $name . '】的认证审核结果已出。',
                'keyword1' => $message,
                'keyword2' => date('Y-m-d H:i:s', time()),
                'remark' => '点击查看详情'
            );
            $this->send->sendTemplate($sendMsg);
        }
    }

}
