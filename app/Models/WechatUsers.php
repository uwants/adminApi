<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class WechatUsers extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'user_id',
        'openid',
        'unionid',
        'created_at',
        'updated_at'
    ];

    public function wapOpenId()
    {
        return $this->hasOne(WechatUserSubscribes::class,'unionid','unionid');
    }
}
