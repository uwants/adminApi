<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Boxes extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'user_id',
        'name',
        'status',
        'is_only_once',
        'is_all_anonymous',
        'content',
        'company_id',
        'system',
        'invite_token',
        'invite_token_expired_at',
        'old_box_id',
        'open',
        'show',
        'notify',
    ];


    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }

}
