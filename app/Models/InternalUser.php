<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InternalUser extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'user_id',
    ];

}
