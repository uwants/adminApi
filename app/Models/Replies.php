<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Replies extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'comment_id',
        'user_id',
        'read',
        'content',
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    public function box()
    {
        return $this->hasOne(Boxes::class, 'id', 'box_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }


    public function reply()
    {
        return $this->hasMany(replies::class, 'comment_id', 'id');
    }

    public function companyUser(){
        return $this->hasMany(CompanyUsers::class, 'user_id', 'user_id');
    }



}
