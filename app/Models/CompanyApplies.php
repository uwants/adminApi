<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyApplies extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'box_id',
        'company_id',
        'image',
        'id_card_face',
        'id_card_overleaf',
        'business',
        'reject_reason',
        'deleted_at'
    ];


    public function company()
    {
        return $this->hasOne(Companies::class,'id','company_id');
    }

    public function box()
    {
        return $this->hasOne(Boxes::class,'id','box_id');
    }
}
