<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityBoxes extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'box_id',
        'activity_id',
        'created_at',
        'update_at',
    ];

    public function box(){
        return $this->hasOne(Boxes::class,'id','box_id');
    }

    public function comment(){
        return $this->hasMany(Comments::class,'box_id','box_id');
    }
}
