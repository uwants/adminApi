<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class CompanyUsers extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'company_id',
        'user_id',
        'name',
        'avatar',
        'role',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    // 超级管理员
    const ROLE_SUPPER_ADMINISTRATOR = 'ROLE_SUPPER_ADMINISTRATOR';
    // 管理员
    const ROLE_ADMINISTRATOR = 'ROLE_ADMINISTRATOR';
    // 普通成员
    const ROLE_NORMAL = 'ROLE_NORMAL';

    private static array $statusDesc = [
        self::ROLE_SUPPER_ADMINISTRATOR => '超级管理员',
        self::ROLE_ADMINISTRATOR => '管理员',
        self::ROLE_NORMAL => '普通成员',
    ];

    private static array $statusValue = [
        self::ROLE_SUPPER_ADMINISTRATOR => '2',
        self::ROLE_ADMINISTRATOR => '1',
        self::ROLE_NORMAL => '0',
    ];


    /**
     * 获取状态描述
     * @return mixed
     */
    public function getRoleDescAttribute()
    {
        return Arr::get(self::$statusDesc, $this->getOriginal('role'), '');
    }

    public function getRoleValueAttribute()
    {
        return Arr::get(self::$statusValue, $this->getOriginal('role'), '');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
