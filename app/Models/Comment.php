<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Comment extends Model
{
    use HasFactory,SoftDeletes;

    public $fillable = [
        'id',
        'box_id',
        'user_id',
        'content',
        'adopt',
        'anonymous',
        'read',
        'visit',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    function box(){
        return $this->belongsTo(Boxes::class);
    }
    function user(){
        return $this->belongsTo(User::class);
    }
    function reply(){
        return $this->hasOne(Reply::class);
    }
}
