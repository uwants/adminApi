<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'mobile',
        'name',
        'avatar',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function userUnionid()
    {
        return $this->hasOne(WechatUsers::class, 'user_id', 'id');
    }


    public function company()
    {
        return $this->hasMany(Companies::class, 'user_id', 'id');
    }

    public function reply()
    {
        return $this->hasMany(Reply::class);
    }

    public function companyUser()
    {
        return $this->hasMany(CompanyUsers::class, 'user_id', 'id');
    }

}
