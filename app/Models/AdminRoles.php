<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRoles extends Model
{
    use HasFactory, SoftDeletes;
    public $fillable = [
        'id',
        'name',
        'remark',
        'created_at',
        'created_admin_id'
    ];

    public function adminUser(){
        return $this->hasOne(AdminUser::Class,'id','created_admin_id');
    }
}
