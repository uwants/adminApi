<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AppletKeywords extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'value',
        'created_at',
        'updated_at'
    ];
}
