<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comments extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'box_id',
        'user_id',
        'content',
        'adopt',
        'anonymous',
        'read',
        'visit',
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    public function box(){
        return $this->hasOne(Boxes::class,'id','box_id');
    }

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function reply(){
        return $this->hasMany(Replies::class,'comment_id','id');
    }
}
