<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlatformBanner extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'url',
        'image',
        'platform_code',
        'suspend',
        'sort',
        'created_at',
    ];

}
