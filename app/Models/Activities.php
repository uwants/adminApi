<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activities extends Model
{
    use HasFactory,SoftDeletes;

    public $fillable = [
        'id',
        'name',
        'start',
        'end',
        'suspend',
        'created_at',
        'update_at',
    ];
}
