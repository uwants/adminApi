<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class WechatTemplates extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'template_id',
        'template_title',
        'content',
        'primary_industry',
        'deputy_industry',
        'example'
    ];
}

