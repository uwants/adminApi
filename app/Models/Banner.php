<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use HasFactory,SoftDeletes;

    public $fillable = [
        'content',
        'image',
        'type',
        'suspend',
        'sort',
        'created_at',
    ];

    /**
     * 小程序链接
     */
    const TYPE_MINI = 'MINI';
    /**
     * 超链接
     */
    const TYPE_WWW = 'WWW';
    /**
     * 公众号
     */
    const TYPE_PUBLIC = 'PUBLIC';
}
