<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUser extends Authenticatable
{
    use HasFactory,SoftDeletes;

    public $fillable = [
        'id',
        'admin_name',
        'password',
        'avatar',
        'nick_name',
        'role_id',

    ];


    public function role(){
        return $this->hasOne(AdminRoles::Class,'id','role_id');
    }




}
