<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companies extends Model
{
    use HasFactory, SoftDeletes;

    // 认证通过
    const STATUS_CERTIFIED_PASS = 'STATUS_CERTIFIED_PASS';
    // 未认证
    const STATUS_CERTIFIED_DEFAULT = 'STATUS_CERTIFIED_PRE_COMMIT';
    // 审核中
    const STATUS_CERTIFIED_AUDIT = 'STATUS_CERTIFIED_AUDIT';
    // 不通过
    const STATUS_CERTIFIED_FAILED = 'STATUS_CERTIFIED_FAILED';

    public $fillable = [
        'id',
        'name',
        'alias',
        'logo',
        'user_id',
        'industry',
        'profile',
        'poster',
        'certified_status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
