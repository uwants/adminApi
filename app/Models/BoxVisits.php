<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class BoxVisits extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'box_id',
        'type',
        'created_at',
        'updated_at',
        'deleted_at'
    ];



}
