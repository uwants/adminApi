<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminRolePermission extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = [
        'id',
        'role_id',
        'menu_id',
        'level',
        'parent_id'
    ];
}
