<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminMenus extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'admin_menus';

    public $fillable = [
        'id',
        'name',
        'title',
        'path',
        'icon',
        'power_name',
        'parent_id',
        'level',
        'is_ban',
        'sort',
        'created_at',
        'updated_at',
    ];
}
