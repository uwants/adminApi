<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Index\ListIndexInterface;

class IndexController extends Controller
{
    public function index(ListIndexInterface $list)
    {
        $list->execute();
        $this->response->setData($list->getData());
        $this->response->execute();
    }

    public function box(ListIndexInterface $box){
        $box->thirtyBox();
        $this->response->setData($box->getData());
        $this->response->execute();
    }

    public function opinion(ListIndexInterface $opinion){
        $opinion->thirtyOp();
        $this->response->setData($opinion->getData());
        $this->response->execute();
    }
}
