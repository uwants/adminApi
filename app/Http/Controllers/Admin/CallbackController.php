<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\CallBack\CallbackInterface;

class CallbackController extends Controller
{
    public function index(CallbackInterface $back)
    {
        $postStr = file_get_contents("php://input", "r");
        $notify = (array)simplexml_load_string($postStr, "SimpleXMLElement", LIBXML_NOCDATA);
        $back->setMessage($notify);
        $back->execute();
        echo $back->getData();
        exit;
    }
}
