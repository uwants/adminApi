<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\WechatTemplate\GetMenuInterface;
use App\Contracts\WechatTemplate\UpdateMenuInterface;

class AppletsController extends Controller
{
    public function index(GetMenuInterface $index)
    {
        $index->execute();
        $this->response->setData($index->getData());
        $this->response->execute();
    }

    public function update(UpdateMenuInterface $update)
    {
        $update->setMenuMessage($this->request->input('menuMessage'));
        $update->execute();
        $data = $update->getData();
        $this->response->setData($data);
        $this->response->execute();
    }
}
