<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\WechatTemplate\ListTemplateInterface;
use App\Contracts\WechatTemplate\GetTemplateInterface;
use App\Contracts\WechatTemplate\SendTemplateInterface;
use App\Contracts\WechatTemplate\PullTemplateInterface;

class TemplateController extends Controller
{
    public function index(ListTemplateInterface $list)
    {
        if ($this->request->input('keyword')) {
            $list->setKeyword($this->request->input('keyword'));
        }
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        $list->execute();
        $data = $list->getData();
        $this->response->setData($data);
        $this->response->execute();

    }

    public function show()
    {

    }

    public function pull(PullTemplateInterface $pull)
    {
        $pull->execute();
        $this->response->execute();
    }

    public function send(SendTemplateInterface $send)
    {
        $send->setMessage($this->request->input('message'));
        $send->setTemplateId($this->request->input('templateId'));
        if ($this->request->input('jumpUrl')) {
            $send->setJumpUrl($this->request->input('jumpUrl'));
        }
        if ($this->request->input('jumpType')) {
            $send->setJumpType($this->request->input('jumpType'));
        }
        $send->setIds($this->request->input('ids'));
        $send->execute();
        $data = $send->getData();
        $this->response->setData($data);
        $this->response->execute();

    }

    public function back()
    {

    }

    public function message(GetTemplateInterface $message)
    {
        $message->setId($this->request->input('id'));
        $message->execute();
        $data = $message->getData();
        $this->response->setData($data);
        $this->response->execute();
    }


}
