<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Boxes\ListBoxInterface;
use App\Contracts\Boxes\GetBoxInterface;
use App\Contracts\Boxes\ListBoxOpInterface;

class BoxesController extends Controller
{
    public function index(ListBoxInterface $list)
    {
        if ($this->request->input('keyword')) {
            $list->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $list->setTime($this->request->input('time'));
        }
        if ($this->request->input('exportData')) {
            $list->setExportData($this->request->input('exportData'));
        }
        if ($this->request->input('sortColumn')) {
            $list->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $list->setSortVal($this->request->input('sortVal'));
        }
        $list->setFilter($this->request->input('filter'));
        $list->setStatus($this->request->input('status'));
        $list->setBoxStatus($this->request->input('boxStatus'));
        $list->setBoxType($this->request->input('boxType'));
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        $list->execute();
        $this->response->setData($list->getData());
        $this->response->execute();
    }

    public function explode(ListBoxInterface $explode)
    {
        if ($this->request->input('keyword')) {
            $explode->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $explode->setTime($this->request->input('time'));
        }
        $explode->setStatus($this->request->input('status'));
        $explode->setBoxStatus($this->request->input('boxStatus'));
        $explode->setBoxType($this->request->input('boxType'));
        $explode->explode();
        $this->response->setData($explode->getData());
        $this->response->execute();
    }

    public function show(GetBoxInterface $show)
    {
        $show->setId($this->request->input('id'));
        $show->execute();
        $this->response->setData($show->getData());
        $this->response->execute();
    }

    public function opinion(ListBoxOpInterface $opinion)
    {
        if ($this->request->input('keyword')) {
            $opinion->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $opinion->setTime($this->request->input('time'));
        }
        if ($this->request->input('exportData')) {
            $opinion->setExportData($this->request->input('exportData'));
        }
        if ($this->request->input('sortColumn')) {
            $opinion->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $opinion->setSortVal($this->request->input('sortVal'));
        }
        $opinion->setKeywordState($this->request->input('keywordState'));
        $opinion->setBoxId($this->request->input('boxId'));
        $opinion->setPage($this->getPage());
        $opinion->setPageSize($this->getPageSize());
        $opinion->execute();
        $this->response->setData($opinion->getData());
        $this->response->execute();
    }
}
