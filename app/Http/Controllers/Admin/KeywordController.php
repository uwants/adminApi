<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Keyword\GetKeywordInterface;
use App\Contracts\Keyword\CreateKeywordInterface;
use App\Contracts\Keyword\DelAllKeywordInterface;
use App\Contracts\Keyword\DeleteKeywordInterface;

class KeywordController extends Controller
{
    public function index(GetKeywordInterface $index)
    {
        if ($this->request->input('keyword')) {
            $index->setKeyword($this->request->input('keyword'));
        }
        $index->execute();
        $this->response->setData($index->getData());
        $this->response->execute();
    }

    public function create(CreateKeywordInterface $create)
    {
        $create->setValue($this->request->input('value'));
        $create->execute();
        $this->response->setData($create->getData());
        $this->response->execute();

    }

    public function delete(DeleteKeywordInterface $delete)
    {
        $delete->setId($this->request->input('id'));
        $delete->execute();
        $id = $delete->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }

    public function delAll(DelAllKeywordInterface $all)
    {
        $all->setIds($this->request->input('id'));
        $all->execute();
        $this->response->setData($all->getIds());
        $this->response->execute();
    }
}
