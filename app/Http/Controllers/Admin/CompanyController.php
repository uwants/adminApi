<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Company\ListCompanyApplyInterface;
use App\Contracts\Company\ListCompanyInterface;
use App\Contracts\Company\UpdateCompanyApplyInterface;
use App\Contracts\Company\UpdateCompanyInterface;
use App\Contracts\Company\GetCompanyInterface;
use App\Contracts\Company\ListCompanyBoxInterface;
use App\Contracts\Company\ListComOpInterface;
use App\Contracts\Company\ListMemberInterface;
use App\Contracts\Company\ListHotCompanyInterface;

class CompanyController extends Controller
{
    /**
     * @param ListCompanyApplyInterface $applyList
     * 企业申请列表
     */
    public function apply(ListCompanyApplyInterface $applyList)
    {
        if ($this->request->input('keyword')) {
            $applyList->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $applyList->setTime($this->request->input('time'));
        }
        $applyList->setStatus($this->request->input('status'));
        $applyList->setPage($this->getPage());
        $applyList->setPageSize($this->getPageSize());
        $applyList->execute();
        $data = $applyList->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    /**
     * @param UpdateCompanyApplyInterface $check
     * 企业审核
     */
    public function check(UpdateCompanyApplyInterface $check)
    {
        $check->setId($this->request->input('id'));
        $check->setStatus($this->request->input('status'));
        if ($this->request->input('reason')) {
            $check->setReason($this->request->input('reason'));
        }
        $check->setName($this->request->input('name'));
        $check->setOldName($this->request->input('oldName'));
        $check->execute();
        $this->response->execute();
    }

    /**
     * @param ListCompanyInterface $index
     * 企业列表
     */
    public function index(ListCompanyInterface $index)
    {
        $index->setPage($this->getPage());
        $index->setPageSize($this->getPageSize());
        if ($this->request->input('keyword')) {
            $index->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $index->setTime($this->request->input('time'));
        }
        if ($this->request->input('exportData')) {
            $index->setExportData($this->request->input('exportData'));
        }
        if ($this->request->input('sortColumn')) {
            $index->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $index->setSortVal($this->request->input('sortVal'));
        }
        $index->setFilter($this->request->input('filter'));
        $index->setCompanyStatus($this->request->input('companyStatus'));
        $index->setStatus($this->request->input('status'));
        $index->execute();
        $data = $index->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    /**
     * @param ListCompanyInterface $list
     * 企业下拉列表
     */
    public function select(ListCompanyInterface $list)
    {
        $list->allList();
        $data = $list->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    /**
     * @param UpdateCompanyInterface $update
     * 修改企业
     */
    public function update(UpdateCompanyInterface $update)
    {
        $update->setId($this->request->input('id'));
        $update->setData($this->request->input('data'));
        $update->execute();
        $this->response->setData($update->getData());
        $this->response->execute();
    }

    /**
     * @param GetCompanyInterface $message
     * 企业信息
     */
    public function message(GetCompanyInterface $message)
    {
        $message->setId($this->request->input('id'));
        $message->execute();
        $this->response->setData($message->getData());
        $this->response->execute();
    }

    /**
     * @param ListCompanyBoxInterface $box
     * 企业意见箱
     */
    public function box(ListCompanyBoxInterface $box)
    {
        $box->setCompanyId($this->request->input('companyId'));
        $box->setPage($this->getPage());
        $box->setPageSize($this->getPageSize());
        $box->execute();
        $this->response->setData($box->getData());
        $this->response->execute();
    }

    /**
     * @param ListComOpInterface $opinion
     * 企业意见
     */
    public function opinion(ListComOpInterface $opinion)
    {
        $opinion->setCompanyId($this->request->input('companyId'));
        $opinion->setPage($this->getPage());
        $opinion->setPageSize($this->getPageSize());
        $opinion->execute();
        $this->response->setData($opinion->getData());
        $this->response->execute();
    }

    /**
     * @param ListMemberInterface $member
     * 企业成员
     */
    public function member(ListMemberInterface $member)
    {
        $member->setCompanyId($this->request->input('companyId'));
        $member->setPage($this->getPage());
        $member->setPageSize($this->getPageSize());
        $member->execute();
        $this->response->setData($member->getData());
        $this->response->execute();
    }

    /**
     * @param ListHotCompanyInterface $hot
     * 热门企业列表
     */
    public function hot(ListHotCompanyInterface $hot)
    {
        $hot->setPage($this->getPage());
        $hot->setPageSize($this->getPageSize());
        if ($this->request->input('keyword')) {
            $hot->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('sortColumn')) {
            $hot->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $hot->setSortVal($this->request->input('sortVal'));
        }
        $hot->execute();
        $data = $hot->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function updateHot(ListHotCompanyInterface $update)
    {
        $update->setId($this->request->input('id'));
        $update->setName($this->request->input('name'));
        $update->setLogo($this->request->input('logo'));
        $update->setSort($this->request->input('sort'));
        $update->setStatus($this->request->input('status'));
        $update->update();
        $this->response->execute();
    }

    public function deleteHot(ListHotCompanyInterface $delete)
    {
        $delete->setId($this->request->input('id'));
        $delete->delete();
        $this->response->execute();
    }

    public function switchHot(ListHotCompanyInterface $switch)
    {
        $switch->setId($this->request->input('id'));
        $switch->setStatus($this->request->input('status'));
        $switch->switch();
        $this->response->execute();

    }

}
