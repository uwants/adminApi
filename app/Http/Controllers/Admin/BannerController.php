<?php


namespace App\Http\Controllers\Admin;

use App\Contracts\Banner\DeleteBannerInterface;
use App\Contracts\Banner\GetBannerInterface;
use App\Contracts\Banner\SwitchBannerInterface;
use App\Contracts\Banner\UpdateBannerInterface;
use App\Http\Controllers\Controller;
use App\Contracts\Banner\ListBannerInterface;
use App\Contracts\Banner\CreateBannerInterface;

class BannerController extends Controller
{
    public function index(ListBannerInterface $listBanner)
    {
        $listBanner->setPage($this->getPage());
        $listBanner->setPageSize($this->getPageSize());
        $listBanner->execute();
        $this->response->setData($listBanner->getData());
        $this->response->execute();
    }

    public function create(CreateBannerInterface $createBanner)
    {
        $this->validate($this->request, [
            'image' => ['required', 'url'],
        ]);
        if ($this->request->input('content')) {
            $createBanner->setContent($this->request->input('content'));
        }
        $createBanner->setImage($this->request->input('image'));
        $createBanner->setSort($this->request->input('sort'));
        $createBanner->setSuspend($this->request->input('suspend'));
        $createBanner->setType($this->request->input('type'));
        $createBanner->execute();
        $id = $createBanner->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }

    public function update(UpdateBannerInterface $updateBanner)
    {
        $this->validate($this->request, [
            'image' => ['required', 'url'],
        ]);
        if ($this->request->input('content')) {
            $updateBanner->setContent($this->request->input('content'));
        }
        $updateBanner->setId($this->request->input('id'));
        $updateBanner->setImage($this->request->input('image'));
        $updateBanner->setSuspend($this->request->input('suspend'));
        $updateBanner->setSort($this->request->input('sort'));
        $updateBanner->setType($this->request->input('type'));
        $updateBanner->execute();
        $this->response->execute();
    }

    public function delete(DeleteBannerInterface $deleteBanner)
    {
        $deleteBanner->setId($this->request->input('id'));
        $deleteBanner->execute();
        $this->response->execute();
    }

    public function switchStatus(SwitchBannerInterface $switchBanner)
    {
        $switchBanner->setId($this->request->input('id'));
        $switchBanner->setStatus($this->request->input('status'));
        $switchBanner->execute();
        $this->response->execute();
    }

    public function get(GetBannerInterface $getBanner)
    {
        $getBanner->setId($this->request->input('id'));
        $getBanner->execute();
        $this->response->setData($getBanner->getData());
        $this->response->execute();
    }
}

