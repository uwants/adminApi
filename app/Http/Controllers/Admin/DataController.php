<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Statistics\DataUsersInterface;
use App\Contracts\Statistics\EveryDataInterface;
use App\Contracts\Statistics\ActivityDataInterface;
use App\Contracts\Statistics\ActivityOpInterface;

class DataController extends Controller
{
    public function userDataList(DataUsersInterface $list)
    {
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        if ($this->request->input('timeState')) {
            $list->setTimeState($this->request->input('timeState'));
        }
        if ($this->request->input('exportData')) {
            $list->setExportData($this->request->input('exportData'));
        }
        if ($this->request->input('time')) {
            $list->setTime($this->request->input('time'));
        }
        if ($this->request->input('sortColumn')) {
            $list->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $list->setSortVal($this->request->input('sortVal'));
        }
        $list->execute();
        $data = $list->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function userDataPic(DataUsersInterface $list)
    {
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        if ($this->request->input('timeState')) {
            $list->setTimeState($this->request->input('timeState'));
        }
        if ($this->request->input('time')) {
            $list->setTime($this->request->input('time'));
        }
        $list->picExecute();
        $data = $list->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function every(EveryDataInterface $every)
    {
        $every->setFilter($this->request->input('filter'));
        if ($this->request->input('timeState')) {
            $every->setTimeState($this->request->input('timeState'));
        }
        if ($this->request->input('time')) {
            $every->setTime($this->request->input('time'));
        }
        if ($this->request->input('sortColumn')) {
            $every->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $every->setSortVal($this->request->input('sortVal'));
        }
        $every->execute();
        $data = $every->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function activity(ActivityDataInterface $activity)
    {
        $activity->setPage($this->getPage());
        $activity->setPageSize($this->getPageSize());
        $activity->setFilter($this->request->input('filter'));
        if ($this->request->input('activityId')) {
            $activity->setActivityId($this->request->input('activityId'));
        }
        if ($this->request->input('timeState')) {
            $activity->setTimeState($this->request->input('timeState'));
        }
        if ($this->request->input('time')) {
            $activity->setTime($this->request->input('time'));
        }
        if ($this->request->input('sortColumn')) {
            $activity->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $activity->setSortVal($this->request->input('sortVal'));
        }
        if ($this->request->input('activityType')) {
            $activity->setActivityType($this->request->input('activityType'));
        }
        if ($this->request->input('numberSelect')) {
            $activity->setNumberSelect($this->request->input('numberSelect'));
        }
        if ($this->request->input('number')) {
            $activity->setNumber($this->request->input('number'));
        }
        $activity->execute();
        $data = $activity->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function activityBox(ActivityDataInterface $activity)
    {
        $activity->setPage($this->getPage());
        $activity->setPageSize($this->getPageSize());
        $activity->setFilter($this->request->input('filter'));
        if ($this->request->input('activityId')) {
            $activity->setActivityId($this->request->input('activityId'));
        }
        if ($this->request->input('timeState')) {
            $activity->setTimeState($this->request->input('timeState'));
        }
        if ($this->request->input('time')) {
            $activity->setTime($this->request->input('time'));
        }
        if ($this->request->input('sortColumn')) {
            $activity->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $activity->setSortVal($this->request->input('sortVal'));
        }
        if ($this->request->input('activityType')) {
            $activity->setActivityType($this->request->input('activityType'));
        }
        if ($this->request->input('numberSelect')) {
            $activity->setNumberSelect($this->request->input('numberSelect'));
        }
        if ($this->request->input('number')) {
            $activity->setNumber($this->request->input('number'));
        }
        if ($this->request->input('exportData')) {
            $activity->setExportData($this->request->input('exportData'));
        }
        $activity->executeBox();
        $data = $activity->getDataBox();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function activityUser(ActivityDataInterface $activity)
    {
        $activity->setPage($this->getPage());
        $activity->setPageSize($this->getPageSize());
        $activity->setFilter($this->request->input('filter'));
        if ($this->request->input('activityId')) {
            $activity->setActivityId($this->request->input('activityId'));
        }
        if ($this->request->input('timeState')) {
            $activity->setTimeState($this->request->input('timeState'));
        }
        if ($this->request->input('time')) {
            $activity->setTime($this->request->input('time'));
        }
        if ($this->request->input('sortColumn')) {
            $activity->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $activity->setSortVal($this->request->input('sortVal'));
        }
        if ($this->request->input('activityType')) {
            $activity->setActivityType($this->request->input('activityType'));
        }
        if ($this->request->input('numberSelect')) {
            $activity->setNumberSelect($this->request->input('numberSelect'));
        }
        if ($this->request->input('number')) {
            $activity->setNumber($this->request->input('number'));
        }
        if ($this->request->input('exportData')) {
            $activity->setExportData($this->request->input('exportData'));
        }
        $activity->executeUser();
        $data = $activity->getDataUser();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function getOpinion(ActivityOpInterface $op){
        $op->setPage($this->getPage());
        $op->setPageSize($this->getPageSize());
        $op->setBoxId($this->request->input('boxId'));
        $op->setFilter($this->request->input('filter'));
        if ($this->request->input('exportData')) {
            $op->setExportData($this->request->input('exportData'));
        }
        $op->execute();
        $data = $op->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

}
