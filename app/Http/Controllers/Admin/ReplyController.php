<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Reply\GetReplyInterface;
use App\Contracts\Reply\ListReplyInterface;

class ReplyController extends Controller
{
    public function index(ListReplyInterface $list){

        if ($this->request->input('keyword')) {
            $list->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $list->setTime($this->request->input('time'));
        }
        if($this->request->input('sortType')){
            $list->setSortType($this->request->input('sortType'));
        }
        $list->setFilter($this->request->input('filter'));
        $list->setStatus($this->request->input('status'));
        $list->setOpinionStatus($this->request->input('opinionStatus'));
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        $list->execute();
        $this->response->setData($list->getData());
        $this->response->execute();
    }
    public function explode(ListReplyInterface $explode){
        if ($this->request->input('keyword')) {
            $explode->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $explode->setTime($this->request->input('time'));
        }
        $explode->setStatus($this->request->input('status'));
        $explode->setOpinionStatus($this->request->input('opinionStatus'));
        $explode->explode();
        $this->response->setData($explode->getData());
        $this->response->execute();

    }
    public function show(GetReplyInterface $show){
        $show->setId($this->request->input('id'));
        $show->execute();
        $this->response->setData($show->getData());
        $this->response->execute();
    }
}
