<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Operate\ActivityInterface;
use App\Contracts\Operate\CreateActivityInterface;
use App\Http\Controllers\Controller;

class OperateController extends Controller
{
    public function index(ActivityInterface $activity)
    {
        $activity->setPage($this->getPage());
        $activity->setPageSize($this->getPageSize());
        if ($this->request->input('keyword')) {
            $activity->setKeyword($this->request->input('keyword'));
        }
        $activity->execute();
        $data = $activity->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function update(CreateActivityInterface $update)
    {
        $update->setActivity($this->request->input('activity'));
        $update->execute();
        $this->response->setData($update->getData());
        $this->response->execute();
    }
}
