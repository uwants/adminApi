<?php


namespace App\Http\Controllers\Admin;
use App\Contracts\InternalUser\CreateInternalUserInterface;
use App\Contracts\InternalUser\DeleteInternalUserInterface;
use App\Http\Controllers\Controller;
use App\Logic\InternalUser\ListInternalUserLogic;

class InternalUserController extends Controller
{
    public function index(ListInternalUserLogic $list){
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        $list->execute();
        $data = $list->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function store(CreateInternalUserInterface $create){
        $create->setUserId($this->request->input('user_id'));
        $create->execute();
        $id = $create->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }
    public function destory(DeleteInternalUserInterface $delete){
        $delete->setId($this->request->input('id'));
        $delete->execute();
        $this->response->execute();
    }
}
