<?php


namespace App\Http\Controllers\Admin;


use App\Contracts\Assembly\DragInterface;
use App\Contracts\Assembly\OrganizationInterface;
use App\Contracts\Assembly\TreeInterface;
use App\Http\Controllers\Controller;

class AssemblyController extends Controller
{
    public function drag(DragInterface $drag)
    {
        $drag->execute();
        $this->response->setData($drag->getData());
        $this->response->execute();
    }

    public function tree(TreeInterface $tree)
    {
        $tree->execute();
        $this->response->setData($tree->getData());
        $this->response->execute();
    }

    public function organization(OrganizationInterface $organization)
    {
        $organization->execute();
        $this->response->setData($organization->getData());
        $this->response->execute();
    }
}
