<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\PlatformBanner\CreatePlatformBannerInterface;
use App\Contracts\PlatformBanner\DeletePlatformBannerInterface;
use App\Contracts\PlatformBanner\GetPlatformBannerInterface;
use App\Contracts\PlatformBanner\ListPlatformBannerInterface;
use App\Contracts\PlatformBanner\SwitchPlatformBannerInterface;
use App\Contracts\PlatformBanner\UpdatePlatformBannerInterface;
use App\Http\Controllers\Controller;

class PlatformBannerController extends Controller
{
    public function index(ListPlatformBannerInterface $listBanner)
    {
        if ($this->request->input('platformCode')){
            $listBanner->setPlatformCode($this->request->input('platformCode'));
        }
        $listBanner->setPage($this->getPage());
        $listBanner->setPageSize($this->getPageSize());
        $listBanner->execute();
        $this->response->setData($listBanner->getData());
        $this->response->execute();
    }

    public function create(CreatePlatformBannerInterface $createBanner)
    {
        $this->validate($this->request, [
            'image' => ['required', 'url'],
        ]);
        if ($this->request->input('url')) {
            $createBanner->setUrl($this->request->input('url'));
        }
        $createBanner->setImage($this->request->input('image'));
        $createBanner->setSort($this->request->input('sort'));
        $createBanner->setSuspend($this->request->input('suspend'));
        $createBanner->setPlatformCode($this->request->input('platformCode'));
        $createBanner->execute();
        $id = $createBanner->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }

    public function update(UpdatePlatformBannerInterface $updateBanner)
    {
        $this->validate($this->request, [
            'image' => ['required', 'url'],
        ]);
        if ($this->request->input('url')) {
            $updateBanner->setUrl($this->request->input('url'));
        }
        $updateBanner->setId($this->request->input('id'));
        $updateBanner->setImage($this->request->input('image'));
        $updateBanner->setSuspend($this->request->input('suspend'));
        $updateBanner->setSort($this->request->input('sort'));
        $updateBanner->setPlatformCode($this->request->input('platformCode'));
        $updateBanner->execute();
        $this->response->execute();
    }

    public function delete(DeletePlatformBannerInterface $deleteBanner)
    {
        $deleteBanner->setId($this->request->input('id'));
        $deleteBanner->execute();
        $this->response->execute();
    }

    public function switchStatus(SwitchPlatformBannerInterface $switchBanner)
    {
        $switchBanner->setId($this->request->input('id'));
        $switchBanner->setStatus($this->request->input('status'));
        $switchBanner->execute();
        $this->response->execute();
    }

    public function get(GetPlatformBannerInterface $getBanner)
    {
        $getBanner->setId($this->request->input('id'));
        $getBanner->execute();
        $this->response->setData($getBanner->getData());
        $this->response->execute();
    }
}
