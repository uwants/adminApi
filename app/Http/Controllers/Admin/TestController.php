<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use EasyWeChat\Factory;
use App\Contracts\WechatTemplate\SendTemplateInterface;
use Illuminate\Http\Request;
use App\Logic\Test\DingCallbackCrypto;

class TestController extends Controller
{
    public function index()
    {
        $app = Factory::payment(config('wechat.payment.default'));
        $result = $app->refund->byTransactionId([]);
        return config('wechat.payment.default');
    }

    public function token(SendTemplateInterface $send)
    {
        $data = $send->getSAppAccessToken();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function register(Request $request)
    {
        header("Content-Type: application/json");
        $data = $request->json()->all();
        $message = $request->all();
        $data['encrypt'] = '1a3NBxmCFwkCJvfoQ7WhJHB+iX3qHPsc9JbaDznE1i03peOk1LaOQoRz3+nlyGNhwmwJ3vDMG+OzrHMeiZI7gTRWVdUBmfxjZ8Ej23JVYa9VrYeJ5as7XM/ZpulX8NEQis44w53h1qAgnC3PRzM7Zc/D6Ibr0rgUathB6zRHP8PYrfgnNOS9PhSBdHlegK+AGGanfwjXuQ9+0pZcy0w9lQ==';
        $check = new DingCallbackCrypto('cjyjx2', '3xhybjmccz3xysursyo6sajz65dg74c1pvrzy1ducx3', 'suitevdowp2ajomyamq7y');
        //$text = $check->getDecryptMsg($message['msg_signature'], $message['timestamp'], $message['nonce'], $data['encrypt']);
        $res = $check->getEncryptedMap("success");
        return $res;
        die();
    }
}
