<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Users\ListUsersInterface;
use App\Contracts\Users\ListUserOrderInterface;
use App\Contracts\Users\GetUsersInterface;
use App\Contracts\Users\ListUserOpInterface;
use App\Contracts\Users\ListUserBoxInterface;

class UsersController extends Controller
{
    public function index(ListUsersInterface $list)
    {
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        $list->setFilter($this->request->input('filter'));
        if ($this->request->input('keyword')) {
            $list->setSearchType($this->request->input('searchType'));
            $list->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('exportData')) {
            $list->setExportData($this->request->input('exportData'));
        }
        if ($this->request->input('time')) {
            $list->setTime($this->request->input('time'));
        }
        if ($this->request->input('sortColumn')) {
            $list->setSortColumn($this->request->input('sortColumn'));
        }
        if ($this->request->input('sortVal')) {
            $list->setSortVal($this->request->input('sortVal'));
        }
        $list->execute();
        $this->response->setData($list->getData());
        $this->response->execute();
    }

    public function show(GetUsersInterface $get)
    {
        $get->setId($this->request->input('id'));
        $get->execute();
        $data = $get->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function order(ListUserOrderInterface $order)
    {
        $order->setId($this->request->input('id'));
        $order->setStatus($this->request->input('status'));
        $order->execute();
        $data = $order->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function box(ListUserBoxInterface $box)
    {
        $box->setUserId($this->request->input('userId'));
        $box->setPage($this->getPage());
        $box->setPageSize($this->getPageSize());
        $box->execute();
        $this->response->setData($box->getData());
        $this->response->execute();
    }

    public function opinion(ListUserOpInterface $opinion)
    {
        $opinion->setUserId($this->request->input('userId'));
        $opinion->setPage($this->getPage());
        $opinion->setPageSize($this->getPageSize());
        $opinion->execute();
        $this->response->setData($opinion->getData());
        $this->response->execute();
    }

}
