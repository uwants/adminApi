<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class ImagesController extends Controller
{
    /**
     * @param Request $request
     * @throws Exception
     */
    public function create(Request $request)
    {
        $config_url = env('QN_CDN_URL');
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        $filePath = 'image/'.gmdate("Y")."/".gmdate("m")."/".gmdate("d")."/".uniqid(str::random(8)).'.'.$extension;
        $mess=Storage::disk('qiniu')->put($filePath,File::get($file));
        if($mess){
            $url = $config_url.'/'.$filePath;
            $value = $filePath;
            $this->response->setData(compact('url','value'));
            $this->response->execute();
        }else{
            throw new Exception($mess);
        }

    }
}
