<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\AdminUser\GetAdminUserInterface;
use App\Contracts\AdminUser\CreateAdminUserInterface;
use App\Contracts\AdminUser\GetMessageInterface;
use App\Contracts\AdminUser\UpdateAdminInterface;
use App\Contracts\AdminUser\ListAdminInterface;
use App\Contracts\AdminUser\DeleteAdminInterface;
use App\Contracts\AdminUser\CreateMenuInterface;
use App\Contracts\AdminUser\DeleteMenuInterface;
use App\Contracts\AdminUser\ListMenuInterface;
use App\Contracts\AdminUser\ListRoleInterface;
use App\Contracts\AdminUser\CreateRoleInterface;

class AdminUsersController extends Controller
{
    public function login(GetAdminUserInterface $adminLogin)
    {
        $adminLogin->setAdminName($this->request->input('adminName'));
        $adminLogin->setPassword($this->request->input('password'));
        $adminLogin->execute();
        $data = $adminLogin->getData();
        $this->response->setData($data);
        $this->response->execute();

    }

    public function create(CreateAdminUserInterface $create)
    {
        $create->setPassword($this->request->input('password'));
        $create->setAdminName($this->request->input('adminName'));
        $create->setRoleId($this->request->input('roleId'));
        $create->setId($this->request->input('id'));
        $create->execute();
        $id = $create->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }
    public function menu(GetMessageInterface $menu){
        $menu->setRoleId($this->request->input('roleId'));
        $menu->getMenuList();
        $this->response->setData($menu->getData());
        $this->response->execute();
    }
    public function show(GetMessageInterface $msg)
    {
        $msg->setId($this->request->input('id'));
        $msg->execute();
        $this->response->setData($msg->getData());
        $this->response->execute();
    }

    public function update(UpdateAdminInterface $update)
    {
        $update->setData($this->request->input('data'));
        $update->execute();
        $id = $update->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }

    public function index(ListAdminInterface $index)
    {
        if ($this->request->input('keyword')) {
            $index->setKeyword($this->request->input('keyword'));
        }
        $index->setPage($this->getPage());
        $index->setPageSize($this->getPageSize());
        $index->execute();
        $this->response->setData($index->getData());
        $this->response->execute();
    }

    public function delete(DeleteAdminInterface $delete)
    {
        $delete->setId($this->request->input('id'));
        $delete->execute();
        $this->response->execute();
    }


    public function createMenu(CreateMenuInterface $menu)
    {
        $menu->setMenu($this->request->input('menu'));
        $menu->execute();
        $this->response->setData($menu->getData());
        $this->response->execute();
    }

    public function listMenu(ListMenuInterface $list)
    {
        $list->execute();
        $this->response->setData($list->getData());
        $this->response->execute();
    }

    public function deleteMenu(DeleteMenuInterface $delete)
    {
        $delete->setId($this->request->input('id'));
        $delete->execute();
        $this->response->execute();
    }

    public function role(ListRoleInterface $role)
    {
        if ($this->request->input('state')) {
            $role->setKeyword($this->request->input('state'));
        }
        if ($this->request->input('keyword')) {
            $role->setKeyword($this->request->input('keyword'));
        }
        $role->setPage($this->getPage());
        $role->setPageSize($this->getPageSize());
        $role->execute();
        $this->response->setData($role->getData());
        $this->response->execute();
    }

    public function roleCreate(CreateRoleInterface $roleCreate)
    {
        $roleCreate->setRole($this->request->input('role'));
        $roleCreate->execute();
        $this->response->setData($roleCreate->getData());
        $this->response->execute();
    }

    public function roleMenu(ListRoleInterface $roleMenu){
        $roleMenu->setRoleId($this->request->input('roleId'));
        $roleMenu->roleMenu();
        $this->response->setData($roleMenu->getData());
        $this->response->execute();
    }

    public function updateRole(ListRoleInterface $updateRole){
        $updateRole->setRoleId($this->request->input('roleId'));
        $updateRole->setRoleList($this->request->input('roleList'));
        $updateRole->setRolePower();
        $this->response->setData($updateRole->getData());
        $this->response->execute();
    }

    public function deleteRole(ListRoleInterface $deleteRole){
        $deleteRole->setRoleId($this->request->input('roleId'));
        $deleteRole->delete();
        $this->response->setData($deleteRole->getData());
        $this->response->execute();
    }


}
