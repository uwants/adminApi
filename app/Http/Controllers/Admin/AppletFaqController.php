<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Faq\CreateFaqInterface;
use App\Contracts\Faq\DeleteFaqInterface;
use App\Contracts\Faq\GetFaqInterface;
use App\Contracts\Faq\ListFaqInterface;
use App\Contracts\Faq\SwitchFaqInterface;
use App\Contracts\Faq\UpdateFaqInterface;
use App\Http\Controllers\Controller;

class AppletFaqController extends Controller
{
    public function index(ListFaqInterface $listFaq)
    {
        $listFaq->setPage($this->getPage());
        $listFaq->setPageSize($this->getPageSize());
        $listFaq->execute();
        $this->response->setData($listFaq->getData());
        $this->response->execute();
    }

    public function create(CreateFaqInterface $createFaq)
    {
        $createFaq->setTitle($this->request->input('title'));
        $createFaq->setContent($this->request->input('content'));
        $createFaq->setSort($this->request->input('sort'));
        $createFaq->setSuspend($this->request->input('suspend'));
        $createFaq->execute();
        $id = $createFaq->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }

    public function update(UpdateFaqInterface $updateFaq)
    {
        $updateFaq->setId($this->request->input('id'));
        $updateFaq->setTitle($this->request->input('title'));
        $updateFaq->setContent($this->request->input('content'));
        $updateFaq->setSuspend($this->request->input('suspend'));
        $updateFaq->setSort($this->request->input('sort'));
        $updateFaq->execute();
        $this->response->execute();
    }

    public function delete(DeleteFaqInterface $deleteFaq)
    {
        $deleteFaq->setId($this->request->input('id'));
        $deleteFaq->execute();
        $this->response->execute();
    }

    public function switchStatus(SwitchFaqInterface $switchFaq)
    {
        $switchFaq->setId($this->request->input('id'));
        $switchFaq->setStatus($this->request->input('status'));
        $switchFaq->execute();
        $this->response->execute();
    }

    public function get(GetFaqInterface $getFaq)
    {

    }

}
