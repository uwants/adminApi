<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Opinion\ListOpinionInterface;
use App\Contracts\Opinion\GetOpinionInterface;

class OpinionController extends Controller
{
    public function index(ListOpinionInterface $list)
    {
        if ($this->request->input('keyword')) {
            $list->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $list->setTime($this->request->input('time'));
        }
        $list->setFilter($this->request->input('filter'));
        $list->setStatus($this->request->input('status'));
        $list->setOpinionStatus($this->request->input('opinionStatus'));
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        $list->execute();
        $this->response->setData($list->getData());
        $this->response->execute();
    }

    public function explode(ListOpinionInterface $explode){
        if ($this->request->input('keyword')) {
            $explode->setKeyword($this->request->input('keyword'));
        }
        if ($this->request->input('time')) {
            $explode->setTime($this->request->input('time'));
        }
        $explode->setStatus($this->request->input('status'));
        $explode->setOpinionStatus($this->request->input('opinionStatus'));
        $explode->explode();
        $this->response->setData($explode->getData());
        $this->response->execute();
    }

    public function show(GetOpinionInterface $show){
        $show->setId($this->request->input('id'));
        $show->execute();
        $this->response->setData($show->getData());
        $this->response->execute();
    }


}
