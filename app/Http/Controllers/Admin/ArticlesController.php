<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contracts\Article\ListArticleInterface;
use App\Contracts\Article\ListArticleAllInterface;
use App\Contracts\Article\GetArticleInterface;
use App\Contracts\Article\DeleteArticleInterface;
use App\Contracts\Article\CreateArticleInterface;
use App\Contracts\Article\ListEditionInterface;
use App\Contracts\Article\GetEditionInterface;
use App\Contracts\Article\CreateEditionInterface;
use App\Contracts\Article\SwitchEditionInterface;
use App\Contracts\Article\DeleteEditionInterface;

class ArticlesController extends Controller
{

    public function index(ListArticleInterface $list)
    {
        if ($this->request->input('keyword')) {
            $list->setKeyword($this->request->input('keyword'));
        }
        $list->setPage($this->getPage());
        $list->setPageSize($this->getPageSize());
        $list->execute();
        $data = $list->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function show(GetArticleInterface $get)
    {
        $get->setId($this->request->input('id'));
        $get->execute();
        $data = $get->getData();
        $this->response->setData($data);
        $this->response->execute();
    }

    public function store(CreateArticleInterface $create)
    {
        $create->setId($this->request->input('id'));
        $create->setTitle($this->request->input('title'));
        $create->setContent($this->request->input('content'));
        $create->setCat($this->request->input('cat'));
        $create->setIsOpen($this->request->input('isOpen'));
        $create->execute();
        $id = $create->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }

    public function destroy(DeleteArticleInterface $delete)
    {
        $delete->setId($this->request->input('id'));
        $delete->execute();
        $this->response->execute();
    }

    public function all(ListArticleAllInterface $all)
    {
        $all->execute();
        $this->response->setData($all->getData());
        $this->response->execute();
    }

    public function edition(ListEditionInterface $edition)
    {
        if ($this->request->input('keyword')) {
            $edition->setKeyword($this->request->input('keyword'));
        }
        $edition->setPage($this->getPage());
        $edition->setPageSize($this->getPageSize());
        $edition->execute();
        $this->response->setData($edition->getData());
        $this->response->execute();
    }

    public function detail(GetEditionInterface $get)
    {
        $get->setId($this->request->input('id'));
        $get->execute();
        $this->response->setData($get->getData());
        $this->response->execute();
    }

    public function delete(DeleteEditionInterface $delete)
    {
        $delete->setId($this->request->input('id'));
        $delete->execute();
        $this->response->execute();
    }

    public function create(CreateEditionInterface $create)
    {
        $create->setId($this->request->input('id'));
        $create->setTitle($this->request->input('title'));
        $create->setContent($this->request->input('content'));
        $create->setCreateTime($this->request->input('createTime'));
        $create->setSort($this->request->input('sort'));
        $create->setStatus($this->request->input('status'));
        $create->execute();
        $id = $create->getId();
        $this->response->setData(compact('id'));
        $this->response->execute();
    }

    public function switch(SwitchEditionInterface $switch)
    {
        $switch->setId($this->request->input('id'));
        $switch->setStatus($this->request->input('status'));
        $switch->execute();
        $this->response->setData($switch->getData());
        $this->response->execute();
    }

}
