<?php

namespace App\Http\Controllers;

use App\Contracts\Response\SuccessResponseInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected SuccessResponseInterface $response;
    protected Request $request;
    protected int $page = 1;
    protected int $pageSize = 20;

    /**
     * Controller constructor.
     * @param SuccessResponseInterface $response
     * @param Request $request
     */
    public function __construct(SuccessResponseInterface $response, Request $request)
    {
        $this->response = $response;
        $this->request = $request;
        if($this->request->has('page')){
            $this->setPage(max(1, $this->request->input('page')));
        }
        if ($this->request->has('pageSize')) {
            $this->setPageSize(max(1, min(100, $this->request->input('pageSize'))));
        }
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize(int $pageSize): void
    {
        $this->pageSize = $pageSize;
    }

}
