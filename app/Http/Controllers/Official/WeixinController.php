<?php

namespace App\Http\Controllers\Official;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Laravel\Socialite\Facades\Socialite;
use App\User;
use Socialite;

class WeixinController extends Controller
{
    public function weixin(){
        return Socialite::with('weixin')->redirect();
    }

    public function weixinLogin(){
        $user = Socialite::driver('weixin')->user();
        dd($user);
    }
}
