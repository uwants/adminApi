<?php

namespace App\Http\Controllers\Official;

use App\Http\Controllers\Controller;
use App\Contracts\Website\Home\GetHomeInterface;
class HomeController extends Controller
{
    public function index(GetHomeInterface $index)
    {
        $index->setId($this->request->input('userId'));
        $index->execute();
        $this->response->setData($index->getData());
        $this->response->execute();
    }
}
