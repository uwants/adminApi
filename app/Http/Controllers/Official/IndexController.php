<?php

namespace App\Http\Controllers\Official;


use App\Http\Controllers\Controller;
use App\Contracts\Website\Index\GetMessageInterface;
use App\Contracts\Website\Index\ListContentInterface;
use App\Contracts\Website\Index\GetContentInterface;
use App\Contracts\Website\Index\ListSortInterface;
use App\Contracts\Website\Index\LoginCodeInterface;

class IndexController extends Controller
{
    /**
     * @param GetMessageInterface $message
     * 官网统计数据
     */
    public function index(GetMessageInterface $message)
    {
        $message->execute();
        $this->response->setData($message->getData());
        $this->response->execute();
    }

    /**
     * @param ListContentInterface $content
     * 资讯列表
     */
    public function content(ListContentInterface $content)
    {
        $content->setPage($this->getPage());
        $content->setPageSize($this->getPageSize());
        $content->execute();
        $this->response->setData($content->getData());
        $this->response->execute();
    }

    /**
     * @param GetContentInterface $inter
     * 获取文章详情
     */
    public function information(GetContentInterface $inter)
    {
        $inter->setMediaId($this->request->input('mediaId'));
        $inter->setTitle($this->request->input('title'));
        $inter->execute();
        $this->response->setData($inter->getData());
        $this->response->execute();
    }

    /**
     * 最近七天企业收到意见
     * @param ListSortInterface $sort
     */
    public function sort(ListSortInterface $sort)
    {
        $sort->execute();
        $this->response->setData($sort->getData());
        $this->response->execute();
    }

    public function code(LoginCodeInterface $code)
    {
        $code->setCode($this->request->input('code'));
        $code->execute();
        $this->response->setData($code->getData());
        $this->response->execute();
    }

    public function register(LoginCodeInterface $register)
    {
        $register->setCode($this->request->input('code'));
        $register->register();
        $this->response->setData($register->getData());
        $this->response->execute();
    }

}
