<?php

namespace App\Http\Middleware;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class WebsiteToken
{
    public function handle($request, Closure $next)
    {
        $token = $request->header('authorization');
        $user = User::query()
            ->where('token', $token)
            ->where('token_expired_at', '>=', Carbon::now()->format('Y-m-d H:i:d'))
            ->first();

        if ($user) {
            $request->offsetSet('userId', $user->getKey());
        } else {
            return ['code' => 1, 'message' => '登录超时，请重新登录！', 'data' => ['error' => 10000, 'wechat' => '请重新登录!']];
        }
        return $next($request);
    }
}
