<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AuthWebsite
{
    public function handle($request, Closure $next)
    {
        return $next($request);

    }
}

