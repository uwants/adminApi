<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class VerifyAdminToken
{
    public function handle($request, Closure $next)
    {
        $token = !empty($request->input('access-token')) ? $request->input('access-token') : '';

        if (empty($token)) {
            return response('未授权，token不存在!', 401);
        } else {
            $userId = Cache::get('admin_user_token_from_id_' . $token);
            if (empty($userId)) {
                return  'out';
            }
            Auth::loginUsingId($userId);
            return $next($request);
        }
    }
}
