<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

function showImage($url, $type = '')
{
    switch ($type) {
        case'avatar':
            if (empty($url)) {
                $urlList = 'https://cjyjxqny.missapp.com/avatar/2021/05/25/PxCldgLd60ac9a7be3b59.png';
            } else {
                $urlList = $url;
            }
            break;
        case'company':
            if (empty($url)) {
                $urlList = 'https://cjyjxqny.missapp.com/tmp/y84uNqr1CinNb99376e1c54886804d5a196f15811138.png';
            } else {
                $urlList = $url;
            }
            break;
        default:
            if (empty($url)) {
                $urlList = 'https://cjyjxqny.missapp.com/no_picture.jpg';
            } else {
                $urlList = $url;
            }
    }
    return $urlList;
}


function checkImage($url)
{
    if (empty($url)) {
        $urlList = '';
    } elseif ($url == 'https://cjyjxqny.missapp.com/no_picture.jpg') {
        $urlList = '';
    } else {
        $urlList = $url;
    }
    return $urlList;
}

/**
 * @return mixed
 * 服务号token
 */
function serviceAccessToken()
{
    $appId = env('WX_SER_APP_ID');
    $secret = env('WX_SER_APP_SECRET');

    $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appId&secret=$secret";
    $accessToken = Cache::get('app_access_token_' . $appId);

    if (!empty($accessToken)) {
        $access_token = $accessToken;
    } else {
        $mes = curl_get($url, []);
        $re_mes = json_decode($mes);
        if (!empty($re_mes->errcode)) {
            return $re_mes;
        }
        Cache::put('app_access_token_' . $appId, $re_mes->access_token, '100');
        $access_token = $re_mes->access_token;
    }
    return $access_token;
}


/**
 * @return mixed
 * 超级意见箱i
 */
function newAccessToken()
{
    $appId = 'wxb0ae9a693e04ae07';
    $secret = '57f00630998f44892f690689b3f405a8';

    $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appId&secret=$secret";
    $accessToken = Cache::get('app_access_token_' . $appId);

    if (!empty($accessToken)) {
        $access_token = $accessToken;
    } else {
        $mes = curl_get($url, []);
        $re_mes = json_decode($mes);
        if (!empty($re_mes->errcode)) {
            return $re_mes;
        }
        Cache::put('app_access_token_' . $appId, $re_mes->access_token, '100');
        $access_token = $re_mes->access_token;
    }
    return $access_token;
}

/**
 * @param $code
 * @return mixed
 * 开放平台
 */
function openPlatformToken($code)
{
    $appId = 'wx00011aef64d8e691';
    $secret = 'f51078feb25bb52903c6b1ad479932dc';
    $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=$appId&secret=$secret&code=$code&grant_type=authorization_code";
    $mes = curl_get($url, []);
    $token = json_decode($mes);
    if (!empty($token->errcode)) {
        return $token;
    } else {
        $urlUser = "https://api.weixin.qq.com/sns/userinfo?access_token=$token->access_token&openid=$token->openid&lang=zh_CN";
        $user = curl_get($urlUser, []);
        return json_decode($user);
    }
}

function getWebToken()
{
    $string = '';
    for ($i = 0; $i < 99; $i++) {
        $string = Str::random(64);
        $message = DB::table('users')
            ->select('id')
            ->where('token', $string)
            ->first();
        if (empty($message->id)) {
            break;
        }
    }
    return $string;
}


function curl_get($url, $headOPT = [])
{
    if (empty($headOPT)) {
        $headOPT = array('platform_src:WAP', 'cookie_id:', 'systype:wap');
    }
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // 获取数据返回
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true); // 在启用  时候将获取数据返回
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headOPT);//修改header信息
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 8);

    $out_put = curl_exec($ch);
    curl_close($ch);
    unset($ch);
    return $out_put;
}

function curl_post($url, $curlPost, $headerOpt = [])
{

    if (empty($headerOpt)) {
        $headerOpt = array('platform_src:WAP', 'cookie_id:', 'systype:wap');
    }
    $ch = curl_init();//初始化curl
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);//抓取指定网页
    curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headerOpt);//修改header信息
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $out_put = curl_exec($ch);//运行curl
    curl_close($ch);
    return $out_put;
}

// 过滤掉emoji表情
function filterEmoji($str)
{
    $str = preg_replace_callback('/./u',
        function (array $match) {
            return strlen($match[0]) >= 4 ? '' : $match[0];
        },
        $str);
    return $str;
}

function unique_array_color($total)
{
    $arrayColor = [
        '#e3a189', '#e58876', '#f0a36b', '#db7c5c', '#db7c5c',
        '#c2ca65', '#aec460', '#aec460', '#aec460', '#bace8f',
        '#84a463', '#7fb06e', '#79888d', '#90acc4', '#cee2df',
        '#eab1b8', '#f6d1e0', '#eed8d8', '#f0c8bf', '#e9b4a4',
        '#e6d590', '#dfce66', '#e5dd94', '#dcd384', '#dedcac',
        '#dfdae1', '#d7bfcc', '#b587a3', '#b794b4', '#b66a88',
        '#e1e1c5', '#add08c', '#c8dcb7', '#b5ddb9', '#88c17c',];
    $newArray = array();
    shuffle($arrayColor);
    $length = count($arrayColor);
    for ($i = 0; $i < $total; $i++) {
        if ($i < $length) {
            $newArray[] = $arrayColor[$i];
        }
    }
    return $newArray;
}

