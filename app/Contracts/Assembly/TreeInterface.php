<?php

namespace App\Contracts\Assembly;

interface TreeInterface
{
    public function execute(): void;

    public function getData(): array;

}
