<?php

namespace App\Contracts\Assembly;


interface DragInterface
{
    public function execute(): void;

    public function getData(): array;
}
