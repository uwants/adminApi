<?php

namespace App\Contracts\Assembly;

interface OrganizationInterface
{
    public function execute(): void;

    public function getData(): array;
}
