<?php


namespace App\Contracts\InternalUser;


interface ListInternalUserInterface
{
    public function execute():void;

    public function getData():array;

    public function setPage(int $page):void;

    public function setPageSize(int $pageSize):void;

}
