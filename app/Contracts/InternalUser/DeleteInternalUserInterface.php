<?php


namespace App\Contracts\InternalUser;


interface DeleteInternalUserInterface
{

    public function execute():void;

    public function setId(int $id):void;

    public function getUserId():int;

}
