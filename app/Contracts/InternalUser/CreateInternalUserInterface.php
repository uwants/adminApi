<?php


namespace App\Contracts\InternalUser;


interface CreateInternalUserInterface
{
    public function execute():void;

    public function setUserId(int $userId):void;

    public function getUserId():int;

}
