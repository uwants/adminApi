<?php

namespace App\Contracts\Index;

interface ListIndexInterface
{
    public function execute(): void;

    public function thirtyOp(): void;

    public function thirtyBox(): void;

    public function getData(): array;
}
