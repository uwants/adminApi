<?php


namespace App\Contracts\Banner;


interface GetBannerInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
