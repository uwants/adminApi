<?php


namespace App\Contracts\Banner;


interface UpdateBannerInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setImage(string $title): void;

    public function setContent(string $content): void;

    public function setSort(int $sort): void;

    public function setSuspend(bool $suspend): void;

    public function getId(): int;
}
