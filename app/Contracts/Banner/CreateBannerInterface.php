<?php


namespace App\Contracts\Banner;


interface CreateBannerInterface
{
    public function execute():void;

    public function setImage(string $title):void;

    public function setContent(string $content):void;

    public function setSort(int $sort):void;

    public function setSuspend(bool $suspend):void;

    public function getId():int;
}
