<?php


namespace App\Contracts\Banner;


interface ListBannerInterface
{
    public function execute(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function getData(): array;
}
