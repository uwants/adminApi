<?php


namespace App\Contracts\Banner;


interface DeleteBannerInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}
