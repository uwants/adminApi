<?php


namespace App\Contracts\Banner;


interface SwitchBannerInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setStatus(string $status): void;

    public function getData(): array;
}
