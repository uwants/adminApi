<?php

namespace App\Contracts\WechatTemplate;

interface UpdateMenuInterface
{
    public function execute(): void;

    public function setMenuMessage(array $menuMessage): void;

    public function getData(): array;
}
