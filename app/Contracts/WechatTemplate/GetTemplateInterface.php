<?php

namespace App\Contracts\WechatTemplate;

interface GetTemplateInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
