<?php

namespace App\Contracts\WechatTemplate;

interface PullTemplateInterface
{
    public function execute(): void;

    public function getData(): array;
}
