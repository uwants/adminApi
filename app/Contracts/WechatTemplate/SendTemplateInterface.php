<?php

namespace App\Contracts\WechatTemplate;

interface SendTemplateInterface
{
    public function execute(): void;

    public function setIds(array $ids): void;

    public function setTemplateId(string $templateId): void;

    public function setJumpUrl(string $jumpUrl): void;

    public function setJumpType(string $jumpType): void;

    public function setMessage(array $message): void;

    public function sendTemplate(array $sendMsg): array;

    public function getSAppAccessToken(): array;

    public function getData(): array;
}
