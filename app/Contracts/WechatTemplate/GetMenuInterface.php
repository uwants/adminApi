<?php

namespace App\Contracts\WechatTemplate;

interface GetMenuInterface
{
    public function execute(): void;

    public function getData(): array;
}
