<?php
namespace App\Contracts\Boxes;

interface GetBoxInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
