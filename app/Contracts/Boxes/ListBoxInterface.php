<?php

namespace App\Contracts\Boxes;

interface ListBoxInterface
{
    public function execute(): void;

    public function explode(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function setStatus(string $status): void;

    public function setBoxStatus(string $boxStatus): void;

    public function setBoxType(string $boxType): void;

    public function setTime(array $time): void;

    public function setExportData(string $exportData): void;

    public function setSortColumn(string $sortColumn): void;

    public function setSortVal(string $sortVal): void;

    public function getData(): array;
}
