<?php

namespace App\Contracts\Keyword;

interface GetKeywordInterface
{
    public function execute(): void;

    public function setKeyword(string $keyword): void;

    public function getData(): array;
}
