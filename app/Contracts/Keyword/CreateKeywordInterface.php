<?php

namespace App\Contracts\Keyword;

interface CreateKeywordInterface
{
    public function execute(): void;

    public function setValue(string $value): void;

    public function getData(): array;
}
