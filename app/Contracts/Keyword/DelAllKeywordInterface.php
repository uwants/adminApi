<?php

namespace App\Contracts\Keyword;

interface DelAllKeywordInterface
{
    public function execute(): void;

    public function setIds(array $ids): void;

    public function getIds(): array;
}
