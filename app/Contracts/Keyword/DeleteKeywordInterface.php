<?php

namespace App\Contracts\Keyword;

interface DeleteKeywordInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}
