<?php


namespace App\Contracts\Reply;

interface GetReplyInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
