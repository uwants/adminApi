<?php
namespace App\Contracts\Opinion;

interface GetOpinionInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
