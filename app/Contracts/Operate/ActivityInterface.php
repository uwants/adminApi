<?php

namespace App\Contracts\Operate;

interface ActivityInterface
{
    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function execute(): void;

    public function getData(): array;
}
