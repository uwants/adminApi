<?php

namespace App\Contracts\Operate;

interface CreateActivityInterface
{
    public function execute(): void;

    public function setActivity(array $activity): void;

    public function getData(): array;
}
