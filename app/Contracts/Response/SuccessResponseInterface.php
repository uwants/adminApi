<?php

namespace App\Contracts\Response;

use Illuminate\Http\Exceptions\HttpResponseException;

interface SuccessResponseInterface
{
    /**
     * @param string $message
     */
    public function setMessage(string $message): void;

    /**
     * @param array $data
     */
    public function setData(array $data): void ;

    /**
     * @return HttpResponseException
     */
    public function execute(): HttpResponseException;
}
