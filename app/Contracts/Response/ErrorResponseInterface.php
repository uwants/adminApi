<?php

namespace App\Contracts\Response;

use Illuminate\Http\Exceptions\HttpResponseException;

interface ErrorResponseInterface
{
    /**
     * @param int $code
     */
    public function setCode(int $code): void ;

    /**
     * @param string $message
     */
    public function setMessage(string $message): void;

    /**
     * @return HttpResponseException
     */
    public function execute(): HttpResponseException;
}
