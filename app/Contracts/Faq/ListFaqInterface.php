<?php


namespace App\Contracts\Faq;


interface ListFaqInterface
{
    public function execute(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function getData(): array;
}
