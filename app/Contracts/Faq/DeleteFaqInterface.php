<?php


namespace App\Contracts\Faq;


interface DeleteFaqInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}
