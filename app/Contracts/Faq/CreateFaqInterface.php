<?php


namespace App\Contracts\Faq;


interface CreateFaqInterface
{
    public function execute():void;

    public function setTitle(string $title):void;

    public function setContent(string $content):void;

    public function setSort(int $sort):void;

    public function setSuspend(bool $suspend):void;

    public function getId():int;
}
