<?php


namespace App\Contracts\Faq;


interface UpdateFaqInterface
{
    public function execute(): void;

    public function setTitle(string $title): void;

    public function setContent(string $content): void;

    public function setSort(int $sort): void;

    public function setSuspend(int $suspend): void;

    public function setId(int $id): void;

    public function getId(): int;

}
