<?php


namespace App\Contracts\Faq;


interface GetFaqInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
