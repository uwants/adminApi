<?php


namespace App\Contracts\Faq;


interface SwitchFaqInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setStatus(string $status): void;

    public function getData(): array;
}
