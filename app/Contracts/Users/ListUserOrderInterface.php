<?php

namespace App\Contracts\Users;

interface ListUserOrderInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setStatus(string $status): void;

    public function getData(): array;
}
