<?php

namespace App\Contracts\Users;

interface ListUserOpInterface
{
    public function execute(): void;

    public function setUserId(int $userId): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function getData(): array;
}
