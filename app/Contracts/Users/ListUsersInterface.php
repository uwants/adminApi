<?php

namespace App\Contracts\Users;

interface ListUsersInterface
{
    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function setSearchType(string $searchType): void;

    public function setTime(array $time): void;

    public function setExportData(string $exportData): void;

    public function setSortColumn(string $sortColumn): void;

    public function setSortVal(string $sortVal): void;

    public function execute(): void;

    public function getData(): array;
}
