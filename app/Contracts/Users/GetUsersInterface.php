<?php

namespace App\Contracts\Users;

interface GetUsersInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
