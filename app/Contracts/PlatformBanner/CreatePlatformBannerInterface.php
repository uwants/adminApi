<?php


namespace App\Contracts\PlatformBanner;


interface CreatePlatformBannerInterface
{
    public function execute(): void;

    public function setImage(string $title): void;

    public function setUrl(string $content): void;

    public function setSort(int $sort): void;

    public function setSuspend(bool $suspend): void;

    public function setPlatformCode(string $platform): void;

    public function getId(): int;
}