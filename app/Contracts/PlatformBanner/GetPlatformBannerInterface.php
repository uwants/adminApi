<?php


namespace App\Contracts\PlatformBanner;


interface GetPlatformBannerInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}