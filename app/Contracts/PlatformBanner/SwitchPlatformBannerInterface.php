<?php


namespace App\Contracts\PlatformBanner;


interface SwitchPlatformBannerInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setStatus(string $status): void;

    public function getData(): array;
}