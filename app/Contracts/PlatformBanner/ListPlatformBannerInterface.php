<?php


namespace App\Contracts\PlatformBanner;


interface ListPlatformBannerInterface
{
    public function execute(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setPlatformCode(string $platformCode): void;

    public function getData(): array;
}