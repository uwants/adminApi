<?php


namespace App\Contracts\PlatformBanner;


interface DeletePlatformBannerInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}