<?php
namespace App\Contracts\AdminUser;

interface ListRoleInterface
{
    public function execute(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function setState(string $keyState): void;

    public function getData(): array;
}
