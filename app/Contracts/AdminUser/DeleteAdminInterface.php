<?php

namespace App\Contracts\AdminUser;

interface DeleteAdminInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}
