<?php

namespace App\Contracts\AdminUser;

interface CreateAdminUserInterface
{
    public function execute(): void;

    public function setAdminName(string $adminName): void;

    public function setPassword(string $password): void;

    public function setRoleId(int $roleId): void;

    public function getId(): int;
}
