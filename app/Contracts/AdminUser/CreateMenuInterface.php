<?php

namespace App\Contracts\AdminUser;

interface CreateMenuInterface
{
    public function execute(): void;

    public function setMenu(array $menu): void;

    public function getData(): array;
}
