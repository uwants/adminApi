<?php
namespace App\Contracts\AdminUser;

interface CreateRoleInterface
{
    public function execute(): void;

    public function setRole(array $role): void;

    public function getData(): array;
}
