<?php

namespace App\Contracts\AdminUser;

interface GetAdminUserInterface
{
    public function execute(): void;

    public function setAdminName(string $adminName): void;

    public function setPassword(string $passWord): void;

    public function getData(): array;
}
