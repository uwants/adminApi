<?php

namespace App\Contracts\AdminUser;

interface DeleteMenuInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}
