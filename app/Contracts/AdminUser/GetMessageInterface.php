<?php

namespace App\Contracts\AdminUser;

interface GetMessageInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
