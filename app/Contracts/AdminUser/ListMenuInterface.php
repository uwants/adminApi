<?php
namespace App\Contracts\AdminUser;

interface ListMenuInterface
{
    public function execute(): void;

    public function getData(): array;

}
