<?php

namespace App\Contracts\AdminUser;

interface UpdateAdminInterface
{
    public function execute(): void;

    public function setData(array $date): void;

    public function getId(): int;

}
