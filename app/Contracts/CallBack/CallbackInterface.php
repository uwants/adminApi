<?php

namespace App\Contracts\CallBack;

interface CallbackInterface
{
    public function execute(): void;

    public function setMessage(array $message): void;

    public function getData(): string;

}
