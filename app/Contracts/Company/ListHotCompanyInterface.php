<?php

namespace App\Contracts\Company;

interface ListHotCompanyInterface
{
    public function execute(): void;

    public function update(): void;

    public function delete(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function setId(int $id): void;

    public function setName(string $name): void;

    public function setLogo(string $logo): void;

    public function getData(): array;
}
