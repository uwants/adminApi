<?php

namespace App\Contracts\Company;

interface ListCompanyApplyInterface
{
    public function execute(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function setStatus(string $status): void;

    public function setTime(array $time): void;

    public function getData(): array;
}
