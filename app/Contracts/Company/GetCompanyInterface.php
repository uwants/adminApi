<?php

namespace App\Contracts\Company;

interface GetCompanyInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
