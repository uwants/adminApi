<?php

namespace App\Contracts\Company;

interface ListCompanyInterface
{
    public function execute(): void;

    public function allList(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function setStatus(string $status): void;

    public function setCompanyStatus(string $companyStatus): void;

    public function setTime(array $time): void;

    public function setExportData(string $exportData): void;

    public function setSortColumn(string $sortColumn): void;

    public function setSortVal(string $sortVal): void;

    public function getData(): array;
}
