<?php

namespace App\Contracts\Company;

interface ListComOpInterface
{
    public function execute(): void;

    public function setCompanyId(int $companyId): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function getData(): array;
}
