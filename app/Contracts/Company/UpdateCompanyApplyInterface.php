<?php

namespace App\Contracts\Company;

interface UpdateCompanyApplyInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setStatus(string $status): void;

    public function setReason(string $reason): void;

    public function setName(string $name): void;

    public function setOldName(string $oldName): void;

    public function getId(): int;
}
