<?php

namespace App\Contracts\Company;

interface UpdateCompanyInterface
{
    public function execute(): void;

    public function setData(array $data): void;

    public function setId(int $id): void;

    public function getId(): int;
}
