<?php

namespace App\Contracts\Article;

interface DeleteEditionInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}
