<?php

namespace App\Contracts\Article;

interface ListArticleAllInterface
{
    public function execute(): void;

    public function getData(): array;
}
