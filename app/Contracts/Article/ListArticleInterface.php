<?php

namespace App\Contracts\Article;

interface ListArticleInterface
{
    public function execute(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setKeyword(string $keyword): void;

    public function getData(): array;
}
