<?php

namespace App\Contracts\Article;

interface SwitchEditionInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setStatus(string $status): void;

    public function getData(): array;
}
