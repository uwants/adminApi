<?php

namespace App\Contracts\Article;

interface CreateArticleInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setTitle(string $title): void;

    public function setContent(string $content): void;

    public function setCat(string $cat): void;

    public function setIsOpen(string $isOpen): void;

    public function getId(): int;
}
