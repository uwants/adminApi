<?php

namespace App\Contracts\Article;

interface GetEditionInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getData(): array;
}
