<?php

namespace App\Contracts\Article;

interface DeleteArticleInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function getId(): int;
}
