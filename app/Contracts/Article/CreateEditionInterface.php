<?php

namespace App\Contracts\Article;

interface CreateEditionInterface
{
    public function execute(): void;

    public function setId(int $id): void;

    public function setTitle(string $title): void;

    public function setContent(string $content): void;

    public function setStatus(string $status): void;

    public function setCreateTime(string $createTime): void;

    public function setSort(int $sort): void;

    public function getId(): int;
}
