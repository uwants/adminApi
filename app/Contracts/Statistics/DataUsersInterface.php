<?php

namespace App\Contracts\Statistics;

interface DataUsersInterface
{
    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setTime(array $time): void;

    public function setTimeState(string $timeState): void;

    public function setExportData(string $exportData): void;

    public function setSortColumn(string $sortColumn): void;

    public function setSortVal(string $sortVal): void;

    public function execute(): void;

    public function picExecute(): void;

    public function getData(): array;
}
