<?php

namespace App\Contracts\Statistics;

interface ActivityOpInterface
{
    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setExportData(string $exportData): void;

    public function setBoxId(int $boxId): void;

    public function execute(): void;

    public function getData(): array;
}
