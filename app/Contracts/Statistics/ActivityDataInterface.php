<?php

namespace App\Contracts\Statistics;

interface ActivityDataInterface
{
    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function setActivityId(int $activityId): void;

    public function setTime(array $time): void;

    public function setTimeState(string $timeState): void;

    public function setActivityType(string $activityType): void;

    public function setNumberSelect(string $numberSelect): void;

    public function setNumber(int $number): void;

    public function setSortColumn(string $sortColumn): void;

    public function setSortVal(string $sortVal): void;

    public function execute(): void;

    public function executeBox(): void;

    public function executeUser(): void;

    public function getData(): array;
}
