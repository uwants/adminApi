<?php

namespace App\Contracts\Statistics;

interface EveryDataInterface
{
    public function setTime(array $time): void;

    public function setSortColumn(string $sortColumn): void;

    public function setTimeState(string $timeState): void;

    public function setSortVal(string $sortVal): void;

    public function execute(): void;

    public function getData(): array;
}
