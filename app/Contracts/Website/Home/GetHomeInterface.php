<?php

namespace App\Contracts\Website\Home;

interface GetHomeInterface
{
    public function execute(): void;

    public function getData(): array;
}
