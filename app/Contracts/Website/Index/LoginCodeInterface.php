<?php

namespace App\Contracts\Website\Index;

interface LoginCodeInterface
{
    public function execute(): void;

    public function register(): void;

    public function setCode(string $code): void;

    public function getData(): array;
}
