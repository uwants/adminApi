<?php

namespace App\Contracts\Website\Index;

interface ListSortInterface
{
    public function execute(): void;

    public function getData(): array;
}
