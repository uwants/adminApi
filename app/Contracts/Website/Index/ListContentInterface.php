<?php

namespace App\Contracts\Website\Index;


interface ListContentInterface
{
    public function execute(): void;

    public function setPage(int $page): void;

    public function setPageSize(int $pageSize): void;

    public function getData(): array;
}
