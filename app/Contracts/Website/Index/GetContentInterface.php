<?php

namespace App\Contracts\Website\Index;

interface GetContentInterface
{
    public function execute(): void;

    public function setMediaId(string $mediaId): void;

    public function setTitle(string $title): void;

    public function getData(): array;
}
