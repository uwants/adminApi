<?php

namespace App\Contracts\Website\Index;


interface GetMessageInterface
{
    public function execute(): void;

    public function getData(): array;
}
