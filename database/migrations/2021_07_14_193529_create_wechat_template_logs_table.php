<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWechatTemplateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wechat_template_logs', function (Blueprint $table) {
            $table->id();
            $table->string('msgid')->comment('发送id');
            $table->string('template_id')->comment('模板id');
            $table->string('openid')->comment('openid');
            $table->string('errcode')->comment('错误代码');
            $table->string('errmsg')->comment('错误信息');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wechat_template_logs');
    }
}
