<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_menus', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('路由名字');
            $table->string('path')->comment('路径');
            $table->string('title')->comment('路由标题');
            $table->string('icon')->comment('图标');
            $table->string('power_name')->comment('权限名字');
            $table->foreignId('parent_id')->comment('父id');
            $table->foreignId('level')->comment('等级');
            $table->boolean('is_ban')->default(false)->comment('是否禁止');
            $table->foreignId('sort')->default(100)->comment('排序');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_menus');
    }
}
