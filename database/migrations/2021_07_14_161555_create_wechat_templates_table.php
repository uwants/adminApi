<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWechatTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wechat_templates', function (Blueprint $table) {
            $table->id();
            $table->string('template_title')->index()->comment('模板标题');
            $table->string('template_id')->index()->comment('模板id');
            $table->text('content')->comment('模板内容');
            $table->string('primary_industry')->comment('第一产业');
            $table->string('deputy_industry')->comment('副业');
            $table->string('example')->comment('模板例子');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wechat_templates');
    }
}
