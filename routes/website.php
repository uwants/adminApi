<?php
//不用登陆
Route::middleware(['auth.web'])->group(function () {

    Route::post('homePage/index', 'IndexController@index');
    Route::post('information/list', 'IndexController@content');
    Route::post('information/message', 'IndexController@information');
    Route::post('information/sort', 'IndexController@sort');

    Route::post('code/login', 'IndexController@code');
    Route::post('code/register', 'IndexController@register');

//微信一键登录
    Route::get('/weixin', 'WeixinController@weixin')->name('weixin');
    Route::get('/weixin/callback', 'WeixinController@weixinLogin');



});

//需要登录接口
Route::middleware(['website'])->group(function () {
    //后台数据
    Route::post('home/message', 'HomeController@index');

});

