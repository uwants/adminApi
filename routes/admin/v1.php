<?php

use Illuminate\Support\Facades\Route;

// 登陆接口
Route::post('/login', 'TestController@index');


Route::middleware(['auth.wechat', 'auth'])->group(function () {
    // 获取用户手机号
    Route::post('/Users/mobile', 'UsersController@mobile');

});


