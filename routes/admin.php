<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::post('test/index', 'TestController@index');
Route::post('create', 'AdminUsersController@create');
Route::post('adminLogin', 'AdminUsersController@login');
Route::post('image/create', 'ImagesController@create');
Route::any('follow/callback', 'CallbackController@index');
Route::post('follow/token', 'TestController@token');
Route::any('register', 'TestController@register');


Route::any('logout', function () {
    Artisan::call('cache:clear');
    return 'login';
});

Route::middleware(['auth.admin'])->group(function () {
    //管理员中心
    Route::post('admin/get', 'AdminUsersController@show');
    Route::post('admin/update', 'AdminUsersController@update');
    Route::post('admin/list', 'AdminUsersController@index');
    Route::post('admin/create', 'AdminUsersController@create');
    Route::post('admin/delete', 'AdminUsersController@delete');
    Route::post('admin/menu', 'AdminUsersController@menu');

    //菜单管理
    Route::post('admin/createMenu', 'AdminUsersController@createMenu');
    Route::post('admin/listMenu', 'AdminUsersController@listMenu');
    Route::post('admin/deleteMenu', 'AdminUsersController@deleteMenu');

    //角色管理
    Route::post('admin/role', 'AdminUsersController@role');
    Route::post('admin/createRole', 'AdminUsersController@roleCreate');
    Route::post('admin/roleMenu', 'AdminUsersController@roleMenu');
    Route::post('admin/updateRole', 'AdminUsersController@updateRole');
    Route::post('admin/deleteRole', 'AdminUsersController@deleteRole');

    //内部用户管理
    Route::post('internalUser/get', 'InternalUserController@index');
    Route::post('internalUser/delete', 'InternalUserController@destory');
    Route::post('internalUser/create', 'InternalUserController@store');


    //企业中心
    Route::post('company/get', 'CompanyController@apply');//企业认证列表
    Route::post('company/examine', 'CompanyController@check');//企业审核
    Route::post('company/list', 'CompanyController@index');//企业列表
    Route::post('company/update', 'CompanyController@update');
    Route::post('company/message', 'CompanyController@message');//企业详情
    Route::post('company/box', 'CompanyController@box');//企业意见箱
    Route::post('company/opinion', 'CompanyController@opinion');//企业收到意见
    Route::post('company/member', 'CompanyController@member');//企业成员
    Route::post('company/allList', 'CompanyController@select');//企业搜索列表
    Route::post('company/hot', 'CompanyController@hot');//热门企业列表
    Route::post('company/updateHot', 'CompanyController@updateHot');//热门企业列表编辑
    Route::post('company/deleteHot', 'CompanyController@deleteHot');//热门企业列表编辑
    Route::post('company/switchHot', 'CompanyController@switchHot');//企业状态编辑


    //用户
    Route::post('users/get', 'UsersController@index');
    Route::post('users/detail', 'UsersController@show');
    Route::post('users/box', 'UsersController@box');
    Route::post('users/opinion', 'UsersController@opinion');

    //文章管理
    Route::post('articles/get', 'ArticlesController@index');
    Route::post('articles/detail', 'ArticlesController@show');
    Route::post('articles/create', 'ArticlesController@store');
    Route::post('articles/delete', 'ArticlesController@destroy');
    Route::post('articles/getAll', 'ArticlesController@all');

    //版本管理
    Route::post('edition/get', 'ArticlesController@edition');
    Route::post('edition/detail', 'ArticlesController@detail');
    Route::post('edition/create', 'ArticlesController@create');
    Route::post('edition/delete', 'ArticlesController@delete');
    Route::post('edition/switch', 'ArticlesController@switch');

    //消息模板
    Route::post('template/get', 'TemplateController@index');
    Route::post('template/pull', 'TemplateController@pull');
    Route::post('template/message', 'TemplateController@message');//详情
    Route::post('template/send', 'TemplateController@send');//发送模板消息

    //微信公众号自定义菜单
    Route::post('wechat/index', 'AppletsController@index');
    Route::post('wechat/update', 'AppletsController@update');
    //意见中心
    Route::post('opinion/index', 'OpinionController@index');
    Route::post('opinion/explode', 'OpinionController@explode');
    Route::post('opinion/get', 'OpinionController@show');

    //意见回复
    Route::post('reply/index', 'ReplyController@index');
    Route::post('reply/explode', 'ReplyController@explode');
    Route::post('reply/get', 'ReplyController@get');

    //意见箱
    Route::any('box/index', 'BoxesController@index');
    Route::post('box/explode', 'BoxesController@explode');
    Route::post('box/get', 'BoxesController@show');
    Route::post('box/getOpinion', 'BoxesController@opinion');

    //首页
    Route::post('index/index', 'IndexController@index');
    Route::post('index/box', 'IndexController@box');
    Route::post('index/opinion', 'IndexController@opinion');

    //关键词过滤
    Route::any('AppletKeywords/getMsg', 'KeywordController@index');
    Route::any('AppletKeywords/insertMsg', 'KeywordController@create');
    Route::any('AppletKeywords/delMsg', 'KeywordController@delete');
    Route::any('AppletKeywords/delAllMsg', 'KeywordController@delAll');

    //数据中心
    Route::post('data/userList', 'DataController@userDataList');
    Route::post('data/every', 'DataController@every');
    Route::post('data/userPic', 'DataController@userDataPic');
    Route::post('data/activity', 'DataController@activity');
    Route::post('data/activityBox', 'DataController@activityBox');
    Route::post('data/activityUser', 'DataController@activityUser');
    Route::post('data/opinion', 'DataController@getOpinion');

    //FAQ管理
    Route::post('AppletFaq/get', 'AppletFaqController@index');
    Route::post('AppletFaq/update', 'AppletFaqController@update');
    Route::post('AppletFaq/delete', 'AppletFaqController@delete');
    Route::post('AppletFaq/create', 'AppletFaqController@create');
    Route::post('AppletFaq/detail', 'AppletFaqController@get');
    Route::post('AppletFaq/switchStatus', 'AppletFaqController@switchStatus');

    //Banner管理
    Route::post('AppletBanner/get', 'BannerController@index');
    Route::post('AppletBanner/update', 'BannerController@update');
    Route::post('AppletBanner/delete', 'BannerController@delete');
    Route::post('AppletBanner/create', 'BannerController@create');
    Route::post('AppletBanner/detail', 'BannerController@get');
    Route::post('AppletBanner/switchStatus', 'BannerController@switchStatus');

    //运营中心
    Route::post('operate/list', 'OperateController@index');
    Route::post('operate/update', 'OperateController@update');

    //PlatformBanner管理
    Route::post('platform-banner/get', 'PlatformBannerController@index');
    Route::post('platform-banner/update', 'PlatformBannerController@update');
    Route::post('platform-banner/delete', 'PlatformBannerController@delete');
    Route::post('platform-banner/create', 'PlatformBannerController@create');
    Route::post('platform-banner/detail', 'PlatformBannerController@detail');
    Route::post('platform-banner/switchStatus', 'PlatformBannerController@switchStatus');

    //组件
    Route::any('get_drag_list', 'AssemblyController@drag');//拖拽
    Route::any('get_tree_select_data', 'AssemblyController@tree');//树型下拉
    Route::any('get_org_data', 'AssemblyController@organization');//组织结构

});

